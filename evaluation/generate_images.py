#!/usr/bin/env python3

import os

import cv2 as cv
import numpy as np

from hypothesis_4 import bb_union
from contour_erosion import erode_convex_hull, default_elem_iterator
import util


"""
Script which generates images for demonstration in the paper.
"""


imgs = util.load_images()
anns = util.load_annotations()
data = util.load_data()


def generate_images_h1():
    color_iterator = util.ColorIterator()

    #              PID,      IMAGE_NAME
    selection = [( '1', '0065-V-R-0017'),
                 ( '1', '0098-V-L-0025'),
                 ('35', '0094-V-R-0024')]

    for pid, image_name in selection:
        img = imgs[image_name]
        ann = anns[image_name]

        contours, _ = cv.findContours(ann, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        ann = np.repeat(ann, 3).reshape(img.shape)
        res = cv.addWeighted(img, 1.0, ann, 0.5, 0.0)
        cv.drawContours(res, contours, -1, (0, 0, 0), 2)

        for oid in data[pid][image_name]:
            outline = np.array(data[pid][image_name][oid])

            color = next(color_iterator)
            cv.drawContours(res, [outline], -1, color, 3)

        cv.namedWindow('H1', cv.WINDOW_NORMAL)
        cv.resizeWindow('H1', 1000, 1000)
        cv.imshow('H1', res)

        key = cv.waitKey(0)
        if key == ord('s'):
            cv.imwrite(f'multiple-stains_{pid}_{image_name}.png', res)
        elif key == ord('q'):
            break


def generate_images_h2():
    color_iterator = util.ColorIterator(colors=(util.COLORS[0], util.COLORS[-1], util.COLORS[4]))

    #                          [PID],      IMAGE_NAME
    selection = [(['56',  '5', '34'], '0295-V-L-0074'),
                 (['70', '48', '34'], '0033-V-L-0009')]


    for pids, image_name in selection:
        img = imgs[image_name]
        ann = anns[image_name]

        contours, _ = cv.findContours(ann, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        ann = np.repeat(ann, 3).reshape(img.shape)
        res = cv.addWeighted(img, 1.0, ann, 0.5, 0.0)
        cv.drawContours(res, contours, -1, (0, 0, 0), 2)
        
        for pid in pids:
            color = next(color_iterator)

            for oid in data[pid][image_name]:
                outline = np.array(data[pid][image_name][oid])
                cv.drawContours(res, [outline], -1, color, 3)

        cv.namedWindow('H2', cv.WINDOW_NORMAL)
        cv.resizeWindow('H2', 1000, 1000)
        cv.imshow('H2', res)

        key = cv.waitKey(0)
        if key == ord('s'):
            cv.imwrite(f'arbitrary_{"-".join(pids)}_{image_name}.png', res)
        elif key == ord('q'):
            break


def generate_images_h3():
    color_iterator = util.ColorIterator()

    #              PID,      IMAGE_NAME, OID 
    selection = [( '5', '0049-V-L-0013', '2'),
                 ('22', '0098-V-R-0025', '0'),
                 ('24', '0295-V-L-0074', '6')]

    for pid, image_name, oid in selection:
        img = imgs[image_name]
        ann = anns[image_name]

        contours, _ = cv.findContours(ann, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        ann = np.repeat(ann, 3).reshape(img.shape)
        res = cv.addWeighted(img, 1.0, ann, 0.5, 0.0)
        cv.drawContours(res, contours, -1, (0, 0, 0), 2)

        outline = np.array(data[pid][image_name][oid])
        color = next(color_iterator)
        cv.drawContours(res, [outline], -1, color, 3)

        cv.namedWindow('H3', cv.WINDOW_NORMAL)
        cv.resizeWindow('H3', 1000, 1000)
        cv.imshow('H3', res)

        key = cv.waitKey(0)
        if key == ord('s'):
            cv.imwrite(f'cut-stains_{pid}_{image_name}.png', res)
        elif key == ord('q'):
            break


def generate_images_h4():
    COLOR_OUTLINE = util.COLORS[0]
    COLOR_CONVEX_HULL = util.COLORS[-1]
    COLOR_CONTOUR = util.COLORS[4]
    LINE_WIDTH = 2

    # Images to generate
    #             PID,  OID,      IMAGE_NAME,    RESULT_NAME
    imgs_gen = [( '5', '37', '0053-V-R-0014', 'good_erosion'),
                ('48',  '6', '0102-V-R-0026',  'bad_erosion'),
                ( '6',  '2', '0049-V-L-0013',  'good_single'),
                ('24',  '1', '0049-V-L-0013',  'bad_single')]

    # label the stains
    stains = {}
    for img_name, annotation in anns.items():
        _, stains[img_name] = cv.connectedComponents(annotation, connectivity=8)

    for pid, oid, img_name, res_name in imgs_gen:
        img = imgs[img_name]
        ann = anns[img_name]
        outline = np.array(data[pid][img_name][oid])

        res = cv.addWeighted(img, 1.0, ann.repeat(3).reshape(img.shape), 0.5, 0.0)
        cnts, _ = cv.findContours(ann, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
        cv.drawContours(res, cnts, -1, (0, 0, 0), 1)
        cv.drawContours(res, [outline], -1, COLOR_OUTLINE, LINE_WIDTH)

        mask = np.zeros(ann.shape, dtype=np.uint8)
        dst = np.zeros(ann.shape, dtype=np.int32)
        cv.drawContours(mask, [outline], -1, 1, -1)
        np.copyto(dst, stains[img_name], where=mask==1)
        stain_indices = np.unique(dst)[1:]
        if stain_indices.shape[0] == 1:
            print('Single stain...')
            stain_img = np.where(stains[img_name] == stain_indices[0], 255, 0).astype(np.uint8)
            contours, _ = cv.findContours(stain_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
            contour = contours[0][:, 0, :]
        else:
            for i in stain_indices:
                np.copyto(dst, stains[img_name], where=(stains[img_name] == i))
            dst = ((dst > 0) * 255).astype(np.uint8)
            stain_img = np.zeros(dst.shape, np.uint8)
            np.copyto(stain_img, dst, where=mask==1)
            print('Multiple stains')

            stain_points = cv.findNonZero(stain_img)
            hull = cv.convexHull(stain_points)
            hull = hull[:, 0, :]
            cv.drawContours(res, [hull], -1, COLOR_CONVEX_HULL, LINE_WIDTH)

            contour = erode_convex_hull(hull, stain_img, default_elem_iterator())

        cv.drawContours(res, [contour], -1, COLOR_CONTOUR, LINE_WIDTH)


        bb_outline = cv.boundingRect(outline)
        bb_contour = cv.boundingRect(contour)
        left, top, width, height = bb_union(bb_outline, bb_contour)

        left = left - width // 2
        top = top - height // 2
        right = left + width * 2
        bottom = top + height * 2

        res = res[top:bottom, left:right]

        cv.namedWindow('H4', cv.WINDOW_NORMAL)
        cv.resizeWindow('H4', 1000, 1000)
        cv.imshow('H4', res)

        key = cv.waitKey(0)
        if key == ord('s'):
            cv.imwrite(f'similarity-{res_name}.png', res)
        elif key  == ord('q'):
            break


if __name__ == '__main__':
    generate_images_h1()
    generate_images_h2()
    generate_images_h3()
    generate_images_h4()
