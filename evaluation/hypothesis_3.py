#!/usr/bin/env python3
import time

import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv

import util

"""
H3: Outlines do not cut stains.
"""

annotations = util.load_annotations()
images = util.load_images()
data = util.load_data()

# label the stains
label_imgs = {}
for name, annotation in annotations.items():
    _, label_imgs[name], _, _ = cv.connectedComponentsWithStats(annotation, connectivity=8)

outlines_per_img = {}
intersecting_outlines_per_img = {}

for pid, name, oid, outline in util.OutlineIterator(data):
    outlines_per_img[name] = outlines_per_img.get(name, 0) + 1

    label_img = label_imgs[name]
    empty = np.zeros(label_img.shape, dtype=label_img.dtype)

    # create a mask and its inverse for each outline
    # a outline intersects a stain if a label appears inside the mask and inside its inverse
    mask = np.zeros(label_img.shape, dtype=np.uint8)
    cv.drawContours(mask, [outline], 0, 1, cv.FILLED)

    label_img_inside  = np.where(mask == 1, label_img, empty)
    label_img_outside = np.where(mask == 0, label_img, empty)

    label_inside  = np.unique(label_img_inside)
    label_outside = np.unique(label_img_outside)

    cut_outline_labels = np.intersect1d(label_inside, label_outside)
    # compare the regions inside and outside of the mask
    # if a region other than 0 (no stain) is found both inside and outside of the mask, the region is intersected by the outline
    # since intersect1d returns a sorted array, it is sufficient to check whether the last element isn't 0
    if cut_outline_labels[-1] != 0:
        intersecting_outlines_per_img[name] = intersecting_outlines_per_img.get(name, 0) + 1

        # ========== debug begin ==========
        continue    # make this line a comment for debugging
        img = np.zeros((*annotations[name].shape, 3), dtype=np.float32)
        img[:, :, 0] = label_img_inside.astype(np.float32)
        img[:, :, 1] = label_img_outside.astype(np.float32)
        img[:, :, 2] = label_img.astype(np.float32)
        cv.drawContours(img, [outline], 0, (1, 1, 1), 1)
        cv.namedWindow("img", cv.WINDOW_NORMAL & cv.WINDOW_KEEPRATIO)
        height = 1000
        width = int((height * img.shape[1]) / img.shape[0])
        cv.resizeWindow('img', width, height)
        cv.imshow("img", img)

        """
        cv.namedWindow("orig", cv.WINDOW_NORMAL & cv.WINDOW_KEEPRATIO)
        cv.imshow("orig", stains[name].astype(np.float32))
        #cv.namedWindow("mask", cv.WINDOW_NORMAL & cv.WINDOW_KEEPRATIO)
        #cv.imshow("mask", mask.astype(np.float32))
        cv.namedWindow("applied mask", cv.WINDOW_NORMAL & cv.WINDOW_KEEPRATIO)
        cv.imshow("applied mask", dst_1.astype(np.float32))
        cv.namedWindow("applied mask_inverse", cv.WINDOW_NORMAL & cv.WINDOW_KEEPRATIO)
        cv.imshow("applied mask_inverse", dst_2.astype(np.float32))
        """
        if cv.waitKey(0) == ord("q"):
            exit()
        # ========== debug end ==========

intersecting_outlines = sum(intersecting_outlines_per_img.values())
total_outlines = sum(outlines_per_img.values())

print(' ' * 100, end='\r')
print(f'Total number of outlines: {total_outlines}')
print(f'Intersected outlines: {intersecting_outlines}')
print(f'percentage: {100 * intersecting_outlines / total_outlines:.2f}%')


low_outlines = 0
low_intersecting = 0
medium_outlines = 0
medium_intersecting = 0
high_outlines = 0
high_intersecting = 0

stains_per_img = {}
for img_name, ann in annotations.items():
    contours, _ = cv.findContours(ann, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    stains_per_img[img_name] = len(contours)

    if stains_per_img[img_name] < 10:
        low_outlines += outlines_per_img[img_name]
        low_intersecting += intersecting_outlines_per_img[img_name]
    elif 10 < stains_per_img[img_name] < 100:
        medium_outlines += outlines_per_img[img_name]
        medium_intersecting += intersecting_outlines_per_img[img_name]
    else:
        high_outlines += outlines_per_img[img_name]
        high_intersecting += intersecting_outlines_per_img[img_name]

print(f'Low {100 * low_intersecting / low_outlines:.2f}% --- {low_intersecting}, {low_outlines}')
print(f'Medium {100 * medium_intersecting / medium_outlines:.2f}% --- {medium_intersecting}, {medium_outlines}')
print(f'High {100 * high_intersecting / high_outlines:.2f}% --- {high_intersecting}, {high_outlines}')


xs = []
ys = []
for img_name in stains_per_img:
    xs.append(stains_per_img[img_name])
    ys.append(intersecting_outlines_per_img[img_name] / outlines_per_img[img_name])

plt.title('Intersecting Outlines per Image over Stain Count')

plt.xscale('log')
plt.xlabel('Stain Count')

plt.ylabel('Percentage of Intersecting Outlines')
plt.ylim([0.0, 1.0])
plt.yticks(np.arange(0, 1.05, 0.1))

plt.scatter(xs, ys)

plt.show()
