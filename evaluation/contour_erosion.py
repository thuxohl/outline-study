import cv2 as cv
import numpy as np

from model_zoo.util.opencv import compute_connected_components

DEBUG = False

class StructuralElement():

    def __init__(self, size=3):
        self.size = size
        self.center = size // 2

        self.window = np.zeros((size, size), np.uint8)

    def pad_img(self, img, y_min, y_max, x_min, x_max):
        """
        Pad an image with zeros so that it can be index as img[y_min:y_max, x_min:x_max].
        The indices are also returned because they need to be updated if y_min or x_min are negative.
        """
        if y_min >= 0 and x_min >= 0 and y_max < img.shape[0] and x_max < img.shape[1]:
            return img, y_min, y_max, x_min, x_max

        if DEBUG:
            print(f'Before Y: 0 < {y_min:4d} < {y_max:4d} < {img.shape[0]:4d}')
            print(f'Before X: 0 < {x_min:4d} < {x_max:4d} < {img.shape[1]:4d}')

        top = left = bottom = right = 0
        if y_min < 0:
            top = -y_min
            y_min = 0
            y_max += top

        if x_min < 0:
            left = -x_min
            x_min = 0
            x_max += left

        if y_max > (img.shape[0] - 1):
            bottom = y_max - (img.shape[0] - 1)

        if x_max > (img.shape[1] - 1):
            right = x_max - (img.shape[1] - 1)


        img = cv.copyMakeBorder(img, top, bottom, left, right, cv.BORDER_CONSTANT, 0)

        if DEBUG:
            print(f'Padding values T{top}, L{left}, B{bottom}, R{right}')
            print(f'After Y: 0 < {y_min:4d} < {y_max:4d} < {img.shape[0]:4d}')
            print(f'After X: 0 < {x_min:4d} < {x_max:4d} < {img.shape[1]:4d}')

        return img, y_min, y_max, x_min, x_max


    def evaluate(self, img, pos):
        y_min = pos[1] - self.center
        x_min = pos[0] - self.center

        y_max = y_min + self.size
        x_max = x_min + self.size

        img, y_min, y_max, x_min, x_max = self.pad_img(img, y_min, y_max, x_min, x_max)

        result = img[y_min:y_max, x_min:x_max] * self.window
        return (result > 0).any()


class CircleElement(StructuralElement):

    def __init__(self, size=3):
        super().__init__(size)
        cv.circle(self.window, (self.center, self.center), self.center, 1, -1)


class SquareElement(StructuralElement):

    def __init__(self, size=3):
        super().__init__(size)
        self.window = 1


class ElementIterator():

    def __init__(self, create_func):
        self.create_func = create_func
        self.i = 0

    def __iter__(self):
        self.i = 0
        return self

    def __next__(self):
        _next = self.create_func(self.i)
        self.i += 1
        return _next


def default_elem_iterator():
    return ElementIterator(lambda i: CircleElement(3 + 2 * i))


def erode_convex_hull(hull, img, elem_iterator):
    img = img.astype(np.uint8) * 255
    left, top, width, height = cv.boundingRect(hull)

    hull_img = np.zeros((height, width), np.uint8)
    cv.drawContours(hull_img, [hull], -1, 255, -1, offset=(-left, -top))

    img_slice = img[top:(top + height), left:(left + width)] > 0

    while True:
        try:
            elem = next(elem_iterator)
        except StopIteration:
            if DEBUG:
                print('Last element applied')
            return last_cnt

        cnts, _ = cv.findContours(hull_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE, offset=(left, top))
        if len(cnts) > 1:
            if DEBUG:
                print(f'Erode operation started to split into multiple regions')
            return last_cnt
        last_cnt = cnts[0][:, 0, :]

        _hull_img = hull_img.copy()
        for pt in last_cnt:
            pt = tuple(pt)
            hull_img[pt[1] - top, pt[0] - left] = elem.evaluate(img, pt) * 255

        if (hull_img == _hull_img).all():
            if DEBUG:
                print(f'Contour no longer changed')
            return last_cnt

        if not (np.logical_and(img_slice, hull_img > 0) == img_slice).all():
            if DEBUG:
                print(f'Original image no longer contained in contour')
            return last_cnt

    cnts, _ = cv.findContours(_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE, offset=(left, top))
    return cnts[0]


# Example of a function for an element iterator
"""
def f(i):
    if i > 10:
        raise StopIteration
    size = 3 + (i * 4)
    return CircleElement(size)
"""
