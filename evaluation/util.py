#!/usr/bin/env python3

import os
import json

import numpy as np
import cv2 as cv

def _load_from_dir(d, mode):
    res = {}
    for filename in sorted(os.listdir(d)):
        key, _ = os.path.splitext(filename)
        val = cv.imread(os.path.join(d, filename), mode)
        res[key] = val
    return res


def load_annotations(d='data/annotations/'):
    return _load_from_dir(d, cv.IMREAD_GRAYSCALE)

def load_images(d='data/images/'):
    return _load_from_dir(d, cv.IMREAD_COLOR)

def load_data(filename='data/data_processed.json'):
    with open(filename, mode='r+') as f:
        return json.load(f)


class OutlineIterator():

    def __init__(self, data, print_process=True):
        self._data = []
        for pid in data:
            for image_name in data[pid]:
                for outline_id in data[pid][image_name]:
                    self._data.append((pid, image_name, outline_id, np.array(data[pid][image_name][outline_id])))
        self.print_process = print_process

    def __len__(self):
        return len(self._data)

    def __getitem__(self, i):
        pid, name, outline_id, outline = self._data[i]
        if self.print_process:
            print(f'Process: participant {pid:>2} image {name} outline {outline_id:>3}', end='\r')
        return self._data[i]


COLORS = [(136,  34,  51),
          ( 51, 119,  17),
          (153, 170,  58),
          (238, 204, 136),
          (119, 204, 221),
          (119, 102, 204),
          (153,  68, 170),
          ( 85,  34, 136),
          ( 75,  75, 255)]


class ColorIterator:

    def __init__(self, colors=COLORS):
        self.index = 0
        self.colors = colors

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        color = self.colors[self.index]
        self.index = (self.index + 1) % len(self.colors)
        return color

