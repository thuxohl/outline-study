#!/usr/bin/env python3

import numpy as np
import cv2 as cv

import matplotlib.pyplot as plt

import util
from contour_erosion import erode_convex_hull, default_elem_iterator

"""
H4: Outlines are similar to the contour of the objects they encompass.
"""

SIMILARITY_THRESHOLD = 0.9


def main():
    # load the data
    imgs = util.load_images()
    annotations = util.load_annotations()
    data = util.load_data()

    # label the stains
    stains = {}
    for img_name, annotation in annotations.items():
        _, stains[img_name] = cv.connectedComponents(annotation, connectivity=8)


    outlines_single = 0
    outlines_multiple = 0
    outlines_single_similar = 0
    outlines_multiple_similar = 0
    for pid, img_name, oid, outline in util.OutlineIterator(data):
        ann = annotations[img_name]

        mask = np.zeros(ann.shape, dtype=np.uint8)
        dst = np.zeros(ann.shape, dtype=np.int32)

        # create and apply the mask
        cv.drawContours(mask, [outline], -1, 1, -1)
        np.copyto(dst, stains[img_name], where=mask==1)

        # extract the stain indices
        stain_indices = np.unique(dst)[1:]

        if stain_indices.shape[0] == 0:
            # skip if there are no stains inside the outline
            continue

        if stain_indices.shape[0] == 1:
            # a single stain in the outline
            outlines_single += 1

            stain_img = np.where(stains[img_name] == stain_indices[0], 255, 0).astype(np.uint8)
            contours, _ = cv.findContours(stain_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
            contour = contours[0][:, 0, :]

            # exclude contour which does not cover enough of the stain
            if amount_contour_covered(outline, contour) < 0.9:
                continue

            distances = compute_distances(outline, contour)
            _, _, widht, height = cv.boundingRect(contour)
            distance_threshold = min(10, min(width, height) / 2)
            if is_similar(distances, distance_threshold=distance_threshold, outline_threshold=SIMILARITY_THRESHOLD):
                outlines_single_similar += 1

        else:
            # multiple stains in the outline
            outlines_multiple += 1

            # only consider contour of stains and parts of them inside the outline
            for i in stain_indices:
                np.copyto(dst, stains[img_name], where=(stains[img_name] == i))
            dst = ((dst > 0) * 255).astype(np.uint8)
            stain_img = np.zeros(dst.shape, np.uint8)
            np.copyto(stain_img, dst, where=mask==1)

            stain_points = cv.findNonZero(stain_img)
            hull = cv.convexHull(stain_points)
            hull = hull[:, 0, :]
            contour = erode_convex_hull(hull, stain_img, default_elem_iterator())

            distances = compute_distances(outline, contour)
            _, _, width, height = cv.boundingRect(contour)
            distance_threshold = min(10, min(width, height) / 2)
            if is_similar(distances, distance_threshold=distance_threshold, outline_threshold=SIMILARITY_THRESHOLD):
                outlines_multiple_similar += 1


    print(' ' * 100)
    print(f'Similar single:   {outlines_single_similar / outlines_single:.2f}%')
    print(f'Similar multiple: {outlines_multiple_similar / outlines_multiple:.2f}%')
    print(f'Similar overall:  {(outlines_single_similar + outlines_multiple_similar) / (outlines_single + outlines_multiple):.2f}%')


def contour_without_approx(contour):
    """
    Often contours are approximated by only storing necessary points.
    E.g. if there is a straight line, it is enough to only the store the two
    end points (see: https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html#ga4303f45752694956374734a03c54d5ff).

    This is also the case for user drawn outlines, since the outline is
    created by storing the touch point of the user at fixed time intervals.

    This method converts such a contour into a non approximating one.
    Thus, every pixel the contour touches is stored.
    This is needed to compute the distances.
    Else, e.g. for a straight line, lots of distances would not be computed.
    """
    # limit calculation to an image the size of the bounding box of the contour
    left, top, width, height = cv.boundingRect(contour)
    # draw the contour onto an image
    img = np.zeros((height, width), np.uint8)
    cv.drawContours(img, [contour], 0, 255, -1, offset=(-left, -top))
    # re-find the contour without approximation
    contours, _ = cv.findContours(img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE, offset=(left, top))
    return contours[0][:, 0, :]


def compute_distances(outline, contour):
    """
    Compute the minimal unsigned distance of every pixel the outline runs
    through to the contour.
    """
    outline = contour_without_approx(outline)
    contour = contour_without_approx(contour)

    distances = []
    for pt in outline:
        distance = abs(cv.pointPolygonTest(contour, tuple(pt), measureDist=True))
        distances.append(distance)
    return distances


def is_similar(distances, distance_threshold=5, outline_threshold=0.9):
    """
    Determine if a list of distances from an outline to a contour
    show that the outline is similar to the contour
    """
    mean = np.mean(distances)
    bounds = (mean - distance_threshold, mean + distance_threshold)
    valid_distances = list(filter(lambda d: bounds[0] <= d <= bounds[1], distances))
    return (len(valid_distances) / len(distances)) >= outline_threshold


def amount_contour_covered(outline, contour):
    """
    Compute the relative amount of pixels of the region defined by contour
    which are covered by the outline.
    """
    bb_outline = cv.boundingRect(outline)
    bb_contour = cv.boundingRect(contour)

    left, top, width, height = bb_union(bb_contour, bb_outline)
    offset = (-left, -top)

    img_outline = np.zeros((height, width), np.uint8)
    cv.drawContours(img_outline, [outline], -1, 1, -1, offset=offset)

    img_contour = np.zeros((height, width), np.uint8)
    cv.drawContours(img_contour, [contour], -1, 1, -1, offset=offset)

    return np.logical_and(img_outline, img_contour).sum() / img_contour.sum()


def bb_union(bb1, bb2):
    """
    Compute the union of two bounding boxes.
    Bounding boxes are tuples of the form (left, top, width, heigh).
    """
    left, top, width, height = bb1
    right = left + width
    bottom = top + height

    _left, _top, _width, _height = bb2
    _right = _left + _width
    _bottom = _top + _height

    left = min(left, _left)
    top = min(top, _top)
    right = max(right, _right)
    bottom = max(bottom, _bottom)
    return left, top, right - left, bottom - top


if __name__ == '__main__':
    main()
