#!/usr/bin/env python3

import numpy as np
import cv2 as cv

import util


"""
H1: Stains are often grouped and not outlined individually.
"""

def main():
    annotations = util.load_annotations()
    images = util.load_images()
    data = util.load_data()

    # loop through all outlines and and count the number of regions inside a outline
    multiple_stains_count = 0
    stain_counts = []

    iterator = util.OutlineIterator(data)
    for pid, name, oid, outline in iterator:
        annotation = annotations[name]

        mask = np.zeros(annotation.shape, dtype=np.uint8)
        dst = np.zeros(annotation.shape, dtype=np.uint8)

        # create and apply the mask
        cv.drawContours(mask, [outline], -1, 1, -1)
        np.copyto(dst, annotation, where=mask==1)

        # count the number of regions inside the mask and subtract 1 for the background
        region_count, _ = cv.connectedComponents(dst, connectivity=8)
        region_count -= 1

        stain_counts.append(region_count)
        if region_count > 1:
            multiple_stains_count += 1

    print(' ' * 100, end='\r')
    print(f'Total number of outlines: {len(iterator)}')
    print(f'Outlines containing multiple stains: {multiple_stains_count}')
    print(f'Percentage: {100 * multiple_stains_count / len(iterator):.2f}%')
    print(f'Average number of stains per outline: {np.mean(stain_counts):.2f}')
    print(f'Median number of stains per outline: {np.median(stain_counts):.2f}')


if __name__ == '__main__':
    main()
