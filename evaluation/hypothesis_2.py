#!/usr/bin/env python3

import numpy as np
import cv2 as cv

import util

"""
H2: The grouping of stains is arbitrary.
"""
# An outline is seen as arbitrary if less than OUTLINE_CONSISTENCY % of participants have a similar outline
# two outlines are similar if they share at least OUTLINE_SIMILARITY % of stains
OUTLINE_SIMILARITY = 0.67  # percantage how many stains inside the outlines have to match
OUTLINE_CONSISTENCY = 0.25 # percantage of participants that need to have a similar outline

def main():
    # load the data
    annotations = util.load_annotations()
    data = util.load_data()

    # label the stains
    stains = {}
    for img_name, annotation in annotations.items():
        _, stains[img_name] = cv.connectedComponents(annotation, connectivity=8)


    # calculate what region labels are contained in the different outlines
    outline_labels = {}
    for img_name in annotations:
        outline_labels[img_name] = {}

    for pid, img_name, oid, outline in util.OutlineIterator(data):
        outline_list = outline_labels[img_name].get(pid, [])

        annotation = annotations[img_name]
        mask = np.zeros(annotation.shape, dtype=np.uint8)
        dst = np.zeros(annotation.shape, dtype=np.int32)

        # create and apply the mask
        cv.drawContours(mask, [outline], -1, 255, -1)
        np.copyto(dst, stains[img_name], where=(mask==255))

        # extract the region labels and remove the first element (0 = background)
        outline_list.append(np.unique(dst)[1:])

        outline_labels[img_name][pid] = outline_list

    print(' ' * 100, end='\r')

    outline_counter_total = 0
    outline_counter_arbitrary = 0

    for img_name in sorted(outline_labels):
        print(f'Process: {img_name}', end='\r')
        for participant_1, outlines_participant_1 in outline_labels[img_name].items():
            for outline_participant_1 in outlines_participant_1:
                outline_counter_total += 1

                # check whether the outline is arbitrary
                # similar_outlines starts at 1, since the outline is similar to itself
                similar_outlines = 1
                for participant_2, outlines_participant_2 in outline_labels[img_name].items():
                    # skip the current participant
                    if participant_2 == participant_1:
                        continue

                    for outline_participant_2 in outlines_participant_2:
                        # check whether c is similar to outline
                        shared_stains = np.intersect1d(outline_participant_1, outline_participant_2).shape[0]
                        if shared_stains >= OUTLINE_SIMILARITY * outline_participant_1.shape[0] and \
                           shared_stains >= OUTLINE_SIMILARITY * outline_participant_2.shape[0]:
                            similar_outlines += 1
                            break

                    # exit loop if condition is already fulfilled
                    if similar_outlines >= OUTLINE_CONSISTENCY * len(data):
                        break
                # check whether condition is fulfilled
                if similar_outlines < OUTLINE_CONSISTENCY * len(data):
                    outline_counter_arbitrary += 1

    print(' ' * 100, end='\r')
    print(f'Total number of outlines: {outline_counter_total}')
    print(f'Number of arbitrary outlines: {outline_counter_arbitrary}')
    print(f'Percentage: {100 * outline_counter_arbitrary / outline_counter_total:.2f}%')


if __name__ == '__main__':
    main()
