#!/usr/bin/env python3

import functools
import json
import os

def load_data(data_file):
    with open(data_file, mode='r') as f:
        return json.load(f)

images = [filename[:-4] for filename in os.listdir('./data/images/')]
data = load_data('./data/data.json')

pids = set(map(lambda row: row['participant_id'], data))

def filter_pid(pid):
    contains_image = dict([(image_name, False) for image_name in images])

    _data = list(filter(lambda row: row['participant_id'] == pid, data))
    for row in _data:
        contains_image[row['image_name']] = True

    return functools.reduce(lambda x, y: x and y, contains_image.values()) 


pids = set(filter(filter_pid, pids))
print(pids)
