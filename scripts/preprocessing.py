#!/usr/bin/env python3

import cv2 as cv
import argparse
import numpy as np
import os
import csv
import json


def fuse_contour(contours, data, annotations, contour_id):
    ann = annotations[image_name]

    contours = map(lambda contour: np.array(contour), contours)
    contours = list(contours)
    contour = connect_contours(contours, ann)

    data[contour_id] = contour.tolist()


def connect_contours(contours, img):
    _img = np.zeros(img.shape[:2], np.uint8)
    cv.drawContours(_img, contours, -1, 255, -1)
    _contours, _ = cv.findContours(_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

    if args.debug:
        cv.imshow('test', _img)
        cv.waitKey(0)

    if len(_contours) != 1:
        raise ValueError(f'Connecting contours {contours} resulted in {len(_contours)} contours')

    return _contours[0]


def load_json(json_file):
    with open(json_file, mode='r+') as f:
        return json.load(f)


def load_imgs(annotations_dir, color=False):
    annotations = {}
    for filename in sorted(os.listdir(annotations_dir)):
        mode = cv.IMREAD_COLOR if color else cv.IMREAD_GRAYSCALE
        ann = cv.imread(os.path.join(annotations_dir, filename), mode)
        filename, _ = os.path.splitext(filename)

        annotations[filename] = ann
    return annotations


def reformat_contour(contour, height, width):
    return [(round(pt['x'] * width), round(pt['y'] * height)) for pt in contour]

def reformat_data(data, annotations):
    data = sorted(data, key=(lambda row: '{:03d}'.format(row['participant_id']) + '_' +
                                         row['image_name'] + '_' +
                                         '{:03d}'.format(row['contour_id'])))
    result = {}
    for row in data:
        participant = result.get(row['participant_id'], {})
        image = participant.get(row['image_name'], {})
        image[int(row['contour_id'])] = reformat_contour(row['contour'], *annotations[row['image_name']].shape)
        participant[row['image_name']] = image
        result[row['participant_id']] = participant
    return result


def contour_to_json(contour, height, width):
    return [{'x': pt[0] / width, 'y': pt[1] / height} for pt in contour]

def filter_not_all_images(data, annotations):
    pids_every_image_marked = filter(lambda pid: data[pid].keys() == annotations.keys, data)
    as_dict = dict(map(lambda pid: (pid, data[pid]), pids_every_image_marked))
    return as_dict


def get_tuple_from_delete(delete):
    remove = []
    if delete != '[]':
        delete = delete.split('.')
        image_name = ''
        for i in range(len(delete)):
            if i % 2 == 0:
                image_name = delete[i][3:-1:]
            else:
                contour_id = delete[i].split(')')[0]
                contour_id = int(contour_id)
                remove.append((image_name, contour_id))

    return remove


def get_tuple_from_connect(connect):
    add = []
    if connect != '[]':
        connect = connect.split('.')
        image_name = ''
        case = ''

        for i in range(len(connect)):
            temp = connect[i].split(';')
            image_name = temp[0][3:-1:]
            contour_ids = []
            for j in range(1, len(temp)):
                temp[j] = int(temp[j].split(']')[0])
                contour_ids.append(temp[j])
            add.append((image_name, contour_ids))
    return add


def load_csv(filepath):
    with open(args.csv, mode='r') as csv_file:
        operations = {}
        rows = csv.reader(csv_file, delimiter=',', quotechar='"', skipinitialspace=True)
        next(rows)
        for row in rows:
            usable = int(row[1])
            if usable == 0:
                continue

            participant_id = int(row[0])
            delete = row[2]
            connect = row[3]

            operations[participant_id] = {
                'remove': get_tuple_from_delete(delete),
                'fuse': get_tuple_from_connect(connect)
            }
    return operations


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='script to preprocess contours for evaluation')
    parser.add_argument('--data', help='path to data.json', default='./data/data.json')
    parser.add_argument('--out_file', help='path the saved processed data file', default='./data/data_processed.json')
    parser.add_argument('--csv', help='path to eval.csv', default='./eval.csv')
    parser.add_argument('--image_dir', help='path to image files', default='./data/images')
    parser.add_argument('--debug', help='run script in debug mode', action='store_true')
    args = parser.parse_args()

    annotations = load_imgs('./data/annotations/')
    data = load_json(args.data)
    data = reformat_data(data, annotations)
    # filter out all participants that did not annotate every image
    data = dict(map(lambda pid: (pid, data[pid]),
                    filter(lambda pid: data[pid].keys() == annotations.keys(),
                           data)))

    # filter out all participants marked as unusable in the csv file
    operations = load_csv(args.csv)
    pids_to_remove = [pid for pid in data if pid not in operations]
    for pid in pids_to_remove:
        data.pop(pid)

    # remove invalid contours and some contours together
    for pid in operations:
        for (image_name, contour_id) in operations[pid]['remove']:
            data[pid][image_name].pop(contour_id)
        for (image_name, contour_ids) in operations[pid]['fuse']:
            contours_to_fuse = []
            for contour_id in contour_ids:
                contours_to_fuse.append(data[pid][image_name].pop(contour_id))
            fuse_contour(contours_to_fuse, data[pid][image_name], annotations, max(contour_ids))

    # save processed data
    if not args.debug:
        with open(args.out_file, mode='w') as json_file:
            json.dump(data, json_file, indent=2)
