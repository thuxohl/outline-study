#!/usr/bin/env python3

import cv2 as cv
import json
import os
import argparse
import numpy as np

from preprocessing import reformat_data, load_imgs, load_json

parser = argparse.ArgumentParser(description='draw contours onto an image',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-p', '--participant_id', type=str,
                    help='the participant id')
parser.add_argument('-i', '--image_dir', help='path to the dir containing the images', default='./data/images/')
parser.add_argument('-d', '--data', help='path to data file', default='./data/data.json')
parser.add_argument('--image_name', help='only draw contours for this image')
parser.add_argument('--increment', action='store_true', help='draw contours incrementally and print according ids')
parser.add_argument('--pre_processed', action='store_true', help='the data provided is already pre_processed')
args = parser.parse_args()

images = load_imgs(args.image_dir, color=True)
data = load_json(args.data)
if not args.pre_processed:
    args.participant_id = int(args.participant_id)
    annotations = load_imgs('/'.join([*args.image_dir.split('/')[:-2], 'annotations']))
    data = reformat_data(data, annotations)

if args.participant_id not in data:
    raise ValueError(f'Paricipant {args.participant_id} is not available')


if args.image_name:
    images = {args.image_name: images[args.image_name]}

class ColorIterator:

    def __init__(self):
        self.index = 0
        self.colors = [(255,   0,   0),
                       (255, 127,   0),
                       (255, 255,   0),
                       (127, 255,   0),
                       (  0, 255,   0),
                       (  0, 255, 127),
                       (  0, 255, 255),
                       (  0, 127, 255),
                       (  0,   0, 225)]


    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        color = self.colors[self.index]
        self.index += 2
        if self.index >= len(self.colors):
            if self.index % 2 == 0:
                self.index = 1
            else:
                self.index = 0
        return color
color_iterator = ColorIterator()


cv.namedWindow('Contours', cv.WINDOW_NORMAL)
cv.resizeWindow('Contours', 1000, 1000)
for img_name, img in images.items():
    print(f'Image {img_name}')

    height, width, _ = img.shape
    for contour_id, contour in data[args.participant_id][img_name].items():
        contour = np.array(contour)
        img = cv.drawContours(img, [contour], -1, next(color_iterator), 3)

        if not args.increment:
            continue

        print(f'  - ContourId: {contour_id}')
        cv.imshow('Contours', img)
        key = cv.waitKey(0)
        if key == ord('q'):
            exit()
        elif key == ord('n'):
            break

    if not args.increment:
        cv.imshow('Contours', img)
        if cv.waitKey(0) == ord('q'):
            break
