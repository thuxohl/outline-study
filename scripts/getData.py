#!/usr/bin/env python3

import requests
import json
import os
import argparse
import subprocess
import datetime

parser = argparse.ArgumentParser(description='script to get data from server and push to git',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-d', '--dir', help='set output directory', default='./data/')
parser.add_argument('-n', '--name', help='set name for output file', default='data')
parser.add_argument('-s', '--server', help='set server adress', default='http://outline-study.herokuapp.com')
parser.add_argument('--debug', action='store_true',
                    help='debug mode which does not create any files and does not perform any actions with git')
args = parser.parse_args()

r = requests.get(args.server + '/data')
rjson = r.json()
path = os.path.join(args.dir, args.name + '.json')

new_participant_ids = []
for i in range(len(rjson)):
    new_participant_ids.append(rjson[i]['participant_id'])
new_participant_ids = set(new_participant_ids)

old_participant_ids = []
if os.path.exists(path):
    with open(path, 'r') as infile:
        data = json.load(infile)
        for i in range(len(data)):
            old_participant_ids.append(data[i]['participant_id'])
old_participant_ids = set(old_participant_ids)

removed_participant_ids = list(old_participant_ids.difference(new_participant_ids))
added_participant_ids = list(new_participant_ids.difference(old_participant_ids))

commit_message = str(datetime.datetime.now()) + ': '
commit_message += 'removed' + str(removed_participant_ids) + ' '
commit_message += 'added' + str(added_participant_ids)
commit_message = '"' + commit_message + '"'

if args.debug:
    print('JSON:')
    print(json.dumps(rjson, sort_keys=True, indent=2, separators=(',', ':')))
    print()
    print('Commit message: [' + commit_message + ']')
    exit()

with open(path, 'w') as outfile:
    json.dump(rjson, outfile, sort_keys=True, indent=2, separators=(',', ':'))
subprocess.run(['git', 'add', path])
subprocess.run(['git', 'commit', '-m', commit_message, path])
subprocess.run(['git', 'push'])
