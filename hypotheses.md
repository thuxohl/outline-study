# Hypothesen
Generell geht es hier um Annahmen die gemacht werden können, um die vom Nutzer vorgenommene Selektierung automatisch zu verbessern.
Aber auch um Bedeutungen für darauf trainierte Algorithmen.


1. Umrandungen schneiden Flecken nicht
  * Flecken werden nicht durch Umrandungen geteilt
  * Auswertung: Passiert das? Wenn ja wie oft? Pro Anzahl Flecken/Umrandungen? In bestimmten Fällen?
  * __Begründing__:
    Für autmotische Segmentierung wäre es gut, wenn klar ist, dass Flecken wirklich innerhalb sind.
    Dementsprechend wäre es auch nicht schlimm, wenn ein Fleck von einer Kontur geschnitten wird, wenn er dafür in einer anderen enthalten ist

    Motiviert durch gleichmäßge Abstandsthese


Total number of contours: 2733                                                                      
Intersected contours: 1203
percentage: 44.02%




2. Flecken werden nicht immer einzeln umrandet, sondern in Gruppen
  * Wirkt klar, wenn man die Bilder sieht
  * Auswertung: #Konturen mit mehr als einem Fehler & Durchschnittliche Anzahl Flecken pro Kontur
  * __Begründing__:
    Viele Segmentierungsalgorithmen (__BEISPIELE__) gehen davon aus, dass in einer Umrandung nur ein Object liegt -> entsprechend auswählen

    Multi-Object Segmentation needed or not


Total number of contours: 2733
Contours containing multiple stains: 1759, Contours with a single stain 974
Percentage: 64.36%
Average number of stains per contour: 35.80
    

3. Die Gruppierung von Flecken erfolgt willkürlich
  * Das bedeutet, dass die Umrandung die Flecken unterschiedlich in Gruppen einteilt
  * Die Gruppierung könnte abhängig von visueller Zuordnung sein (Gestalt Psychologie)?
  * Auswertung: eine Umrandung bildet eine Gruppe von Flecken -> gucke für jedes Bild ob diese Gruppen von Versuchsperson zu Versuchsperson variieren
  * __Begründing__:
    Argument, warum die Umrandungen nicht direkt als ungenaue markierungen genutzt werden können


    Warum ist das in meinem Kontext wichtig?
      - Es gibt 2 Ansätze, wie man das Problem angehen kann:
        1. basierend auf der Kontur eine feinere Segmentierung vornehmen und diese dann zum Training verwenden
        2. die Konturen so wie sie sind für das Training benutzen
        In 2terem Fall existieren dann verschiedene mögliche Annotationen für ein Bild.
        Widerspricht das tatsächlich dem Loss wie er bisher berechnet wird?

4. Je größer ein Fleck desto wahrscheinlicher wird er einzeln umrandet
  * Ich glaube das kann ich aufgrund des Datensatzes den ich habe nicht wirklich untersuchen
  * Außerdem spielen da zu viele andere Fakten rein: Nähe von Flecken zueinander, große Flecken treten generell häufiger einzeln auf, ...
  * Auswertung: Plotte #einzeln umrandete Flecken/#Fleckgröße in Pixel

5. Die Umrandung ist in der Form ähnlich zur Kontur des/der zu umrandenden Objekte
   Um das zu überprüfen, müsste ich eine Kontur um alle Flecken innerhalb einer vom Teilnehmer gezogenen Kontur legen.
   Dafür finde ich aber keine Lösung :/


  * Das ist natürlich schwierig wenn Gruppen umrandet werden.
  * Im Prinzip untersucht das sonst die Annahme von Pizenberg et al., das für eine Versuchsperson der mittlere Abstand von Umrandung zu Kontur etwa konstant ist
  * Auswertung: Varianz der nächsten Abstände von Objekt zur Kontur

6. Je kleiner ein Fleck ist, desto wahrscheinlicher ist es, dass er nicht umrandet wird
  * Vermutung: kleinere Flecken werden als unwichtiger betrachtet oder einfach übersehen
  * Auswertung: durchschnittliche Größe übersehener Flecken vs durchschnittliche Größe markierter Flecken (in Pixeln)


7. Annotationen konvex?
