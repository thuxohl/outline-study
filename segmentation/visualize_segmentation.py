import argparse
import math
import sys
import os
import time
import copy

import numpy as np
import cv2 as cv
import tqdm

from evaluation.util import load_images, load_annotations, load_data

def main():
    global args
    parser = argparse.ArgumentParser('Visualizes segmentation results',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('-s', '--segmentation_dir', type=str, default='./data/segmentation/deepCutv3',
                        help='the directory which contains the segmentation masks')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "images"))
    gt = load_annotations(d=os.path.join(args.data_dir, "annotations"))
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # iterate over all participants
    for i, participant in enumerate(tqdm.tqdm(data)):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():

            # load the segmentation mask
            filename = f"{args.segmentation_dir}/participant_{participant}_{image_name}.png"
            segmentation = cv.imread(filename, cv.IMREAD_GRAYSCALE)

            # visualization
            contour_img = np.copy(images[image_name])
            # draw the contours onto the image
            cv.drawContours(contour_img, [np.array(x) for x in data[participant][image_name].values()], -1, (255, 0, 0), 2)

            # compare the segmentation to the ground truth
            img = np.copy(contour_img)
            overlay = np.stack((np.zeros(img.shape[:2]), gt[image_name], segmentation), axis=-1)
            img[np.any(overlay != 0, axis=2)] = overlay[np.any(overlay != 0, axis=2)]
            cv.imshow(f"participant_{participant}_{image_name}_gt_comparison", img)
            cv.moveWindow(f"participant_{participant}_{image_name}_gt_comparison", 100, 0)

            # visualize the segmentation by darkening unselected areas
            img = np.copy(contour_img)
            img[segmentation == 0] = np.round(0.3 * img[segmentation == 0]).astype(np.uint8)
            cv.imshow(f"participant_{participant}_{image_name}_segmentation", img)
            cv.moveWindow(f"participant_{participant}_{image_name}_segmentation", img.shape[1] + 100, 0)

            # wait for the user to close the window
            # quit if the user pressed 'q'
            if cv.waitKey(0) == ord('q'):
                cv.destroyAllWindows()
                exit()
            cv.destroyAllWindows()

    cv.destroyAllWindows()

if __name__ == "__main__":
    main()
