import os

def read_mapping_file(mapping_file):
    """
    Read a mapping file into a list.
    Each entry in the list consists of a tuple whose first
    entry is the image and whose second entry is the according
    annotation.
    """
    res = []
    with open(mapping_file, mode='r') as f:
        for line in f:
            mapping_as_list = line.strip().split()
            mapping_as_tuple = (*mapping_as_list,)
            res.append(mapping_as_tuple)
    return res


def write_mapping_file(mapping_file, mappings):
    """
    Write a mapping file as produced by the read_mapping_file
    method.
    """
    with open(mapping_file, mode='w') as f:
        for mapping in mappings:
            file_img = mapping[0]
            file_ann = mapping[1]
            f.write(f'{file_img}\t{file_ann}\n')


def get_laundry_id(filename):
    """
    Extract the laundry id from the filename of a laundry
    image.

    TODO: fix that it works with image names where the lighting type is added
    """
    basename = os.path.basename(filename)
    extension_removed, _ = os.path.splitext(basename)
    patch_info_removed = extension_removed.split('_')[0]
    laundry_id = patch_info_removed.split('-')[-1]
    return int(laundry_id)


def get_patch_info(filename_patch):
    """
    Extract the image filename as well as the indices of
    a patch from a filename.
    """
    _tmp = os.path.basename(filename_patch)
    _tmp, ext = os.path.splitext(_tmp)
    _tmp, patch_info = _tmp.split('_')
    filename_img = _tmp + ext
    i, j = [int(number) for number in patch_info.split('-')]
    return filename_img, (i, j)

def get_patch_filename(filename, patch_coords, stride=1):
    """
    Get the filename for a patch based on the filename of an image
    the patch coordinates. The stride of the patch creation can be
    added to scale the patch coordinates.
    """
    i, j = (int(patch_coords[0] / stride), int(patch_coords[1] / stride))
    basename, extension = os.path.splitext(filename)
    return basename + '_' + str(i) + '-' + str(j) + extension
