import argparse
import json
import logging
import logging.handlers
import sys

from functools import wraps


# add metric log leve
LOG_LEVEL_METRIC = 21 # after info
logging.addLevelName(LOG_LEVEL_METRIC, "METRIC")
def metric(self, message, *args, **kws):
    if self.isEnabledFor(LOG_LEVEL_METRIC):
        self._log(LOG_LEVEL_METRIC, message, args, **kws) 
logging.Logger.metric = metric


def wrap_parse_args(parser, json_parser):
    parser._parse_args = parser.parse_args

    @wraps(parser.parse_args)
    def wrapper(*args, **kwargs):
        json_args, _ = json_parser.parse_known_args(*args, **kwargs)
        if json_args.update_json:
            json_args.load_json = json_args.update_json
            json_args.save_json = json_args.update_json

        if json_args.load_json:
            with open(json_args.load_json, 'r') as f:
                # Note: this is a workaround to handle required values.
                args_list = []
                for (key, val) in filter(lambda x: x[1] is not None, json.load(f).items()):
                    if type(val) is bool:
                        if val:
                            args_list.extend(['--' + key])
                        continue
                    args_list.extend(['--' + key, str(val)])
                # add the original args to the end of the list since late
                # arguments will override newer ones
                args_list.extend(sys.argv[1:])
                args = parser._parse_args(args_list)

        else:
            args = parser._parse_args()

        if json_args.save_json:
            with open(json_args.save_json, 'w') as f:
                as_dict = vars(args)
                del as_dict['save_json']
                del as_dict['load_json']
                del as_dict['update_json']
                json.dump(vars(args), f, indent=4)
            exit()

        # Setup logging
        numeric_level = getattr(logging, args.loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: %s' % args.loglevel)
        logging.getLogger().setLevel(numeric_level)

        log_formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s: %(message)s',
                                          datefmt='%m/%d/%Y %I:%M:%S %p')

        log_handler_console = logging.StreamHandler()
        log_handler_console.setFormatter(log_formatter)
        logging.getLogger().addHandler(log_handler_console)

        if args.logfile:
            log_handler_file = logging.handlers.WatchedFileHandler(args.logfile)
            log_handler_file.setFormatter(log_formatter)
            logging.getLogger().addHandler(log_handler_file)

        return args
    
    parser.parse_args = wrapper


def create_parser(description=''):
    """
    Create an argument parser which registers arguments for logging
    and parsing params from json files per default. Note that the returned
    parser does not support positional arguments and arguments with multpiple
    values.
    """
    json_parser = argparse.ArgumentParser(add_help=False)
    json_parser.add_argument('--save_json', type=str,
                             help='store the program params into a json file')
    json_parser.add_argument('--load_json', type=str,
                             help='load the program params from a json file')
    json_parser.add_argument('--update_json', type=str,
                             help='this value will set save and load json to the same json file')

    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     parents=[json_parser])
    parser.add_argument('--logfile', type=str,
                        help='the file to which output is logged')
    parser.add_argument('--loglevel', type=str, default='INFO',
                        help='the loglevel used')

    wrap_parse_args(parser, json_parser)

    return parser
