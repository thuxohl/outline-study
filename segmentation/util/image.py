import math
import random

import numpy as np
import cv2 as cv
import PIL.Image
import torch

class Patches:

    def __init__(self, image, patch_size, stride, shuffle=False):
        assert isinstance(image, np.ndarray) or isinstance(image, PIL.Image.Image)

        self.image = image
        self.patch_size = patch_size
        self.stride = stride
        self.shuffle = shuffle
        self.indices = []

        if isinstance(image, np.ndarray):
            width = image.shape[1]
            height = image.shape[0]
        else:
            width = image.size[0]
            height = image.size[1]

        for i in range(int(math.floor(height / self.stride))):
            top = i * self.stride
            if top + self.patch_size > height:
                top = height - self.patch_size

            for j in range(int(math.floor(width / self.stride))):
                left = j * self.stride
                if left + self.patch_size > width:
                    left = width - self.patch_size

                if (top, left) in self.indices:
                    continue

                self.indices.append((top, left))

        if self.shuffle:
            random.shuffle(self.indices)

        self.len = len(self.indices)

    def __iter__(self):
        self.indice_iterator = iter(self.indices)
        return self

    def __next__(self):
        top, left = next(self.indice_iterator)
        bottom = top + self.patch_size
        right = left + self.patch_size
        if isinstance(self.image, np.ndarray):
            crop = self.image[top:bottom, left:right]
        else:
            crop = self.image.crop((left, top, right, bottom))
        return crop, (top, left)

    def __len__(self):
        return self.len

