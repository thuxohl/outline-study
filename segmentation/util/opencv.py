import itertools

import cv2 as cv
import numpy as np

class BoundingBox():
    """
    Helper class for bounding boxes.
    """
    def __init__(self, left, top, width, height):
        """
        Initialize a bounding box with the top-left coordinate
        and its width and height.
        """
        self.left = left
        self.right = left + width

        self.top = top
        self.bottom = top + height

        self.width = width
        self.height = height

    def __str__(self):
        return str(self.left) + ', ' + str(self.top) + ' x ' + \
               str(self.width) + ', ' + str(self.height)

    def __repr__(self):
        return self.__str__()

    def slice(self):
        return (slice(self.top, self.bottom), slice(self.left, self.right))

    def union(self, other):
        """
        Create a union of two bounding boxes.
        """
        left = min(self.left, other.left)
        right = max(self.right, other.right)

        top = min(self.top, other.top)
        bottom = max(self.bottom, other.bottom)

        return BoundingBox(left, top, right - left, bottom - top)

    def intersection(self, other):
        """
        Create the intersection of two bounding boxes.

        raise: ValueError if the intersection is empty
        """
        left = max(self.left, other.left)
        right = min(self.right, other.right)

        top = max(self.top, other.top)
        bottom = min(self.bottom, other.bottom)

        if left >= right or top >= bottom:
            raise ValueError()

        return BoundingBox(left, top, right - left, bottom - top)

    def intersects(self, other):
        try:
            self.intersection(other)
            return True
        except:
            return False

    def tl(self):
        return np.array((self.top, self.left))

    def tr(self):
        return np.array((self.top, self.left + self.width))

    def bl(self):
        return np.array((self.top + self.height, self.left))

    def br(self):
        return np.array((self.top + self.height, self.left + self.width))

    def corner_points(self):
        return [self.tl(), self.tr(), self.br(), self.bl()]

class ConnectedComponent:

    def __init__(self, label, label_img, stats, centroid):
        self.label = label
        self.label_img = label_img == label
        self.stats = stats
        self.centroid = centroid
        self.bounding_box = BoundingBox(*self.stats[:4])

    def intersection_count(self, other):
        bb = self.bounding_box
        bb_other = other.bounding_box

        if not bb.intersects(bb_other):
            return 0

        _slice = bb.union(bb_other).slice()
        return np.logical_and(self.label_img[_slice], other.label_img[_slice]).sum()

    def union_count(self, other):
        bb = self.bounding_box
        bb_other = other.bounding_box

        _slice = bb.union(bb_other).slice()
        return np.logical_or(self.label_img[_slice], other.label_img[_slice]).sum()

    def compute_contour(self):
        contours, _ = cv.findContours(self.label_img[self.bounding_box.slice()].astype(np.uint8),
                                      cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE,
                                      offset=(*np.flip(self.bounding_box.tl()),))

        if len(contours) > 1:
            # sort largest contour (by area) to the front
            contours.sort(lambda contour: -cv.contourArea(contour))
            print(f'ERROR multiple contours ({len(contours)}) for connected component {self.label}')

        return contours[0]

    @classmethod
    def fromContour(cls, contour, label, img_shape):
        """
        Create a connected component from a contour and
        set its label.
        """
        label_img = np.zeros(img_shape, np.uint8)
        cv.drawContours(label_img, [contour], -1, label, -1)

        area = int(label_img.sum() / label)
        stats = np.array([*cv.boundingRect(contour), area], np.int32)
        if contour.shape[-1] == 2:
            # the contour consists of only two points, in this case the
            # moments computation below fails, so compute the center of the 2 points
            # manually
            centroid = np.array([stats[cv.CC_STAT_LEFT] + int(stats[cv.CC_STAT_WIDTH]  / 2),
                                 stats[cv.CC_STAT_TOP]  + int(stats[cv.CC_STAT_HEIGHT] / 2)])
        else:
            _m = cv.moments(contour)
            centroid = np.array([_m['m10'] / _m['m00'], _m['m01'] / _m['m00']])

        return cls(label, label_img, stats, centroid)


def compute_connected_components(img, connectivity=8):
    component_count, label_img, stats, centroids = cv.connectedComponentsWithStats(img, connectivity)

    components = []
    for label in range(1, component_count):
        components.append(ConnectedComponent(label, label_img, stats[label], centroids[label]))

    return components


def MATCH_MAX(cc_pre, cc_ann):
    return cc_pre.intersection_count(cc_ann) == cc_pre.stats[cv.CC_STAT_AREA]

def MATCH_MIN(cc_pre, cc_ann):
    return cc_pre.intersection_count(cc_ann) > 0

def MATCH_75(cc_pre, cc_ann):
    return (cc_pre.intersection_count(cc_ann) / cc_pre.stats[cv.CC_STAT_AREA]) >= 0.75

def CONNECT_BY_BBS(ccs):
    # but all bounding box corners into a list of pts
    pts = list(itertools.chain(*map(lambda cc: cc.bounding_box.corner_points(), ccs)))
    # flip points for indexation in opencv
    pts = np.array(list(map(lambda pt: np.flip(pt), pts)))
    # draw convex hull
    return cv.convexHull(pts)


def CONNECT_BY_CONTOURS(ccs):
    contours = list(map(lambda cc: cc.compute_contour(), ccs))
    contours = list(map(lambda contour: contour[:, 0, :], contours))
    pts = np.concatenate(contours, axis=0)
    return cv.convexHull(pts)


def connect_regions(prediction, annotation, matching_criterium=MATCH_75, connection_method=CONNECT_BY_CONTOURS, ccs_pre=None, ccs_ann=None):
    """
    Connect predicted regions through their convex hull if
    multiple predicted regions match to the same annotated region.
    """

    # compute connected components if not already provided
    if ccs_pre is None:
        ccs_pre = compute_connected_components(prediction)
    if ccs_ann is None:
        ccs_ann = compute_connected_components(annotation)

    region_mapping = {}
    for cc_ann in ccs_ann:
        region_mapping[cc_ann.label] = []
        for cc_pre in ccs_pre:
            if matching_criterium(cc_pre, cc_ann):
                region_mapping[cc_ann.label].append(cc_pre)
    region_mapping = dict(filter(lambda elem: len(elem[1]) > 1, region_mapping.items()))

    for cc_ann_label, ccs in region_mapping.items():
        # connect components as a single contour
        cnt_pre = connection_method(ccs)
        # remove all connected componts used
        for cc in ccs:
            # since there is not 'hard' mapping multiple predicted regions can be mapped
            # to different annotations
            # this check makes sure that the are not removed twice
            if cc in ccs_pre:
                ccs_pre.remove(cc)
        label = min(map(lambda cc: cc.label, ccs))
        # append new connected component
        ccs_pre.append(ConnectedComponent.fromContour(cnt_pre, label, prediction.shape))
        # draw new region onto prediction 
        cv.drawContours(prediction, [cnt_pre], 0, 255, -1)

        cc_ann = list(filter(lambda cc: cc.label == cc_ann_label, ccs_ann))[0]
        cnt_ann = cv.convexHull(cc_ann.compute_contour())
        ccs_ann.remove(cc_ann)
        ccs_ann.append(ConnectedComponent.fromContour(cnt_ann, cc_ann.label, annotation.shape))
        cv.drawContours(annotation, [cnt_ann], 0, 255, -1)

    ccs_ann.sort(key=lambda cc: cc.label)
    ccs_pre.sort(key=lambda cc: cc.label)

    return prediction, annotation

