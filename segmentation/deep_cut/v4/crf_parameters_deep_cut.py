import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm
import denseCRF
from sklearn.tree import DecisionTreeClassifier

import torch
import torch.nn as nn
import torchvision.transforms as T

from sklearn.model_selection import GridSearchCV
import sklearn.metrics as metrics
from segmentation.deep_cut.v2.crf_parameters_initial import CRF, custom_fbeta

import segmentation.deep_cut.v2.transforms as customT
from segmentation.deep_cut.v2.cnn import get_transforms
from segmentation.deep_cut.v4.cnn import SegNet
from segmentation.deep_cut.v4.deep_cut import retrain
from evaluation.util import load_images, load_annotations, load_data


def deepCut(roi, img, mask, iterCount, early_stop_threshold=0.01):
    """
    Executes the deepCut algorithm.
    """
    global args
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # extract the region of interest from the image
    p_1, p_2 = roi
    img = img[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    mask = mask[p_1[1] : p_2[1], p_1[0] : p_2[0]]

    # apply the transformations to the image
    transforms = get_transforms(train=False)
    transformed_img = transforms(img, None)[0]
    transformed_img = transformed_img.to(device)

    # copy the mask to compare the changes between iterations
    mask_copy = np.copy(mask)
    contour = np.copy(mask)
    contour_size = cv.countNonZero(mask)

    # create a foreground model
    fgd_model = SegNet()
    fgd_model.init_layers(0.1)
    fgd_model = fgd_model.to(device)

    optimizer = torch.optim.RMSprop(fgd_model.parameters(), lr=args.learning_rate, weight_decay=1e-8, momentum=0.9)

    predictions = []

    # DeepCut iterations: fixed number or until convergence
    # since the iteration 0 is used for initialization the last iteration is iterCount
    for i in range(iterCount + 1):
        #print(f"Iteration {i} of {iterCount}")
        # ============== #
        # CNN prediction #
        # ============== #

        # use the CNN to predict the classes (foreground and background)
        with torch.no_grad():
            fgd_model.eval()
            if i == 0:
                # use the contour as an initialization
                prediction = (mask // cv.GC_PR_FGD).astype(np.float32)
            else:
                # use the foreground model
                prediction = fgd_model(torch.unsqueeze(transformed_img, dim=0))
                prediction = torch.clip(prediction, 0, 1)
                prediction = torch.squeeze(prediction)
                prediction = prediction.cpu().numpy()
                prediction[contour == 0] = 0

        if i != 0:
            predictions.append(prediction)

        if not prediction.sum() == 0:
            prediction = (prediction - prediction.min()) / (prediction.max() - prediction.min())

        # ============== #
        # CRF refinement #
        # ============== #

        # use a dense CRF to refine the segmentation

        # CRF parameters (optimized using grid search on a training set)
        # appearance kernel
        if i == 0:
            # use different parameters in the first round if the initialization is done by contour + CRF
            # todo: arg for init method
            w1 = 70
            alpha = 75
            beta = 3
        else:
            w1 = 30
            alpha = 70
            beta = 1

        # smoothness kernel
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        # number of iterations
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        # calculate the segmentation
        mask[:] = denseCRF.densecrf(img, np.stack((1 - prediction, prediction), axis=-1), params).astype(np.uint8) * cv.GC_PR_FGD
        mask[contour == 0] = 0

        # prevent shrinking
        s = 3
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
        mask[:] = cv.dilate(mask, kernel)

        # ============== #
        # CNN retraining #
        # ============== #

        # stop due to last iteration
        if i == iterCount:
            return predictions

        # retrain the model
        if i == 0:
            # train for more epochs in the first iteration since the model requires the largest change in this iteration
            retrain(fgd_model, img, mask, optimizer, epochs=75)
        else:
            retrain(fgd_model, img, mask, optimizer, epochs=30)

        # update the copy of the mask that is used for early stopping
        mask_copy = np.copy(mask)

    return predictions


def main():
    global args
    parser = argparse.ArgumentParser('Estimates the parameters of the CRF used in the deepCut algorithm',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('--sample_count', type=int, default=20,
                        help='the number of samples used for the grid search')
    parser.add_argument('--seed', type=int, default=0,
                        help='the seed used to initialize the random number generator')
    parser.add_argument('--beta', type=float, default=1,
                        help='the beta used in the F-beta score')
    parser.add_argument('--n_jobs', type=int, default=1,
                        help='number of parallel jobs used for grid search (-1 means using all cpu cores)')
    parser.add_argument('--learning_rate', type=float, default=5e-4,
                        help='the learning rate during retraining')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "train/images"))
    image_set = set(images.keys())
    gts = load_annotations(d=os.path.join(args.data_dir, "train/annotations"))
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # sample from the participants to get a good representation of different contours
    np.random.seed(args.seed)
    if args.sample_count > len(data.keys()):
        raise RuntimeError("sample_count larger than the number of participants")
    participants = np.random.choice(list(data.keys()), args.sample_count)

    # set up the dataset:
    #     image_patches: list of image patches
    #     deep_cut_masks: list of deepCut masks: [[deepCut mask iteration 1, ..., deepCut mask iteration N]]
    #     gt: list of corresponding ground truth patches
    image_patches = []
    deep_cut_predictions = []
    gt_patches = []

    # iterate over all sampled participants
    for participant in tqdm.tqdm(participants):
        # get a list of available image names
        # (the json file contains images from the train and test set)
        image_names = list(image_set.intersection(data[participant].keys()))

        # sample a single image
        image_name = np.random.choice(image_names, 1)[0]

        # sample a single contour
        contour_name = np.random.choice(list(data[participant][image_name].keys()), 1)[0]
        contour = data[participant][image_name][contour_name]

        # create a mask and draw the contour
        mask = np.zeros(images[image_name].shape[:2], np.uint8)
        cv.drawContours(mask, [np.array(contour)], 0, cv.GC_PR_FGD, -1)

        # only work on the local area to improve runtime
        rect = cv.boundingRect(np.array(contour))
        shape = np.array([rect[2], rect[3]])
        s = 0.3     # size of the border to each side
        p_1 = np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int)
        p_1 = np.clip(p_1, 0, [images[image_name].shape[1], images[image_name].shape[0]])
        p_2 = np.round(p_1 + (1 + 2 * s) * shape).astype(np.int)
        p_2 = np.clip(p_2, 0, [images[image_name].shape[1], images[image_name].shape[0]])

        # execute deepCut
        predictions = deepCut([p_1, p_2], images[image_name], mask, iterCount=10, early_stop_threshold=0)

        image_patches.append(images[image_name][p_1[1] : p_2[1], p_1[0] : p_2[0]])
        deep_cut_predictions.append(predictions)
        gt_patches.append(gts[image_name][p_1[1] : p_2[1], p_1[0] : p_2[0]])


    # execute a grid search to estimate the CRF parameters
    crf = CRF()
    scorer = metrics.make_scorer(custom_fbeta, beta=args.beta)

    parameters = {"w1":[30, 40, 50], "alpha":[10, 20, 30], "beta":[0.1, 0.5, 1]}

    for i in range(len(deep_cut_predictions[0])):
        X = []
        for j in range(len(image_patches)):
            prediction = deep_cut_predictions[j][i]
            prediction = np.stack((np.ones(prediction.shape) - prediction, prediction), axis=-1).astype(np.float32)
            X.append([image_patches[j], prediction])
        Y = gt_patches

        grid = GridSearchCV(crf, parameters, scoring=scorer, n_jobs=args.n_jobs, refit=False)
        grid.fit(X, [y.flatten()==255 for y in Y])

        print("Best hyperparameters:", grid.best_params_)
        print("Best score:", grid.best_score_)

        with open("crf_deep_cut_v4.txt", 'a') as file:
            file.write(f"Iteration {i + 1}\n")
            file.write(f"    Best hyperparameters: {grid.best_params_}\n")
            file.write(f"    Best score: {grid.best_score_}\n\n")

if __name__ == "__main__":
    main()
