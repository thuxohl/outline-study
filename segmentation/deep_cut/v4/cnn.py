import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.nn.functional as F
import segmentation.deep_cut.v2.transforms as T
from evaluation.util import load_images, load_annotations, load_data


class SegNet(nn.Module):
    """
    A simple segmentation network inteded to be used in DeepCut.
    """

    def __init__(self):
        super(SegNet, self).__init__()
        self.conv_init = nn.Conv2d(3, 8, 3, padding=1)
        self.conv_final = nn.Conv2d(8, 1, 1, padding=0)
        self.dropout = nn.Dropout()
        self.batch_norm = nn.BatchNorm2d(8)

    def init_layers(self, std=1):
        for parameter in self.parameters():
            torch.nn.init.normal_(parameter, mean=0.0, std=std)

    def forward(self, x):
        x = self.batch_norm(self.dropout(F.relu(self.conv_init(x))))
        y = self.conv_final(x)
        return y



class SegNet2(nn.Module):
    """
    A simple segmentation network inteded to be used in DeepCut.
    """

    def __init__(self):
        super(SegNet2, self).__init__()

        s = 8

        self.conv_init = nn.Conv2d(3, s, 3, padding=1)

        self.conv_1 = nn.Conv2d(s, 2 * s, 3, padding=1)
        self.pool = nn.MaxPool2d(2, 2, ceil_mode=True)
        self.conv_2 = nn.Conv2d(2 * s, 2 * s, 3, padding=1)

        self.deconv = nn.ConvTranspose2d(2 * s, s, 4, stride=2, padding=1)
        self.conv_3 = nn.Conv2d(2 * s, 2*s, 3, padding=1)

        self.conv_final = nn.Conv2d(2 * s, 1, 1, padding=0)

    def init_layers(self, std=1):
        for parameter in self.parameters():
            torch.nn.init.normal_(parameter, mean=0.0, std=std)

    def forward(self, x):
        d_0 = F.relu(self.conv_init(x))

        d_1 = self.pool(F.relu(self.conv_1(d_0)))
        d_1 = F.relu(self.conv_2(d_1))

        u_0 = self.deconv(d_1)
        # due to rounding errors in the max pooling the dimensions of the upsampled layer might be larger by 1
        u_0 = u_0[:, :, :d_0.shape[2], :d_0.shape[3]]
        u_0 = torch.cat([d_0, u_0], dim=1)
        u_0 = F.relu(self.conv_3(u_0))

        y = self.conv_final(u_0)
        return y


def get_transforms(train):
    transforms = [T.ToTensor()]
    if train:
        transforms.append(T.GaussianNoise(mean=0, std=0.025))
        transforms.append(T.RandomHorizontalFlip())
        transforms.append(T.RandomVerticalFlip())
    transforms.append(T.Normalize())
    return T.Compose(transforms)


def load_images(directory, grayscale=False):
    imgs = []
    flags = cv.IMREAD_GRAYSCALE if grayscale else cv.IMREAD_COLOR
    for filename in sorted(os.listdir(directory)):
        imgs.append(cv.imread(os.path.join(directory, filename), flags))
    return imgs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, help='the path of the directory containing the training data', default="./data/train")
    parser.add_argument('--epoch_count', type=int, help='the number of training epochs', default=30)
    parser.add_argument('-v', '--visualize', action="store_true", help='visualize the resulting segmentation masks')
    args = parser.parse_args()
    print(args)

    imgs = load_images(os.path.join(args.data, "images"))
    gts = load_images(os.path.join(args.data, "annotations"), grayscale=True)

    cnn = SegNet()
    train_transforms = get_transforms(train = True)
    test_transforms = get_transforms(train = False)

    # train on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    eval_losses = []

    for i in range(len(imgs)):
        print(f"Image {i + 1} of {len(imgs)}")

        # train the classifier
        cnn.init_layers(std=0.1)
        optimizer = torch.optim.RMSprop(cnn.parameters(), lr=0.0005, weight_decay=1e-8, momentum=0.9)
        cnn.train()

        training_losses = []

        for e in tqdm.tqdm(range(args.epoch_count)):
            optimizer.zero_grad()

            image, labels = train_transforms(imgs[i], gts[i])
            image = image.to(device)
            labels = torch.squeeze(labels)
            labels = labels.to(device, dtype=torch.float)

            predictions = torch.squeeze(cnn(torch.unsqueeze(image, 0)))

            # since the classes are unbalanced the loss needs to be weighted
            weights = 1 - torch.tensor([np.prod(labels.shape) - torch.sum(labels), torch.sum(labels)], device=device) / np.prod(labels.shape)
            weights = weights[labels.type(torch.long)]
            criterion = nn.BCEWithLogitsLoss(weight=weights)

            loss = criterion(predictions, labels)
            training_losses.append(loss.item())

            loss.backward()
            optimizer.step()

        # make a prediction
        cnn.eval()
        with torch.no_grad():
            image, labels = test_transforms(imgs[i], gts[i])
            image = image.to(device)
            labels = torch.squeeze(labels)
            labels = labels.to(device, dtype=torch.float)

            prediction = torch.squeeze(cnn(torch.unsqueeze(image, 0))).numpy()

            # since the classes are unbalanced the loss needs to be weighted
            weights = 1 - torch.tensor([np.prod(labels.shape) - torch.sum(labels), torch.sum(labels)], device=device) / np.prod(labels.shape)
            weights = weights[labels.type(torch.long)]
            criterion = nn.BCEWithLogitsLoss(weight=weights)
            loss = criterion(predictions, labels)
            eval_losses.append(loss.item())


        if args.visualize:
            # plot the loss over time
            plt.title("binary cross entropy loss")
            plt.plot(np.arange(1, len(training_losses) + 1), training_losses)
            plt.xlabel("epoch")
            plt.ylabel("accuracy")
            plt.show()

            # visualization
            cv.imshow("image", imgs[i])
            cv.imshow("foreground probability", prediction)
            if cv.waitKey(0) == ord('q'):
                cv.destroyAllWindows()
                exit()

    print(f"Average evaluation loss over all images: {np.mean(eval_losses)}")

if __name__ == "__main__":
    main()
