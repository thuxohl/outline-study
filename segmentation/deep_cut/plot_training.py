import argparse

import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Plot the loss and accuracy of a cnn training.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--log_csv', nargs="+", type=str, default=["log.csv"],
                    help='the paths of the logs exported as csv')
parser.add_argument('--labels', nargs="*", type=str,
                    help="the labels provided to the plot of each loss file")
args = parser.parse_args()
print(args)

if args.labels is not None:
    if len(args.labels) != len(args.log_csv):
        print(f"Error: number of provided labels {len(args.labels)} does not match number of log files {len(args.log_csv)}")
        exit(1)
    labels = args.labels
else:
    if len(args.log_csv) == 1:
        labels = [None]
    else:
        labels = args.log_csv


def plot_data(axes, data, label, colors):
    label_train = f"{label} - Training" if label is not None else "Training"
    label_test = f"{label} - Test" if label is not None else "Test"

    axes[0].set_title("Cross Entropy Loss")
    axes[0].plot(data[:, 0], data[:, 1], label=label_train, color=colors[0])
    axes[0].plot(data[:, 0], data[:, 5], label=label_test, color=colors[1], linestyle="--")
    axes[0].set_xlabel("Epoch")
    axes[0].set_ylabel("Loss")
    axes[0].legend()
    axes[0].grid()

    # plot the accuracy
    axes[1].set_title("Accuracy")
    axes[1].plot(data[:, 0], data[:, 2], label=label_train, color=colors[0])
    axes[1].plot(data[:, 0], data[:, 6], label=label_test, color=colors[1], linestyle="--")
    axes[1].set_xlabel("Epoch")
    axes[1].set_ylabel("Accuracy")
    axes[1].legend()
    axes[1].grid()

    # plot the mae
    axes[2].set_title("MAE")
    axes[2].plot(data[:, 0], data[:, 3], label=label_train, color=colors[0])
    axes[2].plot(data[:, 0], data[:, 7], label=label_test, color=colors[1], linestyle="--")
    axes[2].set_xlabel("Epoch")
    axes[2].set_ylabel("MAE")
    axes[2].legend()
    axes[2].grid()

colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple",
          "tab:brown", "tab:pink", "tab:gray", "tab:olive", "tab:cyan"]
fig, axes = plt.subplots(1, 3, figsize=[6 * 3, 4.8])

for i, logfile in enumerate(args.log_csv):
    _label = labels[i]
    _colors = (colors[i], colors[i + 1]) if len(args.log_csv) == 1 else (colors[i], colors[i])

    #[epoch, train_loss, train_acc, train_mae, train_time, test_loss, test_acc, test_mae, test_time]
    data = np.loadtxt(logfile, delimiter=',', skiprows=1)

    plot_data(axes, data, _label, _colors)

plt.show()
