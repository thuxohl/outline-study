# Dokumentation zur interaktiven Segmentierung

## Zielsetzung
In einer Studie wurden Flecken auf Wäschestücken durch eine Umrandung markiert. Es hat sich herausgestellt, dass die Teilnehmenden meist mehrere Flecken umrandeten, da diese oft sehr klein und vielzählig waren. Zudem fiel auf, dass Flecken oft von der Kontur geschnitten wurden und es keinen einheitlichen Abstand zwischen Kontur und Flecken gab.\
Um die Markierungen der Nutzerinnen und Nutzer effektiv verwenden zu können (z.B. als Ground Truth für das Training eines neuronalen Netzwerks), ist es daher notwendig die Annotationen nachzuverarbeiten. Der benötigte Algorithmus zur interaktiven Segmentierung hat folgende Anforderungen:
- Die Eingabe der Teilnehmenden sind Konturen
- Da die Anwendung interaktiv sein soll, muss der Algorithmus eine geringe Laufzeit haben (Ziel: 1 Sekunde)
- Innerhalb einer Kontur können mehrere getrennte Vordergrundregionen liegen
- Innerhalb einer Kontur muss mindestens eine Vordergrundregion erkannt werden
- Sollte es den Nutzern möglich sein Segmentierungen zu verbessern (siehe Nachbearbeitung GrabCut)?
- Da die Annotationen für ein neuronales Netz verwendet werden soll, wird der Recall höher gewertet als die Precision

## Obere Grenze
Um die Qualität der Algorithmen besser einschätzen zu können wurde zunächst eine obere Schranke für die Qualität der Segmentierung berechnet. Hierzu wurden die Bereiche innerhalb der Konturen mit den Ground Truth Werten verundet. Es wurde also angenommen, dass alle Flecken innerhalb einer Kontur perfekt segmentiert werden. Dies lieferte die folgenden Ergebnisse:

| Precision     | Recall        | F_beta |
| ------------- |:-------------:| ------:|
| 1.0000        | 0.8811        | 0.9026 |


## GrabCut
Zunächst wurde der klassische [GrabCut] Algorithmus getestet. Da es zu einem einzelnen Bild meist mehrere Konturen gibt, wurde GrabCut auf einen lokalen Bildausschnitt beschränkt, welcher die Kontur und einen Rand enthielt (Randbreite = 0.1 * Höhe/Breite). Zudem wird hierdurch die Laufzeit stark verkürzt. Die mit GrabCut erstellten Teilmasken wurden verodert, um eine Segmentierung für das gesamte Bild zu erhalten.

Da die klassische Version von GrabCut keine brauchbaren Ergebnisse produzierte, wurde versucht den Kontrast der Bilder durch eine Histogrammäqualisation und eine Gammakorrektur zu erhöhen. Dies führte zu einer geringfügigen Verbesserung, welche jedoch nicht ausreicht um praxistauglich zu sein.

Es fällt auf das GrabCut vor allem mit aufgedruckten Streifen und Falten Probleme hat. Diese unterscheiden sich farblich stark von dem weißen Hintergrund des Wäschestücks. Streifen weisen zudem eine harte Kante zum weißen Untergrund auf, wodurch GraphCut dazu neigt die Segmentierungsgrenze entlang eines Streifens zu ziehen.

| Variante       | Precision | Recall | F_beta |
| -------------- | --------- | ------ | ------ |
| klassisch      | 0.0710    | 0.2475 | 0.1653 |
| hoher Kontrast | 0.0912    | 0.3776 | 0.2320 |


## DeepCut
Der zweite getestete Ansatz ist der [DeepCut] Algorithmus von Rajchl Et al. Der Algorithmus baut auf dem Grundprinzip von GrabCut auf und übernimmt das EM-Schema, ersetzt jedoch die einzelnen Komponenten durch neuere Varianten. Die Gaußschen Mischmodelle von GrabCut wurden durch ein Convolutional Neural Network ersetzt und anstelle des GraphCut Algorithmus wird ein [Conditional Random Field][CRF] verwendet.\
Da die Autoren des DeepCut Papers keine Implementierung zur Verfügung gestellt haben, wurde der Algorithmus nachimplementiert. Hierbei wurden einige Anpassungen an unsere Problemstellung gemacht.

### CNN

- Architektur angepasst, um bessere Ergebnisse zu erzielen: 3 FC Layers, Conv Layers geändert
- Padding
- Data Augmentation: Flips + Gaussian Noise
- Training: Dropout, Mini-batches, Loss, Optimizer, Lernrate, LR Decay
- Ergebnisse

### Laufzeitoptimierung
- Ansätze auflisten

### CRF
- Hyperparameteroptimierung wie im CRF Paper: Grid Search

### Optimierung der Ergebnisse
- Verschiedene Varianten + Ergebnisse listen

## Diskussion
- GrabCut vs DeepCut


[GrabCut]: https://courses.cs.washington.edu/courses/cse455/09wi/readings/p309-rother.pdf
[DeepCut]: https://arxiv.org/pdf/1605.07866.pdf
[CRF]: https://arxiv.org/pdf/1210.5644.pdf
