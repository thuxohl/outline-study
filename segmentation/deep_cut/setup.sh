mkdir -p data/test/images data/test/annotations
cp data/images/0094-V-R-0024.png data/images/0098-V-L-0025.png data/images/0102-V-R-0026.png data/images/0295-V-L-0074.png data/test/images
cp data/annotations/0094-V-R-0024.png data/annotations/0098-V-L-0025.png data/annotations/0102-V-R-0026.png data/annotations/0295-V-L-0074.png data/test/annotations

mkdir -p data/train/images data/train/annotations
cp data/images/0033-V-L-0009.png data/images/0053-V-R-0014.png data/images/0076-R-L-0019.png data/images/0049-V-L-0013.png data/images/0065-V-R-0017.png data/images/0098-V-R-0025.png data/train/images
cp data/annotations/0033-V-L-0009.png data/annotations/0053-V-R-0014.png data/annotations/0076-R-L-0019.png data/annotations/0049-V-L-0013.png data/annotations/0065-V-R-0017.png data/annotations/0098-V-R-0025.png data/train/annotations
