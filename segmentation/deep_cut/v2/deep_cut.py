import argparse
import math
import sys
import os
import time
import copy

import numpy as np
import cv2 as cv
import tqdm
import denseCRF

import torch
import torch.nn as nn
import torchvision.transforms as T

import segmentation.deep_cut.v2.transforms as customT
from segmentation.deep_cut.v2.cnn import SegNet, get_transforms
from segmentation.deep_cut.v2.cnn_simple import SimpleSegNet, SimpleSegNet2
from evaluation.util import load_images, load_annotations, load_data


def retrain(model, img, mask, optimizer, epochs=5):
    """
    Retrains the CNN with new data.

    Args:
        model: the CNN that needs to be retrained
        img: the image on which to train
        mask: the current segmentation mask used as ground truth
        optimizer: the optimizer used to train the model
        epochs: the number of training epochs
    """
    global args
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    transforms = get_transforms(train=True)
    model.train()

    # create the loss function and the optimizer
    mask = mask // cv.GC_PR_FGD
    fgd_count = np.sum(mask)
    sample_count = np.prod(mask.shape)
    weights = 1 - np.array([sample_count - fgd_count, fgd_count]) / sample_count
    weights[0] *= 2
    #weights[1] *= 2
    weights = weights[mask]
    weights = torch.tensor(weights, device=device)
    criterion = nn.BCEWithLogitsLoss(weight=weights)

    # retrain the model for a given number of epochs
    for i in range(epochs):
        optimizer.zero_grad()

        # create a new training sample by using data augmentation
        sample, label = transforms(img, mask * 255)
        sample = sample.to(device)
        label = torch.squeeze(label.to(device, dtype=torch.float))

        # make a prediction and calculate the loss
        prediction = model(torch.unsqueeze(sample, dim=0))
        prediction = torch.squeeze(prediction)

        loss = criterion(prediction, label)
        nn.utils.clip_grad_value_(model.parameters(), 0.1)

        # backpropagation
        loss.backward()
        optimizer.step()
    model.eval()


def deepCut(roi, img, mask, model, iterCount, downscale=2, early_stop_threshold=0.01):
    """
    Executes the deepCut algorithm.

    Args:
        roi: the region on which deepCut is executed. Using only parts of the image improves the runtime.
        img: the image on which deepCut is executed
        mask: mask marking the region that is possibly foreground (gets overwritten and contains the result)
        iterCount: maximum number of deepCut iterations
        # todo
        downscale: currently unused (the factor by which the image is downscaled to improve runtime)
        early_stop_threshold: stop deepCut early if the number of pixels which changed classes between iterations
            is smaller then early_stop_threshold times the number of pixels within the original contour

    Returns:
        the number of iterations needed
    """

    global args
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # extract the region of interest from the image
    p_1, p_2 = roi
    img = img[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    mask = mask[p_1[1] : p_2[1], p_1[0] : p_2[0]]

    # apply the transformations to the image
    transforms = get_transforms(train=False)
    transformed_img = transforms(img, None)[0]
    transformed_img = transformed_img.to(device)

    # copy the mask to compare the changes between iterations
    mask_copy = np.copy(mask)
    contour = np.copy(mask)
    contour_size = cv.countNonZero(mask)

    # create a foreground model
    fgd_model = SimpleSegNet()
    fgd_model.load_state_dict(torch.load("models/simple_segnet.pth", map_location=device))
    fgd_model = fgd_model.to(device)
    fgd_model.freeze_batch_norm()

    #optimizer = torch.optim.Adagrad(fgd_model.parameters(), lr=args.learning_rate)
    optimizer = torch.optim.RMSprop(fgd_model.parameters(), lr=args.learning_rate, weight_decay=1e-8, momentum=0.9)
    #optimizer = torch.optim.Adam(fgd_model.parameters(), lr=args.learning_rate)
    #optimizer = torch.optim.SGD(fgd_model.parameters(), lr=args.learning_rate)

    # DeepCut iterations: fixed number or until convergence
    for i in range(iterCount + 1):
        #print(f"Iteration {i} of {iterCount}")
        # ============== #
        # CNN prediction #
        # ============== #

        # use the CNN to predict the classes (foreground and background)
        # Todo: scaling still relevant?
        # scale the mask down to improve runtime
        #resized_mask = cv.resize(mask, dsize=(0, 0), fx=1/downscale, fy=1/downscale)
        with torch.no_grad():
            fgd_model.eval()
            if i == 0:
                # 4 different options for the first epoch:
                #       1. use the complex segmentation model that was trained for presegmentation + CRF
                #prediction = model(torch.unsqueeze(transformed_img, dim=0))
                #       2. use the contour + CRF
                prediction = torch.tensor(mask).type(torch.float32)
                #       3. use the simple pretrained segmentation model + CRF
                #prediction = fgd_model(torch.unsqueeze(transformed_img, dim=0))
                fgd_model.noise_layers(std=0.65)
                #       4. no initialization
                # todo

                if args.visualize:
                    cv.imshow("prediction of the noised network", torch.squeeze(torch.clip(fgd_model(torch.unsqueeze(transformed_img, dim=0)), 0, 1)).detach().numpy())
            else:
                # use the foreground model
                prediction = fgd_model(torch.unsqueeze(transformed_img, dim=0))
        prediction = torch.clip(prediction, 0, 1)
        prediction = torch.squeeze(prediction)

        # debug: print the number of changed pixels
        """
        if i == 0:
            prediction_last = prediction.numpy()
        print(np.sum(prediction.numpy() - prediction_last) / np.prod(prediction.shape))
        prediction_last = np.copy(prediction)
        #"""

        prediction = prediction.cpu().numpy()
        prediction[contour == 0] = 0

        if args.visualize:
            cv.imshow("Raw prediction", prediction)

        s = 15
        #prediction = cv.GaussianBlur(prediction, (s, s), 0)
        #prediction *= 1.5
        if not prediction.sum() == 0:
            prediction = (prediction - prediction.min()) / (prediction.max() - prediction.min())
        #prediction = np.clip(prediction, 0, 0.9)

        # todo: scaling still required?
        # upscale the mask after the prediction (uses bilinear interpolation)
        #prediction = cv.resize(prediction, dsize=mask_copy.shape[::-1])

        # ============== #
        # CRF refinement #
        # ============== #

        # use a dense CRF to refine the segmentation

        # CRF parameters (optimized using grid search on a training set)
        # appearance kernel
        if i == 0:
            # use different parameters in the first round if the initialization is done by contour + CRF
            # todo: arg for init method
            w1 = 30
            alpha = 110
            beta = 1
        else:
            """
            w1    = 30   # weight of appearance kernel
            alpha = 20   # spatial std (appearance)
            beta  = 1    # rgb std
            """
            w1 = 30
            alpha = 50
            beta = 1

        # smoothness kernel
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        # number of iterations
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        # calculate the segmentation
        mask[:] = denseCRF.densecrf(img, np.stack((1 - prediction, prediction), axis=-1), params).astype(np.uint8) * cv.GC_PR_FGD
        mask[contour == 0] = 0

        # ============== #
        # CNN retraining #
        # ============== #

        # stop due to last iteration
        if i == iterCount:
            return i + 1

        # stop early if there is only little change since the last iteration
        if cv.countNonZero(mask_copy - mask) < early_stop_threshold * contour_size:
            return i + 1

        """
        # smooth the output mask
        # fill small holes
        s = 3
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
        mask = cv.dilate(mask, kernel)
        # remove small areas
        s = 5
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
        mask = cv.erode(mask, kernel)
        # bring the stain size back to the original size
        s = 3
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
        mask = cv.dilate(mask, kernel)
        #"""

        # retraining model
        if args.visualize:
            cv.imshow("img", img)
            cv.imshow("mask", mask*255)
            cv.imshow("processed prediction", prediction)
            key = cv.waitKey(0)
            if key == ord('q'):
                cv.destroyAllWindows()
                exit()
            elif key == ord('n'):
                return i + 1

        if i == 0:
            # train for more epochs in the first iteration since the model requires the largest change in this iteration
            retrain(fgd_model, img, mask, optimizer, epochs=30)
        else:
            retrain(fgd_model, img, mask, optimizer, epochs=15)

        # update the copy of the mask that is used for early stopping
        mask_copy = np.copy(mask)

    return iterCount


def main():
    global args
    parser = argparse.ArgumentParser('Uses the DeepCut algorithm to improve the annotations from the user study',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--model', type=str, help='the path of the segmentation network')
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('-o', '--output_dir', type=str, default='./data/segmentation/deepCut',
                        help='the directory to which the segmentation masks get exported')
    parser.add_argument('--batch_size', type=int, help='the number of samples used in a mini-batch', default=1)
    parser.add_argument('-v', '--visualize', action="store_true",
                        help='visualize the resulting segmentation masks')
    parser.add_argument('--deepcut_log', type=str, default=None,
                        help='where to store the log containing the iterations and execution time of the deepCut calls (needs to be a csv file)')
    parser.add_argument('--learning_rate', type=float, default=1e-5,
                        help='the learning rate during retraining')
    parser.add_argument('--downscale', type=int, default=2,
                        help='the factor by which deepCut scales the images down to improve the runtime')
    parser.add_argument('--prediction_interpolation', type=int, default=2,
                        help='the factor by which the predictions are interpolated to improve the runtime\n'
                        + '(e.g. only predict every second pixel and interpolate the rest)')
    args = parser.parse_args()
    print(args)


    # process the data on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "images"))
    gt = load_annotations(d=os.path.join(args.data_dir, "annotations")) if args.visualize else None
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # create the output directory and open the output file
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)
    mapping_file = open(os.path.join(args.output_dir, "mapping.txt"), 'w')

    # initialize the CNN
    if args.model:
        net = SegNet()
        net = net.to(device)
        net.load_state_dict(torch.load(args.model))
        net.eval()
    else:
        net = None

    # create an empty list for the deepCut statistics
    statistics = []

    # debug: only evaluate on train or test set
    test_set = os.listdir(os.path.join(args.data_dir, "test", "images"))
    test_set = [name.split('.')[0] for name in test_set]
    train_set = os.listdir(os.path.join(args.data_dir, "train", "images"))
    train_set = [name.split('.')[0] for name in train_set]

    # iterate over all participants
    for i, participant in enumerate(tqdm.tqdm(data)):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():
            # debug: only use the test set
            if image_name in train_set:
                pass
                #continue

            # create a new segmentation mask
            segmentation = np.zeros(images[image_name].shape[:2], np.uint8)

            # iterate over all contours for the current participant and image
            for contour in data[participant][image_name].values():
                mask = np.zeros(segmentation.shape, np.uint8)
                # draw the contour onto the mask
                cv.drawContours(mask, [np.array(contour)], 0, cv.GC_PR_FGD, -1)

                # only work on the local area to improve runtime
                rect = cv.boundingRect(np.array(contour))
                shape = np.array([rect[2], rect[3]])
                s = 0.3     # size of the border to each side
                p_1 = np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int)
                p_1 = np.clip(p_1, 0, [images[image_name].shape[1], images[image_name].shape[0]])
                p_2 = np.round(p_1 + (1 + 2 * s) * shape).astype(np.int)
                p_2 = np.clip(p_2, 0, [images[image_name].shape[1], images[image_name].shape[0]])

                # execute deepCut
                start = time.time()     # measure the time for the statistics
                it = deepCut([p_1, p_2], images[image_name], mask, net, iterCount=10, downscale=args.downscale, early_stop_threshold=0.005)

                statistics.append([len(statistics), it, time.time() - start])   # add the id, the number of iterations and the time to the statistics
                mask = np.isin(mask, [cv.GC_FGD, cv.GC_PR_FGD]).astype(np.uint8) * 255

                # add the result to the segmentation
                segmentation = cv.bitwise_or(segmentation, mask)

            # export the segmentation mask and add it to the mapping file
            filename = f"{args.output_dir}/participant_{participant}_{image_name}.png"
            cv.imwrite(filename, segmentation)
            mapping_file.write(f"{args.data_dir}/annotations/{image_name}.png {filename}\n")

            # visualization
            if args.visualize:
                contour_img = np.copy(images[image_name])
                # draw the contours onto the image
                cv.drawContours(contour_img, [np.array(x) for x in data[participant][image_name].values()], -1, (255, 0, 0), 2)
                #cv.imshow(f"participant_{participant}_{image_name}", img)
                #cv.imshow("segmentation", segmentation)

                # compare the segmentation to the ground truth
                img = np.copy(contour_img)
                overlay = np.stack((np.zeros(img.shape[:2]), gt[image_name], segmentation), axis=-1)
                img[np.any(overlay != 0, axis=2)] = overlay[np.any(overlay != 0, axis=2)]
                cv.imshow(f"participant_{participant}_{image_name}_gt_comparison", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_gt_comparison", 100, 0)

                # visualize the segmentation by darkening unselected areas
                img = np.copy(contour_img)
                img[segmentation == 0] = np.round(0.3 * img[segmentation == 0]).astype(np.uint8)
                cv.imshow(f"participant_{participant}_{image_name}_segmentation", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_segmentation", img.shape[1] + 100, 0)

                # wait for the user to close the window
                # quit if the user pressed 'q'
                if cv.waitKey(0) == ord('q'):
                    cv.destroyAllWindows()

                    # close the mapping file
                    mapping_file.close()

                    # export the deepCut statistics
                    if args.deepcut_log:
                        np.savetxt(args.deepcut_log, statistics, fmt='%f', delimiter=',', header="call_id,iterations,time", comments='')
                    exit()
                cv.destroyAllWindows()

    # close the mapping file
    mapping_file.close()

    # export the deepCut statistics
    if args.deepcut_log:
        np.savetxt(args.deepcut_log, statistics, fmt='%f', delimiter=',', header="call_id,iterations,time", comments='')

    cv.destroyAllWindows()

if __name__ == "__main__":
    main()
