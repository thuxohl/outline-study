import numpy as np
import torch
import torchvision.transforms.functional as F

class ToTensor(object):
    def __call__(self, image, target):
        image = F.to_tensor(image)
        if not target is None:
            target = F.to_tensor(target).type(torch.long)
        return image, target


class GaussianNoise(object):
    def __init__(self, mean=0, std=1):
        self.mean = mean
        self.std = std

    def __call__(self, image, target):
        image = image + torch.randn(image.size()) * self.std + self.mean
        return torch.clip(image, 0, 255), target


class RandomHorizontalFlip(object):
    def __init__(self, prob=0.5):
        self.prob = prob

    def __call__(self, image, target):
        if torch.rand(1).item() < self.prob:
            image = F.hflip(image)
            if not target is None:
                target = F.hflip(target)

        return image, target


class RandomVerticalFlip(object):
    def __init__(self, prob=0.5):
        self.prob = prob

    def __call__(self, image, target):
        if torch.rand(1).item() < self.prob:
            image = F.vflip(image)
            if not target is None:
                target = F.vflip(target)

        return image, target


class Normalize(object):
    def __call__(self, image, target):
        return (image - torch.mean(image)) / torch.std(image), target


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, target):
        for t in self.transforms:
            image, target = t(image, target)
        return image, target
