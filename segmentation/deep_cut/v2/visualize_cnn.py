import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm
import denseCRF

from segmentation.deep_cut.v2.cnn import *


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, required=True, help='the path of the segmentation network')
    parser.add_argument('-d', '--data', type=str, help='the path of the directory containing the image data', default="./data/train")
    parser.add_argument('-s', '--shuffle', help='display the images in a random order', action="store_true")
    args = parser.parse_args()
    print(args)

    # use a GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # load the cnn
    #net = SegNet()
    net = SimpleSegNet()
    net = net.to(device)
    net.load_state_dict(torch.load(args.model))

    transforms = get_transforms(train=False)

    # get a list of available images
    images = os.listdir(os.path.join(args.data, "images"))
    if args.shuffle:
        np.random.shuffle(images)

    # iterate over the image list and show the resulting segmentation
    for file in images:
        img = cv.imread(os.path.join(args.data, "images", file))
        gt = cv.imread(os.path.join(args.data, "annotations", file), cv.IMREAD_GRAYSCALE)

        with torch.no_grad():
            prediction = net(torch.unsqueeze(transforms(img, None)[0], dim=0))
            prediction = torch.squeeze(prediction)
            #prediction = torch.nn.Sigmoid()(prediction)

        #cv.imshow("transformed img", transforms(img, None)[0].permute([1, 2, 0]).numpy())
        cv.imshow("img", img)
        cv.imshow("seg", prediction.numpy().astype(np.float))
        cv.imshow("network results: seg: blue, gt: red", np.stack((prediction.numpy().astype(np.float), np.zeros(gt.shape), gt), axis=-1))

        # refine the segmentation using a crf
        # appearance kernel
        w1    = 5   # weight of appearance kernel
        alpha = 70   # spatial std (appearance)
        beta  = 4.5  # rgb std
        # smoothness kernel
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        # number of iterations
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        p = prediction
        #p = torch.nn.Sigmoid()(prediction)
        p = p.numpy().astype(np.float)
        seg = denseCRF.densecrf(img.astype(np.uint8), np.stack((np.ones(p.shape) - p, p), axis=-1).astype(np.float32), params).astype(np.uint8)
        cv.imshow("CRF results: seg: blue, gt: red", np.stack((seg, np.zeros(p.shape), gt), axis=-1))

        if cv.waitKey(0) == ord('q'):
            cv.destroyAllWindows()
            exit()

if __name__ == "__main__":
    main()
