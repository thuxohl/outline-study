import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm
import denseCRF

import torch
import torch.nn as nn
import torch.nn.functional as F
import segmentation.deep_cut.v2.transforms as T
from torchsummary import summary

def load_images(d, grayscale=False):
    res = []
    flags = cv.IMREAD_GRAYSCALE if grayscale else cv.IMREAD_COLOR
    for filename in sorted(os.listdir(d)):
        res.append(cv.imread(os.path.join(d, filename), flags))
    return res


class Dataset(object):
    def __init__(self, data_dir, transforms):
        self.data_dir = data_dir
        self.transforms = transforms
        self.imgs = load_images(os.path.join(data_dir, "images"))
        self.gts = load_images(os.path.join(data_dir, "annotations"), grayscale=True)


    def __getitem__(self, idx):
        return self.transforms(self.imgs[idx], self.gts[idx])


    def __len__(self):
        return len(self.imgs)


class Downsample(nn.Module):
    """
    A downsampling layer consisting of a convolution followed by dropout, batch normalization and max pooling.
    """
    def __init__(self, in_channels, out_channels, kernel_size=3, use_dropout=True):
        """
        Args:
            in_channels: the number of channels of the input layer
            out_channels: the number of channels of the output layer (usually twice the number of input channels)
            kernel_size: the size of the convolution kernel
        """
        super(Downsample, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size, padding=(kernel_size - 1) // 2)
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size, padding=(kernel_size - 1) // 2)
        self.dropout = nn.Dropout() if use_dropout else nn.Identity()
        self.batch_norm = nn.BatchNorm2d(out_channels)
        self.pool = nn.MaxPool2d(2, 2, ceil_mode=True)

    def forward(self, x):
        x = self.batch_norm(self.dropout(F.relu(self.conv1(x))))
        x = self.pool(self.batch_norm(self.dropout(F.relu(self.conv2(x)))))
        return x


class Upsample(nn.Module):
    """
    An upsampling layer that uses a deconvolution to upsample x_1.
    Afterwards x_1 and x_2 are concatenated and fed into a convolution layer followed by dropout and batch normalization.
    """

    def __init__(self, in_channels, out_channels, kernel_size=3, use_dropout=True):
        """
        Args:
            in_channels: the number of channels of the input layer
            out_channels: the number of channels of the output layer (required to be twice the number of input channels)
            kernel_size: the size of the convolution kernel (the size of the deconvolution kernel is fixed)
        """
        super(Upsample, self).__init__()
        self.deconv = nn.ConvTranspose2d(in_channels, in_channels // 2, 4, stride=2, padding=1)
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, padding=(kernel_size - 1) // 2)
        self.dropout = nn.Dropout() if use_dropout else nn.Identity()
        self.batch_norm = nn.BatchNorm2d(out_channels)

    def forward(self, x_1, x_2):
        upsampled = self.deconv(x_1)
        # due to rounding errors in the max pooling the dimensions of the upsampled layer might be larger by 1
        upsampled = upsampled[:, :, :x_2.shape[2], :x_2.shape[3]]
        upsampled = torch.cat([x_2, upsampled], dim=1)
        upsampled = self.batch_norm(self.dropout(F.relu(self.conv(upsampled))))
        return upsampled


class SegNet(nn.Module):
    """
    A fully convolutional segmentation network similar to the approach in 'U-Net: Convolutional Networks for Biomedical Image Segmentation'.
    (https://arxiv.org/pdf/1505.04597.pdf)
    The implementation is derived from https://github.com/milesial/Pytorch-UNet.
    """

    def __init__(self):
        super(SegNet, self).__init__()

        self.conv_init = nn.Conv2d(3, 16, 3, padding=1)
        self.dropout = nn.Dropout()
        self.bn16 = nn.BatchNorm2d(16)

        self.down_1 = Downsample(16, 32)
        self.down_2 = Downsample(32, 64)
        self.down_3 = Downsample(64, 128)
        self.down_4 = Downsample(128, 256)
        self.down_5 = Downsample(256, 512)

        self.conv_low = nn.Conv2d(512, 512, 3, padding=1)
        self.bn512 = nn.BatchNorm2d(512)

        self.up_5 = Upsample(512, 256)
        self.up_4 = Upsample(256, 128)
        self.up_3 = Upsample(128, 64)
        self.up_2 = Upsample(64, 32)
        self.up_1 = Upsample(32, 32)

        self.conv_final = nn.Conv2d(32, 1, 1, padding=0)


    def init_layers(self, std=1):
        for parameter in self.parameters():
            torch.nn.init.normal_(parameter, mean=0.0, std=std)


    def forward(self, x):
        # initial convolution
        d0 = self.bn16(self.dropout(F.relu(self.conv_init(x))))

        # downsampling
        d1 = self.down_1(d0)
        d2 = self.down_2(d1)
        d3 = self.down_3(d2)
        d4 = self.down_4(d3)
        d5 = self.down_5(d4)

        # add an extra convolution on the lowest layer, that isn't downscaled further
        d5 = self.bn512(self.dropout(F.relu(self.conv_low(d5))))

        # upsampling
        u4 = self.up_5(d5, d4)
        u3 = self.up_4(u4, d3)
        u2 = self.up_3(u3, d2)
        u1 = self.up_2(u2, d1)
        u0 = self.up_1(u1, d0)

        # final convolution
        y = self.conv_final(u0)
        return y


def get_transforms(train):
    transforms = [T.ToTensor()]
    if train:
        transforms.append(T.GaussianNoise(mean=0, std=0.025))
        transforms.append(T.RandomHorizontalFlip())
        transforms.append(T.RandomVerticalFlip())
    transforms.append(T.Normalize())
    return T.Compose(transforms)


def train_one_epoch(net, optimizer, data_loader, criterion, device):
    net.train()

    epoch_loss = 0
    sample_count = 0
    for i, batch in enumerate(data_loader):
        optimizer.zero_grad()

        images, labels = batch
        images = images.to(device)
        labels = torch.squeeze(labels)
        labels = labels.to(device, dtype=torch.float)

        predictions = torch.squeeze(net(images))

        # since the classes are unbalanced the loss needs to be weighted
        weights = 1 - torch.tensor([np.prod(labels.shape) - torch.sum(labels), torch.sum(labels)], device=device) / np.prod(labels.shape)
        #weights[0] *= 1.25
        weights = weights[labels.type(torch.long)]
        #criterion = nn.BCELoss(weight=weights)
        criterion = nn.BCEWithLogitsLoss(weight=weights)

        loss = criterion(predictions, labels)
        epoch_loss += loss.item() * len(batch)
        sample_count += len(batch)

        loss.backward()
        #nn.utils.clip_grad_value_(net.parameters(), 0.1)       # clipping the gradient seems to reduce the networks performance
        optimizer.step()

    epoch_loss /= sample_count
    return epoch_loss, 0, 0


@torch.no_grad()
def evaluate(net, data_loader, criterion, device):
    net.eval()

    eval_loss = 0
    sample_count = 0
    for i, batch in enumerate(data_loader):
        images, labels = batch
        images = images.to(device)
        labels = torch.squeeze(labels)
        labels = labels.to(device, dtype=torch.float)

        predictions = torch.squeeze(net(images))

        weights = 1 - torch.tensor([np.prod(labels.shape) - torch.sum(labels), torch.sum(labels)], device=device) / np.prod(labels.shape)
        #weights[0] *= 1.25
        weights = weights[labels.type(torch.long)]
        #criterion = nn.BCELoss(weight=weights)
        criterion = nn.BCEWithLogitsLoss(weight=weights)

        loss = criterion(predictions, labels)
        eval_loss += loss.item() * len(batch)
        sample_count += len(batch)

    eval_loss /= i + 1
    return eval_loss, 0, 0


def log(msg, log_file, newline=True):
    print(msg)
    if log_file is not None:
        if newline:
            log_file.write(msg + "\n")
        else:
            log_file.write(msg)
        log_file.flush()


def main():
    global args
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_train', type=str, help='the path of the directory containing the training data', default="./data/train")
    parser.add_argument('--data_test', type=str, help='the path of the directory containing the test data', default="./data/test")
    parser.add_argument('--model_in', type=str, help='the path of an existing model of which the training should be continued', default=None)
    parser.add_argument('--model_out', type=str, help='directory where the trained models are stored', default="./models")
    parser.add_argument('--log_file', type=str, help='the path of the log file', default=None)
    parser.add_argument('--log_csv', type=str, help='the path of the log exported as csv', default=None)
    parser.add_argument('--epoch_count', type=int, help='the number of training epochs', default=30)
    parser.add_argument('--batch_size', type=int, help='the number of samples used in a mini-batch', default=1)
    args = parser.parse_args()
    print(args)


    # train on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # load or initialize the CNN
    net = SegNet()
    #summary(net, (3, 500, 500))
    net = net.to(device)
    start_epoch = 0
    if args.model_in:
        start_epoch = int(os.path.splitext(args.model_in)[0].split('_')[1])
        net.load_state_dict(torch.load(args.model_in))
    else:
        net.init_layers(std=0.1)

    # open the logfile
    log_csv = []
    log_file = None
    if args.log_file:
        if args.model_in:
            log_file = open(args.log_file, 'a')
        else:
            log_file = open(args.log_file, 'w')

    # use our dataset and defined transformations
    dataset_train = Dataset(args.data_train, get_transforms(train=True))
    dataset_test = Dataset(args.data_test, get_transforms(train=False))

    # create data loaders
    data_loader_train = torch.utils.data.DataLoader(dataset_train, batch_size=args.batch_size, shuffle=True, num_workers=1, drop_last=False)
    data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=args.batch_size, shuffle=True, num_workers=1, drop_last=False)

    # create the optimizer
    #optimizer = torch.optim.Adagrad(net.parameters(), lr=0.015)
    optimizer = torch.optim.RMSprop(net.parameters(), lr=0.0005, weight_decay=1e-8, momentum=0.9)
    #optimizer = torch.optim.SGD(net.parameters(), lr=0.005)

    # create a scheduler for the learning rate
    #lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.975)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', patience=2)

    # create the loss function
    criterion = nn.BCELoss()

    # train the CNN
    for epoch in range(args.epoch_count):
        # train the network for a single epoch
        start = time.time()
        train_loss, train_acc, train_mae = train_one_epoch(net, optimizer, data_loader_train, criterion, device)
        train_time = time.time() - start
        log("Epoch (%i/%i):" % (start_epoch + epoch + 1, start_epoch + args.epoch_count), log_file)
        log("      Training:   average_cross_entropy_loss: %.5f, average_mae: %.5f, average_accuracy: %.5f, time: %.5f" % (train_loss, train_mae, train_acc, train_time), log_file)

        # evaluate the network on the test set
        start = time.time()
        test_loss, test_acc, test_mae = evaluate(net, data_loader_test, criterion, device)
        test_time = time.time() - start
        log("      Evaluation: average_cross_entropy_loss: %.5f, average_mae: %.5f, average_accuracy: %.5f, time: %.5f" % (test_loss, test_mae, test_acc, test_time), log_file)

        # update the learning rate
        #lr_scheduler.step()
        lr_scheduler.step(test_loss)

        if args.log_csv:
            log_csv.append([start_epoch + epoch + 1, train_loss, train_acc, train_mae, train_time, test_loss, test_acc, test_mae, test_time])

        # export the CNN
        if ((epoch + 1) % 5) == 0 and args.model_out:
            torch.save(net.state_dict(), os.path.join(args.model_out, 'cnn_' + str(start_epoch + epoch + 1) + '.pth'))

    # export the final CNN if not done yet
    if args.model_out and not os.path.isfile(os.path.join(args.model_out, 'cnn_' + str(start_epoch + args.epoch_count) + '.pth')):
        torch.save(net.state_dict(), os.path.join(args.model_out, 'cnn_' + str(start_epoch + args.epoch_count) + '.pth'))

    # export the log
    if args.log_csv:
        header = ""
        if args.model_in:
            f = open(args.log_csv, 'a')
        else:
            header = "epoch,train_loss,train_acc,train_mae,train_time,test_loss,test_acc,test_mae,test_time"
            f = open(args.log_csv, 'w')
        np.savetxt(f, log_csv, delimiter=',', header=header, comments='')
        f.flush()
        f.close()

    if args.log_file:
        log_file.close()

if __name__ == "__main__":
    main()
