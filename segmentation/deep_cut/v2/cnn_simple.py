import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm
import denseCRF

import torch
import torch.nn as nn
import torch.nn.functional as F
import segmentation.deep_cut.v2.transforms as T
from segmentation.deep_cut.v2.cnn import Downsample, Upsample, get_transforms, train_one_epoch, evaluate, log
from torchsummary import summary
from evaluation.util import load_images, load_annotations, load_data


class Dataset(object):
    def __init__(self, data_dir, contour_path, transforms):
        self.transforms = transforms
        self.imgs = load_images(d=os.path.join(data_dir, "images"))
        self.gts = load_annotations(d=os.path.join(data_dir, "annotations"))
        contour_data = load_data(filename=contour_path)

        self.patches = []
        # process and filter the contour data
        for participant in contour_data:
            for image_name in contour_data[participant].keys():
                # the contour data contains images that are not contained in the current subset
                if image_name not in self.imgs.keys():
                    continue

                # extract patches from the contour
                for contour in contour_data[participant][image_name].values():
                    # convert the contour to a numpy array and remove axes of length one
                    contour = np.squeeze(np.array(contour))

                    x_y_min = np.min(contour, axis=0)
                    x_y_min = np.clip(x_y_min, a_min=0, a_max=self.gts[image_name].shape[::-1])

                    x_y_max = np.max(contour, axis=0)
                    x_y_max = np.clip(x_y_max, a_min=0, a_max=self.gts[image_name].shape[::-1])

                    # append the bounding rectangle as a new patch ([image_name, [x_min, y_min, x_max, y_max]])
                    self.patches.append([image_name, np.concatenate((x_y_min, x_y_max))])


    def __getitem__(self, idx):
        name = self.patches[idx][0]
        x_min, y_min, x_max, y_max = self.patches[idx][1]

        img = self.imgs[name]
        img = img[y_min : y_max, x_min : x_max]

        gt = self.gts[name]
        gt = gt[y_min : y_max, x_min : x_max]

        return self.transforms(img, gt)


    def __len__(self):
        return len(self.patches)


class SimpleSegNet(nn.Module):
    """
    A much simpler and shallower version of the segmentation network inteded for usage in DeepCut.
    """

    def __init__(self):
        super(SimpleSegNet, self).__init__()

        use_dropout = True
        self.conv_init = nn.Conv2d(3, 8, 3, padding=1)
        self.dropout = nn.Dropout() if use_dropout else nn.Identity()
        self.bn8 = nn.BatchNorm2d(8)

        self.down_1 = Downsample(8, 16, kernel_size=5, use_dropout=use_dropout)
        self.down_2 = Downsample(16, 32, kernel_size=5, use_dropout=use_dropout)
        self.down_3 = Downsample(32, 64, kernel_size=5, use_dropout=use_dropout)

        self.conv_low = nn.Conv2d(64, 64, 5, padding=2)
        self.bn32 = nn.BatchNorm2d(64)

        self.up_3 = Upsample(64, 32, use_dropout=use_dropout)
        self.up_2 = Upsample(32, 16, use_dropout=use_dropout)
        self.up_1 = Upsample(16, 8, use_dropout=use_dropout)

        self.conv_final = nn.Conv2d(8, 1, 1, padding=0)


    def init_layers(self, std=1):
        for parameter in self.parameters():
            torch.nn.init.normal_(parameter, mean=0.0, std=std)


    def reset_layers(self, mean=0, std=1):
        """
        Resets the weights of all layers except the initial convolution layer and the first downsampling layer.
        """
        """
        for parameter in self.down_2.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
        for parameter in self.down_3.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
        for parameter in self.conv_low.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
            """
        for parameter in self.up_3.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
        for parameter in self.up_2.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
        for parameter in self.up_1.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
        for parameter in self.conv_final.parameters():
            torch.nn.init.normal_(parameter, mean=mean, std=std)
        """
            """

    def freeze_batch_norm(self):
        """
        for name, parameter in self.named_parameters():
            if "batch_norm" in name:
                parameter.requires_grad = False
        """

        # deactivate the running mean and variance
        for name, module in self.named_modules():
            if "bn" in name or "batch_norm" in name:
                module.track_running_stats = False


    def noise_layers(self, std=1):
        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        with torch.no_grad():
            for name, parameter in self.named_parameters():
                if "bn" in name or "batch_norm" in name:
                    continue
                shape = parameter.shape
                noise = torch.normal(mean=0, std=std, size=shape).to(device)
                parameter += noise


    def get_weight_distribution(self):
        parameter_list = []
        for name, parameter in self.named_parameters():
            print(name)
            print(np.mean(parameter.detach().numpy()))
            print(np.std(parameter.detach().numpy()))
            parameter_list.extend(parameter.detach().numpy().flatten())

        print(np.mean(parameter_list))
        print(np.std(parameter_list))

        parameter_list = []
        for parameter in self.conv_final.parameters():
            parameter_list.extend(parameter.detach().numpy().flatten())

        print(np.mean(parameter_list))
        print(np.std(parameter_list))
        exit()


    def forward(self, x):
        # initial convolution
        d0 = self.bn8(self.dropout(F.relu(self.conv_init(x))))

        # downsampling
        d1 = self.down_1(d0)
        d2 = self.down_2(d1)
        d3 = self.down_3(d2)

        # add an extra convolution on the lowest layer, that isn't downscaled further
        d3 = self.bn32(self.dropout(F.relu(self.conv_low(d3))))

        # upsampling
        u2 = self.up_3(d3, d2)
        u1 = self.up_2(u2, d1)
        u0 = self.up_1(u1, d0)

        # final convolution
        y = self.conv_final(u0)
        return y


class SimpleSegNet2(nn.Module):
    """
    A much simpler and shallower version of the segmentation network inteded for used in DeepCut.
    """

    def __init__(self):
        super(SimpleSegNet2, self).__init__()

        self.pool = nn.MaxPool2d(2, 2, ceil_mode=True)
        self.dropout = nn.Dropout()

        self.bn16 = nn.BatchNorm2d(16)
        self.bn32 = nn.BatchNorm2d(32)
        self.bn64 = nn.BatchNorm2d(64)

        self.conv_init = nn.Conv2d(3, 16, 3, padding=1)
        self.conv_d1 = nn.Conv2d(16, 32, 3, padding=1)
        self.conv_d2 = nn.Conv2d(32, 64, 3, padding=1)
        self.conv_low = nn.Conv2d(64, 64, 3, padding=1)

        self.deconv_u2 = nn.ConvTranspose2d(64, 32, 4, stride=2, padding=1)
        self.conv_u2 = nn.Conv2d(64, 32, 3, padding=1)

        self.deconv_u1 = nn.ConvTranspose2d(32, 16, 4, stride=2, padding=1)

        self.conv_final = nn.Conv2d(16, 1, 1, padding=0)


    def init_layers(self, std=1):
        for parameter in self.parameters():
            torch.nn.init.normal_(parameter, mean=0.0, std=std)


    def forward(self, x):
        # initial convolution
        d0 = F.relu(self.bn16(self.dropout(self.conv_init(x))))

        # downsampling
        d1 = self.pool(self.bn32(self.dropout(F.relu(self.conv_d1(d0)))))
        d2 = self.pool(self.bn64(self.dropout(F.relu(self.conv_d2(d1)))))

        # add an extra convolution on the lowest layer, that isn't downscaled further
        d2 = self.bn64(self.dropout(F.relu(self.conv_low(d2))))

        # upsampling
        u1 = self.deconv_u2(d2)
        u1 = u1[:, :, :d1.shape[2], :d1.shape[3]]
        u1 = torch.cat([d1, u1], dim=1)
        u1 = self.bn32(self.dropout(F.relu(self.conv_u2(u1))))

        u0 = self.deconv_u1(u1)
        u0 = u0[:, :, :x.shape[2], :x.shape[3]]

        # final convolution
        y = self.conv_final(u0)
        return y


def main():
    global args
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_train', type=str, help='the path of the directory containing the training data', default="./data/train")
    parser.add_argument('--data_test', type=str, help='the path of the directory containing the test data', default="./data/test")
    parser.add_argument('--participant_data', type=str, help='the json file containing the contours drawn by the participants of the study',
                        default="./data/data_processed.json")
    parser.add_argument('--model_in', type=str, help='the path of an existing model of which the training should be continued', default=None)
    parser.add_argument('--model_out', type=str, help='directory where the trained models are stored', default="./models")
    parser.add_argument('--log_file', type=str, help='the path of the log file', default=None)
    parser.add_argument('--log_csv', type=str, help='the path of the log exported as csv', default=None)
    parser.add_argument('--epoch_count', type=int, help='the number of training epochs', default=30)
    args = parser.parse_args()
    print(args)


    # train on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # load or initialize the CNN
    net = SimpleSegNet()
    #summary(net, (3, 500, 500))
    net = net.to(device)
    start_epoch = 0
    if args.model_in:
        start_epoch = int(os.path.splitext(args.model_in)[0].split('_')[1])
        net.load_state_dict(torch.load(args.model_in, map_location=device))
        #net.get_weight_distribution()
    else:
        net.init_layers(std=0.1)

    # open the logfile
    log_csv = []
    log_file = None
    if args.log_file:
        if args.model_in:
            log_file = open(args.log_file, 'a')
        else:
            log_file = open(args.log_file, 'w')

    # use our dataset and defined transformations
    dataset_train = Dataset(args.data_train, args.participant_data, get_transforms(train=True))
    dataset_test = Dataset(args.data_test, args.participant_data, get_transforms(train=False))

    # create data loaders
    data_loader_train = torch.utils.data.DataLoader(dataset_train, batch_size=1, shuffle=True, num_workers=1, drop_last=False)
    data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=1, shuffle=True, num_workers=1, drop_last=False)

    # create the optimizer
    #optimizer = torch.optim.Adagrad(net.parameters(), lr=0.015)
    optimizer = torch.optim.RMSprop(net.parameters(), lr=0.0005, weight_decay=1e-8, momentum=0.9)
    #optimizer = torch.optim.SGD(net.parameters(), lr=0.005)

    # create a scheduler for the learning rate
    #lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.975)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'max', patience=2)

    # create the loss function
    criterion = nn.BCELoss()

    # train the CNN
    for epoch in range(args.epoch_count):
        # train the network for a single epoch
        start = time.time()
        train_loss, train_acc, train_mae = train_one_epoch(net, optimizer, data_loader_train, criterion, device)
        train_time = time.time() - start
        log("Epoch (%i/%i):" % (start_epoch + epoch + 1, start_epoch + args.epoch_count), log_file)
        log("      Training:   average_cross_entropy_loss: %.5f, average_mae: %.5f, average_accuracy: %.5f, time: %.5f" % (train_loss, train_mae, train_acc, train_time), log_file)

        # evaluate the network on the test set
        start = time.time()
        test_loss, test_acc, test_mae = evaluate(net, data_loader_test, criterion, device)
        test_time = time.time() - start
        log("      Evaluation: average_cross_entropy_loss: %.5f, average_mae: %.5f, average_accuracy: %.5f, time: %.5f" % (test_loss, test_mae, test_acc, test_time), log_file)

        # update the learning rate
        #lr_scheduler.step()
        lr_scheduler.step(test_loss)

        if args.log_csv:
            log_csv.append([start_epoch + epoch + 1, train_loss, train_acc, train_mae, train_time, test_loss, test_acc, test_mae, test_time])

        # export the CNN
        if ((epoch + 1) % 5) == 0 and args.model_out:
            torch.save(net.state_dict(), os.path.join(args.model_out, 'cnn_' + str(start_epoch + epoch + 1) + '.pth'))

    # export the final CNN if not done yet
    if args.model_out and not os.path.isfile(os.path.join(args.model_out, 'cnn_' + str(start_epoch + args.epoch_count) + '.pth')):
        torch.save(net.state_dict(), os.path.join(args.model_out, 'cnn_' + str(start_epoch + args.epoch_count) + '.pth'))

    # export the log
    if args.log_csv:
        header = ""
        if args.model_in:
            f = open(args.log_csv, 'a')
        else:
            header = "epoch,train_loss,train_acc,train_mae,train_time,test_loss,test_acc,test_mae,test_time"
            f = open(args.log_csv, 'w')
        np.savetxt(f, log_csv, delimiter=',', header=header, comments='')
        f.flush()
        f.close()

    if args.log_file:
        log_file.close()

if __name__ == "__main__":
    main()
