import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import denseCRF

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.estimator_checks import check_estimator
from sklearn.model_selection import GridSearchCV
import sklearn.metrics as metrics

from evaluation.util import load_images, load_annotations, load_data


class CRF(BaseEstimator, ClassifierMixin):

    def __init__(self, w1=1, alpha=1, beta=1, w2=1, gamma=1, it=5):
        self.w1 = w1
        self.alpha = alpha
        self.beta = beta
        self.w2 = w2
        self.gamma = gamma
        self.it = it

    def fit(self, X, y):
        # no fitting required
        return self

    def predict(self, X):
        prediction = []
        params = (self.w1, self.alpha, self.beta, self.w2, self.gamma, self.it)

        for item in X:
            seg = denseCRF.densecrf(item[0], item[1], params).flatten()
            prediction.append(seg == 1)

        print("finished prediction")
        return prediction


def custom_fbeta(y, y_pred, beta):
    # assert that the input is a list of samples
    if type(y[0]) != np.ndarray:
        y = [y]
    if type(y_pred[0]) != np.ndarray:
        y_pred = [y_pred]

    # store the TP, FP, FN for all samples and claculate the F-beta score at the end
    # otherwise a single pixel in a large patch would be less important than a pixel in a small patch
    tp_list = []
    fp_list = []
    fn_list = []

    for i in range(len(y)):
        tp = np.sum(np.logical_and(y[i], y_pred[i]))
        fp = np.sum(y_pred[i]) - tp
        fn = np.sum(y[i]) - tp

        tp_list.append(tp)
        fp_list.append(fp)
        fn_list.append(fn)

    tp = np.sum(tp_list)
    fp = np.sum(fp_list)
    fn = np.sum(fn_list)

    if tp + fp != 0:
        precision = tp / (tp + fp)
    else:
        precision = 0

    if tp + fn != 0:
        recall = tp / (tp + fn)
    else:
        recall = 0

    if recall == 0 and precision == 0:
        return 0
    else:
        f_beta = (1 + beta ** 2) * precision * recall / (beta ** 2 * precision + recall)
        return f_beta


def get_patch(img, contour, gt):
    mask = np.zeros(img.shape[:-1], np.float32)
    # draw the contour onto the mask
    cv.drawContours(mask, [np.array(contour)], 0, color=1, thickness=-1)

    # only work on the local area
    rect = cv.boundingRect(np.array(contour))
    shape = np.array([rect[2], rect[3]])
    s = 0.3     # size of the border to each side
    p_1 = np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int)
    p_1 = np.clip(p_1, 0, [img.shape[1], img.shape[0]])
    p_2 = np.round(p_1 + (1 + 2 * s) * shape).astype(np.int)
    p_2 = np.clip(p_2, 0, [img.shape[1], img.shape[0]])

    # blurr the mask similar to deepCut
    s = 15
    mask = cv.GaussianBlur(mask, (s, s), 0)
    if not mask.sum() == 0:
        mask = (mask - mask.min()) / (mask.max() - mask.min())


    # cut out the patches and add them to the dataset
    img = img[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    gt = gt[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    mask = mask[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    prediction = np.stack((np.ones(mask.shape) - mask, mask), axis=-1).astype(np.float32)
    return [img, prediction], gt


def main():
    global args
    parser = argparse.ArgumentParser('Uses grid search to estimate the parameters of a conditional random field',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data and the annotations')
    parser.add_argument('--beta', type=float, default=1,
                        help='the beta used in the F-beta score')
    parser.add_argument('--n_jobs', type=int, default=1,
                        help='number of parallel jobs (-1 means using all cpu cores)')
    parser.add_argument('--seed', type=int, default=0,
                        help='the seed used to initialize the random number generator')
    parser.add_argument('--sample', action='store_true',
                        help='samples a single contour for each participant instead of using all contours')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "train/images"))
    image_set = set(images.keys())
    gts = load_annotations(d=os.path.join(args.data_dir, "train/annotations"))
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    np.random.seed(args.seed)

    # set up the dataset: extract an image patch and draw a contour patch as prediction for all available contours
    X = []
    Y = []
    # iterate over all participants
    for i, participant in enumerate(data):
        if args.sample:
            # get a list of available image names
            # (the json file contains images from the train and test set)
            image_names = list(image_set.intersection(data[participant].keys()))

            # sample a single image
            image_name = np.random.choice(image_names, 1)[0]

            # sample a single contour
            contour_name = np.random.choice(list(data[participant][image_name].keys()), 1)[0]
            contour = data[participant][image_name][contour_name]

            x, y = get_patch(images[image_name], contour, gts[image_name])
            X.append(x)
            Y.append(y)

        else:
            # iterate over all images the current participant annotated
            for image_name in data[participant].keys():
                # the json file contains images from the train and test set
                if image_name not in images.keys():
                    continue

                # iterate over all contours for the current participant and image
                for contour in data[participant][image_name].values():
                    x, y = get_patch(images[image_name], contour, gts[image_name])
                    X.append(x)
                    Y.append(y)

    # execute a grid search to estimate the CRF parameters
    crf = CRF()
    scorer = metrics.make_scorer(custom_fbeta, beta=args.beta)
    parameters = {"w1":[20, 30, 40], "alpha":[110, 130, 150], "beta":[0.1, 0.5]}

    grid = GridSearchCV(crf, parameters, scoring=scorer, n_jobs=args.n_jobs, refit=False)
    grid.fit(X, [y.flatten()==255 for y in Y])

    print("Best hyperparameters:", grid.best_params_)
    print("Best score:", grid.best_score_)

    with open("crf_initial.txt", 'w') as file:
        file.write(f"Best hyperparameters: {grid.best_params_}\n")
        file.write(f"Best score: {grid.best_score_}")


if __name__ == "__main__":
    main()
