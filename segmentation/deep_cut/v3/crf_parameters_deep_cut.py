import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm
import denseCRF
from sklearn.tree import DecisionTreeClassifier

from sklearn.model_selection import GridSearchCV
import sklearn.metrics as metrics
from segmentation.deep_cut.v2.crf_parameters_initial import CRF, custom_fbeta
from segmentation.deep_cut.v3.deep_cut import detect_lines

from evaluation.util import load_images, load_annotations, load_data


def deepCut(roi, img, mask, iterCount, early_stop_threshold=0.01, penalies_lines=True):
    """
    Executes the deepCut algorithm.
    """

    # extract the region of interest from the image
    p_1, p_2 = roi
    img = img[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    mask = mask[p_1[1] : p_2[1], p_1[0] : p_2[0]]

    # copy the mask to compare the changes between iterations
    mask_copy = np.copy(mask)
    contour = np.copy(mask)
    contour_size = cv.countNonZero(mask)

    # create the decision tree classifier
    clf = DecisionTreeClassifier()

    # detect the lines
    if penalies_lines:
        line_mask = detect_lines(img)

    predictions = []

    # DeepCut iterations: fixed number or until convergence
    # since the iteration 0 is used for initialization the last iteration is iterCount
    for i in range(iterCount + 1):
        #print(f"Iteration {i} of {iterCount}")
        # ============================ #
        # Prediction of the classifier #
        # ============================ #

        # make a prediction for each pixel
        if i == 0:
            # use the contour as an initialization
            prediction = (mask // cv.GC_PR_FGD).astype(np.float32)
        else:
            # use the prediction of the classifier
            X = img.reshape((-1, 3))
            prediction = clf.predict_proba(X)
            prediction = prediction.reshape(mask.shape[0], mask.shape[1], 2).astype(np.float32)
            prediction = prediction[:, :, 1]

        if penalies_lines:
            prediction *= line_mask

        # pixels outside of the contour belong to the background
        prediction[contour == 0] = 0

        if i != 0:
            predictions.append(prediction)

        # ============== #
        # CRF refinement #
        # ============== #

        # use a dense CRF to refine the segmentation

        # CRF parameters (optimized using grid search on a training set)
        # appearance kernel
        if i == 0:
            # use different parameters in the first round if the initialization is done by contour + CRF
            w1 = 70
            alpha = 75
            beta = 3
        else:
            w1 = 10
            alpha = 20
            beta = 1

        # smoothness kernel
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        # number of iterations
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        # calculate the segmentation
        mask[:] = denseCRF.densecrf(img, np.stack((1 - prediction, prediction), axis=-1), params).astype(np.uint8) * cv.GC_PR_FGD
        mask[contour == 0] = 0

        # prevent shrinking
        s = 3
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
        mask[:] = cv.dilate(mask, kernel)

        # stop due to last iteration
        if i == iterCount:
            return predictions

        # check whether only background remains
        if len(np.unique(mask)) == 1:
            if i == 0:
                # CRF didn't work in iteration 0 => use contour as initialization
                mask[:] = np.copy(contour)

        # update the copy of the mask that is used for early stopping
        mask_copy = np.copy(mask)

        # ====================== #
        # Train a new classifier #
        # ====================== #
        # set up the training set
        fgd = img[mask==cv.GC_PR_FGD]
        bgd = img[mask==0]

        X = np.concatenate((fgd, bgd))
        Y = np.concatenate((np.ones(len(fgd)), np.zeros(len(bgd))))

        # train the classifier
        clf = clf.fit(X, Y)

    return predictions


def main():
    global args
    parser = argparse.ArgumentParser('Estimates the parameters of the CRF used in the deepCut algorithm',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('--sample_count', type=int, default=20,
                        help='the number of samples used for the grid search')
    parser.add_argument('--seed', type=int, default=0,
                        help='the seed used to initialize the random number generator')
    parser.add_argument('--beta', type=float, default=1,
                        help='the beta used in the F-beta score')
    parser.add_argument('--n_jobs', type=int, default=1,
                        help='number of parallel jobs used for grid search (-1 means using all cpu cores)')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "train/images"))
    image_set = set(images.keys())
    gts = load_annotations(d=os.path.join(args.data_dir, "train/annotations"))
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # sample from the participants to get a good representation of different contours
    np.random.seed(args.seed)
    if args.sample_count > len(data.keys()):
        raise RuntimeError("sample_count larger than the number of participants")
    participants = np.random.choice(list(data.keys()), args.sample_count)

    # set up the dataset:
    #     image_patches: list of image patches
    #     deep_cut_masks: list of deepCut masks: [[deepCut mask iteration 1, ..., deepCut mask iteration N]]
    #     gt: list of corresponding ground truth patches
    image_patches = []
    deep_cut_predictions = []
    gt_patches = []

    # iterate over all sampled participants
    for participant in tqdm.tqdm(participants):
        # get a list of available image names
        # (the json file contains images from the train and test set)
        image_names = list(image_set.intersection(data[participant].keys()))

        # sample a single image
        image_name = np.random.choice(image_names, 1)[0]

        # sample a single contour
        contour_name = np.random.choice(list(data[participant][image_name].keys()), 1)[0]
        contour = data[participant][image_name][contour_name]

        # create a mask and draw the contour
        mask = np.zeros(images[image_name].shape[:2], np.uint8)
        cv.drawContours(mask, [np.array(contour)], 0, cv.GC_PR_FGD, -1)

        # only work on the local area to improve runtime
        rect = cv.boundingRect(np.array(contour))
        shape = np.array([rect[2], rect[3]])
        s = 0.3     # size of the border to each side
        p_1 = np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int)
        p_1 = np.clip(p_1, 0, [images[image_name].shape[1], images[image_name].shape[0]])
        p_2 = np.round(p_1 + (1 + 2 * s) * shape).astype(np.int)
        p_2 = np.clip(p_2, 0, [images[image_name].shape[1], images[image_name].shape[0]])

        # execute deepCut
        predictions = deepCut([p_1, p_2], images[image_name], mask, iterCount=10, early_stop_threshold=0)

        image_patches.append(images[image_name][p_1[1] : p_2[1], p_1[0] : p_2[0]])
        deep_cut_predictions.append(predictions)
        gt_patches.append(gts[image_name][p_1[1] : p_2[1], p_1[0] : p_2[0]])


    # execute a grid search to estimate the CRF parameters
    crf = CRF()
    scorer = metrics.make_scorer(custom_fbeta, beta=args.beta)

    parameters = {"w1":[5, 10, 15], "alpha":[10, 20, 30], "beta":[0.5, 1, 1.5]}

    for i in range(len(deep_cut_predictions[0])):
        X = []
        for j in range(len(image_patches)):
            prediction = deep_cut_predictions[j][i]
            prediction = np.stack((np.ones(prediction.shape) - prediction, prediction), axis=-1).astype(np.float32)
            X.append([image_patches[j], prediction])
        Y = gt_patches

        grid = GridSearchCV(crf, parameters, scoring=scorer, n_jobs=args.n_jobs, refit=False)
        grid.fit(X, [y.flatten()==255 for y in Y])

        print("Best hyperparameters:", grid.best_params_)
        print("Best score:", grid.best_score_)

        with open("crf_deep_cut_v3.txt", 'a') as file:
            file.write(f"Iteration {i + 1}\n")
            file.write(f"    Best hyperparameters: {grid.best_params_}\n")
            file.write(f"    Best score: {grid.best_score_}\n\n")

if __name__ == "__main__":
    main()
