import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm

from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression


def load_images(directory, grayscale=False):
    imgs = []
    flags = cv.IMREAD_GRAYSCALE if grayscale else cv.IMREAD_COLOR
    for filename in sorted(os.listdir(directory)):
        imgs.append(cv.imread(os.path.join(directory, filename), flags))
    return imgs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, help='the path of the directory containing the training data', default="./data/train")
    parser.add_argument('--classifier', type=str, choices=["tree", "logistic"], default="tree", help="the classifier that is used")
    args = parser.parse_args()
    print(args)

    imgs = load_images(os.path.join(args.data, "images"))
    gts = load_images(os.path.join(args.data, "annotations"), grayscale=True)

    for i in tqdm.tqdm(range(len(imgs))):
        # set up the training set
        fgd = imgs[i][gts[i]==255]
        bgd = imgs[i][gts[i]==0]

        X = np.concatenate((fgd, bgd))
        Y = np.concatenate((np.ones(len(fgd)), np.zeros(len(bgd))))

        # train the classifier
        if args.classifier == "tree":
            clf = DecisionTreeClassifier()
        elif args.classifier == "logistic":
            clf = LogisticRegression()
        clf = clf.fit(X, Y)

        # make a prediction for every pixel
        X = imgs[i].reshape((-1, 3))
        class_probabilities = clf.predict_proba(X)

        # visualization
        cv.imshow("image", imgs[i])
        cv.imshow("foreground probability", class_probabilities[:, 1].reshape(gts[i].shape))
        if cv.waitKey(0) == ord('q'):
            cv.destroyAllWindows()
            exit()


if __name__ == "__main__":
    main()
