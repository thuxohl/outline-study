import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm
import matplotlib.pyplot as plt


def load_images(directory, grayscale=False):
    imgs = []
    flags = cv.IMREAD_GRAYSCALE if grayscale else cv.IMREAD_COLOR
    for filename in sorted(os.listdir(directory)):
        imgs.append(cv.imread(os.path.join(directory, filename), flags))
    return imgs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, help='the path of the directory containing the training data', default="./data/train")
    parser.add_argument('--sampling', type=float, help='percentage of data points plotted for the foreground and background classes', default=0.01)
    args = parser.parse_args()
    print(args)

    imgs = load_images(os.path.join(args.data, "images"))
    gts = load_images(os.path.join(args.data, "annotations"), grayscale=True)

    for i in tqdm.tqdm(range(len(imgs))):
        fgd = imgs[i][gts[i]==255]
        bgd = imgs[i][gts[i]==0]

        # pick a subset from foreground and background since there are too many pixels for plotting
        fgd = fgd[np.random.choice(np.arange(len(fgd)), np.round(len(fgd) * args.sampling * 10).astype(int), replace=False)]
        bgd = bgd[np.random.choice(np.arange(len(bgd)), np.round(len(bgd) * args.sampling).astype(int), replace=False)]

        # plot the samples
        fig, ax = plt.subplots(1, 3, sharey='all')
        ax = ax.flatten()
        marker_size = 4

        ax[0].set_xlabel("blue")
        ax[0].set_ylabel("green")
        ax[0].scatter(bgd[:, 0], bgd[:, 1], s=marker_size, label="background")
        ax[0].scatter(fgd[:, 0], fgd[:, 1], s=marker_size, label="foreground")
        ax[0].legend()

        ax[1].set_xlabel("blue")
        ax[1].set_ylabel("red")
        ax[1].scatter(bgd[:, 0], bgd[:, 2], s=marker_size, label="background")
        ax[1].scatter(fgd[:, 0], fgd[:, 2], s=marker_size, label="foreground")
        ax[1].legend()

        ax[2].set_xlabel("green")
        ax[2].set_ylabel("red")
        ax[2].scatter(bgd[:, 1], bgd[:, 2], s=marker_size, label="background")
        ax[2].scatter(fgd[:, 1], fgd[:, 2], s=marker_size, label="foreground")
        ax[2].legend()
        plt.show()


if __name__ == "__main__":
    main()
