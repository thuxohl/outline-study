import argparse
import math
import sys
import os
import time
import copy

import numpy as np
import cv2 as cv
import tqdm
import denseCRF
from sklearn.tree import DecisionTreeClassifier

from evaluation.util import load_images, load_annotations, load_data


def detect_lines(img, penalty=0.5):
    """
    Detects the lines in an image using Canny edge detection and Hough Line Transform.
    The lines are drawn into a line mask and returned.
    The lines contain low values (between the [1 - penalty] and 1) while the other areas are 1,
    s.t. the line mask can be multiplied with a prediction to decrease the effect of the lines.

    Args:
        img: the color image of region of interest
        penalty: the penalty applied to the stripes (stripes in the prediction are multiplied with 1 - penalty)
    """
    # detect edges in the image
    edge_img = cv.Canny(img, threshold1=50, threshold2=100)

    # apply the Hough lines transform
    threshold = np.round(np.sqrt(np.prod(img.shape)) * 0.15).astype(int)
    lines = cv.HoughLines(edge_img, rho=1, theta=np.pi / 180, threshold=threshold)

    if lines is None:
        return np.ones(img.shape[:-1])

    # detect the dominant rotation of the lines and only keep those lines that have the same rotation
    # this sorts out wrong detections that point in other directions
    lines = np.squeeze(lines).reshape(-1, 2)
    values, counts = np.unique(lines[:, 1], return_counts=True)
    dominant_direction = values[np.argmax(counts)]
    lines = lines[lines[:, 1] == dominant_direction]

    # Sort the lines by distance. This way, neighboring lines in the image are neighboring lines in the array
    lines = lines[lines[:, 0].argsort()]

    # calculate the endpoints of the lines
    # code adapted from OpenCV Tutorial: https://docs.opencv.org/3.4/d9/db0/tutorial_hough_lines.html
    linepoints = np.zeros((len(lines), 4), dtype=int)
    for j, line in enumerate(lines):
        rho = line[0]
        theta = line[1]
        a = math.cos(theta)
        b = math.sin(theta)
        x0 = a * rho
        y0 = b * rho
        linepoints[j, :2] = (int(x0 + 10000 * (-b)), int(y0 + 10000 * (a)))
        linepoints[j, 2:] = (int(x0 - 10000 * (-b)), int(y0 - 10000 * (a)))

    # draw the lines onto the mask
    line_mask = np.zeros(img.shape[:-1])
    for j in range(len(lines)):
        # since we are detecting edges, a single stripe consists of multiple lines
        # if the distance between the current line and the next is small, the lines belong to the same stripe
        # in this case we want to add the whole stripe to the mask
        if j != len(lines) - 1:
            if lines[j + 1, 0] - lines[j, 0] < 10:
                line_area = np.array([linepoints[j, :2], linepoints[j, 2:], linepoints[j+1, 2:], linepoints[j+1, :2]])
                cv.drawContours(line_mask, [line_area], 0, penalty, -1)
                continue

        # in some cases only a single line is detected, so we draw this single line
        cv.line(line_mask, tuple(linepoints[j, :2]), tuple(linepoints[j, 2:]), penalty)

    # the prediction often extends the stripes by a few pixels
    # => dilate the mask to capture most of the detection
    s = 7
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
    line_mask = cv.dilate(line_mask, kernel)

    # smooth the mask to prevent hard transitions
    s = 7
    line_mask = cv.GaussianBlur(line_mask, (s, s), 0)

    # invert the mask
    # => lines are multiplied with a small value, other areas are multiplied with 1
    line_mask = 1 - line_mask
    return line_mask


def deepCut(roi, img, mask, iterCount, early_stop_threshold=0.01, penalize_lines=1, line_penalty=0.5):
    """
    Executes the deepCut algorithm.

    Args:
        roi: the region on which deepCut is executed. Using only parts of the image improves the runtime.
        img: the image on which deepCut is executed
        mask: mask marking the region that is possibly foreground (gets overwritten and contains the result)
        iterCount: maximum number of deepCut iterations
        early_stop_threshold: stop deepCut early if the number of pixels which changed classes between iterations
            is smaller then early_stop_threshold times the number of pixels within the original contour
        penalize_lines:
            0: no line penalization
            1: detect straight lines and reduce their effect on the prediction in every iteration
            2: only apply the line penalty in the last iteration
        line_penalty: the penalty applied to the stripes

    Returns:
        the number of iterations needed
    """

    # extract the region of interest from the image
    p_1, p_2 = roi
    img = img[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    mask = mask[p_1[1] : p_2[1], p_1[0] : p_2[0]]

    # copy the mask to compare the changes between iterations
    mask_copy = np.copy(mask)
    contour = np.copy(mask)
    contour_size = cv.countNonZero(mask)
    prediction = np.zeros(mask.shape)

    # create the decision tree classifier
    clf = DecisionTreeClassifier()

    # detect the lines
    if penalize_lines > 0 and line_penalty > 0:
        line_mask = detect_lines(img, line_penalty)

    # DeepCut iterations: fixed number or until convergence
    # since the iteration 0 is used for initialization the last iteration is iterCount
    for i in range(iterCount + 1):
        #print(f"Iteration {i} of {iterCount}")
        # ============================ #
        # Prediction of the classifier #
        # ============================ #

        # make a prediction for each pixel
        if i == 0:
            # use the contour as an initialization
            prediction = (mask // cv.GC_PR_FGD).astype(np.float32)
        else:
            # use the prediction of the classifier
            X = img.reshape((-1, 3))
            prediction = clf.predict_proba(X)
            prediction = prediction.reshape(mask.shape[0], mask.shape[1], 2).astype(np.float32)
            prediction = prediction[:, :, 1]

        if penalize_lines == 1 and line_penalty > 0:
            prediction *= line_mask

        # pixels outside of the contour belong to the background
        prediction[contour == 0] = 0

        # ============== #
        # CRF refinement #
        # ============== #

        # use a dense CRF to refine the segmentation

        # CRF parameters (optimized using grid search on a training set)
        # appearance kernel
        if i == 0:
            # use different parameters in the first round if the initialization is done by contour + CRF
            w1 = 70
            alpha = 75
            beta = 3
        else:
            w1 = 5
            alpha = 20
            beta = 1.5

        # smoothness kernel
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        # number of iterations
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        # calculate the segmentation
        mask[:] = denseCRF.densecrf(img, np.stack((1 - prediction, prediction), axis=-1), params).astype(np.uint8) * cv.GC_PR_FGD
        mask[contour == 0] = 0

        # prevent shrinking
        s = 3
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (s, s))
        mask[:] = cv.dilate(mask, kernel)

        # debug
        """
        cv.imshow("img", img)
        cv.imshow("mask", mask*255)
        cv.imshow("prediction", prediction)
        key = cv.waitKey(0)
        if key == ord('q'):
            cv.destroyAllWindows()
            exit()
        elif key == ord('n'):
            return i + 1
        #"""

        # stop due to last iteration
        if i == iterCount:
            break

        # stop early if there is only little change since the last iteration
        if cv.countNonZero(mask_copy - mask) < early_stop_threshold * contour_size and i > 0:
            break

        # stop early due to only background remaining
        if len(np.unique(mask)) == 1:
            if i > 0:
                break
            else:
                # CRF didn't work in iteration 0 => use contour as initialization
                mask[:] = np.copy(contour)

        # update the copy of the mask that is used for early stopping
        mask_copy = np.copy(mask)

        # ====================== #
        # Train a new classifier #
        # ====================== #
        # set up the training set
        fgd = img[mask==cv.GC_PR_FGD]
        bgd = img[mask==0]

        X = np.concatenate((fgd, bgd))
        Y = np.concatenate((np.ones(len(fgd)), np.zeros(len(bgd))))

        # train the classifier
        clf = clf.fit(X, Y)

    if penalize_lines == 2 and line_penalty > 0:
        prediction *= line_mask

        w1 = 5
        alpha = 20
        beta = 1.5
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        mask[:] = denseCRF.densecrf(img, np.stack((1 - prediction, prediction), axis=-1), params).astype(np.uint8) * cv.GC_PR_FGD
        mask[contour == 0] = 0

    return i + 1


def main():
    global args
    parser = argparse.ArgumentParser('Uses the DeepCut algorithm to improve the annotations from the user study',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('-o', '--output_dir', type=str, default='./data/segmentation/deepCutv3',
                        help='the directory to which the segmentation masks get exported')
    parser.add_argument('--penalize_lines', type=int, default=0,
                        help='use a hough line transform to detect stripes and penalize their foreground probability\n'
                        + '0: never apply the line penalty\n'
                        + '1: apply the line penalty in every iteration\n'
                        + '2: only apply the line penalty after the last iteration')
    parser.add_argument('--line_penalty', type=float, default=0.5,
                        help='the penalty applied to stripes')
    parser.add_argument('-v', '--visualize', action="store_true",
                        help='visualize the resulting segmentation masks')
    parser.add_argument('--deepcut_log', type=str, default=None,
                        help='where to store the log containing the iterations and execution time of the deepCut calls (needs to be a csv file)')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "images"))
    gt = load_annotations(d=os.path.join(args.data_dir, "annotations")) if args.visualize else None
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # create the output directory and open the output file
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)
    mapping_file = open(os.path.join(args.output_dir, "mapping.txt"), 'w')

    # create an empty list for the deepCut statistics
    statistics = []

    # iterate over all participants
    for i, participant in enumerate(tqdm.tqdm(data)):
        # iterate over all images the current participant annotated
        for j, image_name in enumerate(data[participant].keys()):
            # create a new segmentation mask
            segmentation = np.zeros(images[image_name].shape[:2], np.uint8)

            # iterate over all contours for the current participant and image
            for contour in data[participant][image_name].values():
                mask = np.zeros(segmentation.shape, np.uint8)
                # draw the contour onto the mask
                cv.drawContours(mask, [np.array(contour)], 0, cv.GC_PR_FGD, -1)

                # only work on the local area to improve runtime
                rect = cv.boundingRect(np.array(contour))
                shape = np.array([rect[2], rect[3]])
                s = 0.5     # size of the border to each side
                p_1 = np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int)
                p_1 = np.clip(p_1, 0, [images[image_name].shape[1], images[image_name].shape[0]])
                p_2 = np.round(p_1 + (1 + 2 * s) * shape).astype(np.int)
                p_2 = np.clip(p_2, 0, [images[image_name].shape[1], images[image_name].shape[0]])

                # execute deepCut
                start = time.time()     # measure the time for the statistics
                it = deepCut([p_1, p_2], images[image_name], mask, iterCount=10, early_stop_threshold=0.02, penalize_lines=args.penalize_lines, line_penalty=args.line_penalty)

                statistics.append([len(statistics), it, time.time() - start])   # add the id, the number of iterations and the time to the statistics
                mask = np.isin(mask, [cv.GC_FGD, cv.GC_PR_FGD]).astype(np.uint8) * 255

                # add the result to the segmentation
                segmentation = cv.bitwise_or(segmentation, mask)

            # export the segmentation mask and add it to the mapping file
            filename = f"{args.output_dir}/participant_{participant}_{image_name}.png"
            cv.imwrite(filename, segmentation)
            mapping_file.write(f"{args.data_dir}/annotations/{image_name}.png {filename}\n")

            # visualization
            if args.visualize:
                contour_img = np.copy(images[image_name])
                # draw the contours onto the image
                cv.drawContours(contour_img, [np.array(x) for x in data[participant][image_name].values()], -1, (255, 0, 0), 2)
                #cv.imshow(f"participant_{participant}_{image_name}", img)
                #cv.imshow("segmentation", segmentation)

                # compare the segmentation to the ground truth
                img = np.copy(contour_img)
                overlay = np.stack((np.zeros(img.shape[:2]), gt[image_name], segmentation), axis=-1)
                img[np.any(overlay != 0, axis=2)] = overlay[np.any(overlay != 0, axis=2)]
                cv.imshow(f"participant_{participant}_{image_name}_gt_comparison", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_gt_comparison", 100, 0)

                # visualize the segmentation by darkening unselected areas
                img = np.copy(contour_img)
                img[segmentation == 0] = np.round(0.3 * img[segmentation == 0]).astype(np.uint8)
                cv.imshow(f"participant_{participant}_{image_name}_segmentation", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_segmentation", img.shape[1] + 100, 0)

                # wait for the user to close the window
                # quit if the user pressed 'q'
                if cv.waitKey(0) == ord('q'):
                    cv.destroyAllWindows()

                    # close the mapping file
                    mapping_file.close()

                    # export the deepCut statistics
                    if args.deepcut_log:
                        np.savetxt(args.deepcut_log, statistics, fmt='%f', delimiter=',', header="call_id,iterations,time", comments='')
                    exit()
                cv.destroyAllWindows()

    # close the mapping file
    mapping_file.close()

    # export the deepCut statistics
    if args.deepcut_log:
        np.savetxt(args.deepcut_log, statistics, fmt='%f', delimiter=',', header="call_id,iterations,time", comments='')

    if args.visualize:
        cv.destroyAllWindows()

if __name__ == "__main__":
    main()
