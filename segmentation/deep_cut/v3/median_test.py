import argparse
import math
import sys
import os
import time
import copy

import numpy as np
import cv2 as cv
import tqdm

from evaluation.util import load_images, load_annotations, load_data

def main():
    global args
    parser = argparse.ArgumentParser('Tests whether applying a median filter to the segmentations improves the results',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('--segmentation_in', type=str, default='./data/segmentation/deepCutv3',
                        help='the directory which contains the segmentation masks')
    parser.add_argument('--segmentation_out', type=str, default='./data/segmentation/deepCutv3_d3',
                        help='the directory to which the dilated segmentation masks should be exported')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "images"))
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # create the output directory and open the output file
    if not os.path.isdir(args.segmentation_out):
        os.makedirs(args.segmentation_out)
    mapping_file = open(os.path.join(args.segmentation_out, "mapping.txt"), 'w')

    # iterate over all participants
    for i, participant in enumerate(tqdm.tqdm(data)):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():

            # load the segmentation mask
            filename = f"{args.segmentation_in}/participant_{participant}_{image_name}.png"
            segmentation = cv.imread(filename, cv.IMREAD_GRAYSCALE)

            # apply the median filter
            segmentation = cv.medianBlur(segmentation, 3)

            # export the segmentation mask and add it to the mapping file
            filename = f"{args.segmentation_out}/participant_{participant}_{image_name}.png"
            cv.imwrite(filename, segmentation)
            mapping_file.write(f"{args.data_dir}/annotations/{image_name}.png {filename}\n")

    # close the mapping file
    mapping_file.close()

if __name__ == "__main__":
    main()
