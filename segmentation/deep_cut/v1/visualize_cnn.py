import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm

import torch
import torchvision.transforms as T

import segmentation.deep_cut.v1.transforms as customT
from segmentation.deep_cut.v1.cnn import CNN


def get_transforms():
    transforms = []
    transforms.append(T.ToTensor())
    transforms.append(customT.Normalize())
    transforms.append(T.Pad(16, padding_mode='reflect'))
    return T.Compose(transforms)


def extract_patches(img, batch):
    patches = torch.zeros((batch.shape[0], 3, 33, 33))
    for i, [x, y] in enumerate(batch):
        patches[i] = img[:, y : y + 33, x : x + 33]
    return patches


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, help='the path of the directory containing the dataset', default="./data/test")
    parser.add_argument('--model', type=str, help='the path of an existing model of which the training should be continued', default="./models/cnn_30.pth")
    parser.add_argument('--batch_size', type=int, help='the number of samples used in a mini-batch', default=64)
    parser.add_argument('-v', '--visualize', help='turns off the visualization and only calculates the predictions', action='store_false')
    args = parser.parse_args()
    print(args)

    # create the output directory
    if not os.path.isdir(os.path.join(args.data, "prediction")):
        os.makedirs(os.path.join(args.data, "prediction"))

    # process the data on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # initialize the CNN
    net = CNN()
    net = net.to(device)
    net.load_state_dict(torch.load(args.model))
    net.eval()
    softmax = torch.nn.Softmax(dim=1)

    # transformations for normalizing and padding the image
    normalize = get_transforms()

    for filename in sorted(os.listdir(os.path.join(args.data, "images"))):
        # read the image
        image = cv.imread(os.path.join(args.data, "images", filename))
        height, width = image.shape[0], image.shape[1]

        if os.path.exists(os.path.join(args.data, "prediction", ".".join([filename.split('.')[0], "npy"]))):
            # load the prediction if it exists already
            print(f"loading prediction for {filename}")
            prediction = np.load(os.path.join(args.data, "prediction", ".".join([filename.split('.')[0], "npy"])))
        else:
            # calculate the prediction
            print(f"calculating prediction for {filename}")
            prediction = np.zeros(image.shape[:2])

            # create a list containing all image coordinates
            coordinates = np.array(np.meshgrid(np.arange(width), np.arange(height))).T.reshape(-1, 2)
            # normalize the image
            normalized_img = normalize(image)

            for i in tqdm.tqdm(range(0, height * width, args.batch_size)):
                # extract the coordinates and the patches that belong to the current batch
                batch = coordinates[i : i + args.batch_size]
                patches = extract_patches(normalized_img, batch).to(device)

                # make a prediction
                with torch.no_grad():
                    batch_prediction = softmax(net(patches))[:, 1]

                for j, p in enumerate(batch_prediction):
                    prediction[batch[j, 1], batch[j, 0]] = p

            # save the prediction
            np.save(os.path.join(args.data, "prediction", ".".join([filename.split('.')[0], "npy"])), prediction)

        # visualize the prediction
        if args.visualize:
            # load ground truth for additional information
            gt = cv.imread(os.path.join(args.data, "annotations", filename), cv.IMREAD_GRAYSCALE)

            cv.imshow("image", image)
            cv.imshow("prediction (red) vs ground truth (blue)", np.stack((gt.astype(np.float), np.zeros(gt.shape), prediction), axis=-1))
            # darken pixels where the prediction is correct to highlight errors
            correct_pred = ((prediction > 0.5).astype(np.uint) * 255) == gt
            image[correct_pred] = np.round(0.3 * image[correct_pred]).astype(np.uint8)
            cv.imshow("applied errors", image)
            if cv.waitKey(0) == ord('q'):
                cv.destroyAllWindows()
                exit()
    cv.destroyAllWindows()


if __name__ == "__main__":
    main()
