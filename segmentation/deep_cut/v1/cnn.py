import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm
from PIL import Image

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as T

import segmentation.deep_cut.v1.transforms as customT


def load_images(d):
    transforms = get_initial_transforms()
    res = {}
    for filename in sorted(os.listdir(d)):
        val = cv.imread(os.path.join(d, filename))
        res[filename] = transforms(val)
    return res


class Dataset(object):
    def __init__(self, data_dir, transforms):
        self.data_dir = data_dir
        self.transforms = transforms
        self.imgs = load_images(os.path.join(data_dir, "images"))
        self.background = np.load(os.path.join(data_dir, "background.npy"))
        self.foreground = np.load(os.path.join(data_dir, "foreground.npy"))
        # shuffle the samples
        np.random.shuffle(self.background)
        np.random.shuffle(self.foreground)
        self.len = 2 * self.foreground.shape[0]


    def __getitem__(self, idx):
        # start back at the beginning after a complete cycle
        idx = idx % self.len

        # alternate between foreground and background samples
        if idx % 2 == 0:
            # background class
            target = 0
            name = self.background[int(idx / 2), 0]
            x, y = self.background[int(idx / 2), 1:].astype(np.int)
            # extract patch
            img = self.imgs[name][:, y : y + 33, x : x + 33]
        else:
            # foreground class
            target = 1
            name = self.foreground[int((idx - 1) / 2), 0]
            x, y = self.foreground[int((idx - 1) / 2), 1:].astype(np.int)
            # extract patch
            img = self.imgs[name][:, y : y + 33, x : x + 33]

        return self.transforms(img), target


    def __len__(self):
        #return 6400
        return self.len


class CNN(nn.Module):

    def __init__(self):
        super(CNN, self).__init__()
        self.pad1 = nn.ZeroPad2d([2, 1, 2, 1])
        self.conv1 = nn.Conv2d(3, 32, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(32, 32, 3, padding=1)
        self.dropout = nn.Dropout()
        #self.fc = nn.Linear(32 * 8 * 8, 2)
        self.fc1 = nn.Linear(32 * 8 * 8, 100)
        self.fc2 = nn.Linear(100, 10)
        self.fc3 = nn.Linear(10, 2)
        self.sm = nn.Softmax(dim=1)


    def init_layers(self):
        std = 0.1
        torch.nn.init.normal_(self.conv1.weight, std=std)
        torch.nn.init.normal_(self.conv2.weight, std=std)
        #torch.nn.init.normal_(self.fc.weight, std=std)
        torch.nn.init.normal_(self.fc1.weight, std=std)
        torch.nn.init.normal_(self.fc2.weight, std=std)
        torch.nn.init.normal_(self.fc3.weight, std=std)


    def forward(self, x):
        # padding for conv1
        x = self.pad1(x)
        # conv1 + max pooling
        x = self.pool(F.relu(self.conv1(x)))
        # conv2 + dropout + max pooling
        x = F.relu(self.conv2(x))
        x = self.dropout(x)
        x = self.pool(x)
        # flatten
        x = x.view(-1, 32 * 8 * 8)
        # dropout + fully connected
        x = self.dropout(x)
        #x = self.fc(x)
        x = self.dropout(F.relu(self.fc1(x)))
        x = self.dropout(F.relu(self.fc2(x)))
        x = self.fc3(x)
        #x = self.sm(x)
        return x


def get_initial_transforms():
    """
    Initial transformations that are applied to all images in the dataset.
    """
    transforms = []
    transforms.append(T.ToTensor())
    transforms.append(customT.Normalize())
    transforms.append(T.Pad(16, padding_mode='reflect'))
    return T.Compose(transforms)


def get_transforms(train):
    """
    Transformations that are applied to single entries of the dataset.
    """
    transforms = []
    if train:
        transforms.append(customT.GaussianNoise(mean=0, std=0.05))
        transforms.append(T.RandomHorizontalFlip())
        transforms.append(T.RandomVerticalFlip())
    return T.Compose(transforms)


def train_one_epoch(net, optimizer, data_loader, criterion, device):
    net.train()

    mae = torch.nn.L1Loss()
    epoch_loss = 0
    epoch_accuracy = 0
    epoch_mae = 0
    for i, batch in enumerate(data_loader):
        optimizer.zero_grad()
        patches, labels = batch
        patches = patches.to(device)
        labels = labels.to(device)

        predictions = net(patches)
        if isinstance(criterion, nn.CrossEntropyLoss):
            loss = criterion(predictions, labels)
        else:
            loss = criterion(predictions, torch.nn.functional.one_hot(labels).type(torch.float))
        epoch_loss += loss.item()
        epoch_accuracy += (labels == torch.argmax(predictions, dim=1)).sum()
        epoch_mae += mae(predictions, torch.nn.functional.one_hot(labels)).item()

        loss.backward()
        optimizer.step()

    epoch_loss /= i
    epoch_accuracy = epoch_accuracy / (i * data_loader.batch_size)
    epoch_mae /= i
    return epoch_loss, epoch_accuracy, epoch_mae


@torch.no_grad()
def evaluate(net, data_loader, criterion, device):
    net.eval()

    mae = torch.nn.L1Loss()
    eval_loss = 0
    eval_accuracy = 0
    eval_mae = 0
    for i, batch in enumerate(data_loader):
        patches, labels = batch
        patches = patches.to(device)
        labels = labels.to(device)

        predictions = net(patches)
        if isinstance(criterion, nn.CrossEntropyLoss):
            loss = criterion(predictions, labels)
        else:
            loss = criterion(predictions, torch.nn.functional.one_hot(labels).type(torch.float))
        eval_loss += loss.item()
        eval_accuracy += (labels == torch.argmax(predictions, dim=1)).sum()
        eval_mae += mae(predictions, torch.nn.functional.one_hot(labels)).item()

    eval_loss /= i
    eval_accuracy = eval_accuracy / (i * data_loader.batch_size)
    eval_mae /= i
    return eval_loss, eval_accuracy, eval_mae


def log(msg, log_file, newline=True):
    print(msg)
    if log_file is not None:
        if newline:
            log_file.write(msg + "\n")
        else:
            log_file.write(msg)
        log_file.flush()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_train', type=str, help='the path of the directory containing the training data', default="./data/train")
    parser.add_argument('--data_test', type=str, help='the path of the directory containing the test data', default="./data/test")
    parser.add_argument('--model_in', type=str, help='the path of an existing model of which the training should be continued', default=None)
    parser.add_argument('--model_out', type=str, help='directory where the trained models are stored', default="./models")
    parser.add_argument('--log_file', type=str, help='the path of the log file', default=None)
    parser.add_argument('--log_csv', type=str, help='the path of the log exported as csv', default=None)
    parser.add_argument('--epoch_count', type=int, help='the number of training epochs', default=30)
    parser.add_argument('--batch_size', type=int, help='the number of samples used in a mini-batch', default=64)
    args = parser.parse_args()
    print(args)


    # train on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # load or initialize the CNN
    net = CNN()
    net = net.to(device)
    start_epoch = 0
    if args.model_in:
        start_epoch = int(os.path.splitext(args.model_in)[0].split('_')[1])
        net.load_state_dict(torch.load(args.model_in))
    else:
        net.init_layers()

    # open the logfile
    log_csv = []
    log_file = None
    if args.log_file:
        if args.model_in:
            log_file = open(args.log_file, 'a')
        else:
            log_file = open(args.log_file, 'w')

    # use our dataset and defined transformations
    dataset_train = Dataset(args.data_train, get_transforms(train=True))
    dataset_test = Dataset(args.data_test, get_transforms(train=False))

    # create data loaders
    data_loader_train = torch.utils.data.DataLoader(dataset_train, batch_size=args.batch_size, num_workers=4, drop_last=True)
    data_loader_test = torch.utils.data.DataLoader(dataset_test, batch_size=args.batch_size, num_workers=4, drop_last=True)

    # create the optimizer
    #optimizer = torch.optim.Adagrad(net.parameters(), lr=0.015)
    #optimizer = torch.optim.Adagrad(net.parameters(), lr=0.005)
    optimizer = torch.optim.Adagrad(net.parameters(), lr=0.008)

    # create a scheduler for the learning rate
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.975)

    # create the loss function
    criterion = nn.CrossEntropyLoss()
    #criterion = nn.BCELoss()
    #criterion = nn.MSELoss()

    # train the CNN
    for epoch in range(args.epoch_count):
        # train the network for a single epoch
        start = time.time()
        train_loss, train_acc, train_mae = train_one_epoch(net, optimizer, data_loader_train, criterion, device)
        train_time = time.time() - start
        log("Epoch (%i/%i):" % (start_epoch + epoch + 1, start_epoch + args.epoch_count), log_file)
        log("      Training:   average_cross_entropy_loss: %.5f, average_mae: %.5f, average_accuracy: %.5f, time: %.5f" % (train_loss, train_mae, train_acc, train_time), log_file)

        # update the learning rate
        lr_scheduler.step()

        # evaluate the network on the test set
        start = time.time()
        test_loss, test_acc, test_mae = evaluate(net, data_loader_test, criterion, device)
        test_time = time.time() - start
        log("      Evaluation: average_cross_entropy_loss: %.5f, average_mae: %.5f, average_accuracy: %.5f, time: %.5f" % (test_loss, test_mae, test_acc, test_time), log_file)

        if args.log_csv:
            log_csv.append([start_epoch + epoch + 1, train_loss, train_acc, train_mae, train_time, test_loss, test_acc, test_mae, test_time])

        # export the CNN
        if ((epoch + 1) % 10) == 0 and args.model_out:
            torch.save(net.state_dict(), os.path.join(args.model_out, 'cnn_' + str(start_epoch + epoch + 1) + '.pth'))

    # export the final CNN if not done yet
    if args.model_out and not os.path.isfile(os.path.join(args.model_out, 'cnn_' + str(start_epoch + args.epoch_count) + '.pth')):
        torch.save(net.state_dict(), os.path.join(args.model_out, 'cnn_' + str(start_epoch + args.epoch_count) + '.pth'))

    # export the log
    if args.log_csv:
        header = ""
        if args.model_in:
            f = open(args.log_csv, 'a')
        else:
            header = "epoch,train_loss,train_acc,train_mae,train_time,test_loss,test_acc,test_mae,test_time"
            f = open(args.log_csv, 'w')
        np.savetxt(f, log_csv, delimiter=',', header=header, comments='')
        f.flush()
        f.close()

    if args.log_file:
        log_file.close()

if __name__ == "__main__":
    main()
