import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import denseCRF

from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.estimator_checks import check_estimator
from sklearn.model_selection import GridSearchCV
import sklearn.metrics as metrics

class CRF(BaseEstimator, ClassifierMixin):

    def __init__(self, w1=1, alpha=1, beta=1, w2=1, gamma=1, it=5):
        self.w1 = w1
        self.alpha = alpha
        self.beta = beta
        self.w2 = w2
        self.gamma = gamma
        self.it = it

    def fit(self, X, y):
        # no fitting required
        return self

    def predict(self, X):
        prediction = []
        params = (self.w1, self.alpha, self.beta, self.w2, self.gamma, self.it)

        for item in X:
            #prediction.append(denseCRF.densecrf(item[0], item[1], params).flatten())
            seg = denseCRF.densecrf(item[0], item[1], params).flatten()
            prediction.append(seg == 1)

        print("finished prediction")
        return np.array(prediction)


def load_images(path, grayscale=False):
    images = []
    for filename in sorted(os.listdir(path)):
        if grayscale:
            images.append(cv.imread(os.path.join(path, filename), cv.IMREAD_GRAYSCALE).astype(np.float32) / 255.0)
        else:
            images.append(cv.imread(os.path.join(path, filename), cv.IMREAD_COLOR))

    return np.array(images)


def load_predictions(path):
    predictions = []
    for filename in sorted(os.listdir(path)):
        predictions.append(np.load(os.path.join(path, filename)))

    return np.array(predictions)


def main():
    global args
    parser = argparse.ArgumentParser('Uses grid search to estimate the parameters of a conditional random field',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data/train',
                        help='the directory containing the image data and the annotations')
    parser.add_argument('--beta', type=float, default=2,
                        help='the beta used in the F-beta score')
    parser.add_argument('--border_size', type=int, default=30,
                        help='the predictions are limited to the stains + a border of border_size pixels')
    parser.add_argument('--filter_size', type=int, default=51,
                        help='the size of the Gaussian kernel used to blur the borders controlled by border_size')
    parser.add_argument('--n_jobs', type=int, default=1,
                        help='number of parallel jobs (-1 means using all cpu cores)')
    args = parser.parse_args()
    print(args)

    # load the data
    images = load_images(os.path.join(args.data_dir, "images"))
    gts = load_images(os.path.join(args.data_dir, "annotations"), grayscale=True)
    predictions = load_predictions(os.path.join(args.data_dir, "prediction"))

    class_probabilities = []
    for i in range(gts.shape[0]):
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2 * args.border_size + 1, 2 * args.border_size + 1), (args.border_size, args.border_size))
        mask = cv.dilate(gts[i], kernel)
        mask = cv.GaussianBlur(mask, ksize=(args.filter_size, args.filter_size), sigmaX=0, sigmaY=0)
        prob_fgd = np.multiply(predictions[i], mask)
        class_probabilities.append(np.stack((np.ones(prob_fgd.shape) - prob_fgd, prob_fgd), axis=-1).astype(np.float32))

        """
        cv.imshow("blue: fgd. prob, red: cnn prediction", np.stack((prob_fgd, np.zeros(prob_fgd.shape), predictions[i]), axis=-1))
        cv.imshow("blue: fgd. prob, red: gt", np.stack((prob_fgd, np.zeros(prob_fgd.shape), gts[i]), axis=-1))
        if cv.waitKey(0) == ord('q'):
            cv.destroyAllWindows()
            exit()
        """
    class_probabilities = np.array(class_probabilities)

    # concatenate the images and the class probabilities to a single input
    X = [[images[i], class_probabilities[i]] for i in range(images.shape[0])]


    # execute a grid search to estimate the CRF parameters
    crf = CRF()
    scorer = metrics.make_scorer(metrics.fbeta_score, beta=args.beta, average='micro')
    parameters = {"w1":[0.6, 0.8, 1], "alpha":[50, 70, 90], "beta":[3, 4.5, 6]}

    grid = GridSearchCV(crf, parameters, scoring=scorer, n_jobs=args.n_jobs, refit=False)
    grid.fit(X, [y.flatten()==1 for y in gts])

    print("Best hyperparameters:", grid.best_params_)
    print("Best score:", grid.best_score_)


if __name__ == "__main__":
    main()
