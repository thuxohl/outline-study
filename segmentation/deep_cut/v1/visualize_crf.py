import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import denseCRF

def load_images(path, grayscale=False):
    images = []
    for filename in sorted(os.listdir(path)):
        if grayscale:
            images.append(cv.imread(os.path.join(path, filename), cv.IMREAD_GRAYSCALE).astype(np.float32) / 255.0)
        else:
            images.append(cv.imread(os.path.join(path, filename), cv.IMREAD_COLOR))

    return np.array(images)


def load_predictions(path):
    predictions = []
    for filename in sorted(os.listdir(path)):
        predictions.append(np.load(os.path.join(path, filename)))

    return np.array(predictions)


def main():
    global args
    parser = argparse.ArgumentParser('Visualizes the results of a conditional random field',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data/train',
                        help='the directory containing the image data and the annotations')
    parser.add_argument('--w1', type=float, default=0.8,
                        help='crf parameter w1')
    parser.add_argument('--alpha', type=float, default=70,
                        help='crf parameter alpha')
    parser.add_argument('--beta', type=float, default=4.5,
                        help='crf parameter beta')
    parser.add_argument('--w2', type=float, default=1,
                        help='crf parameter w2')
    parser.add_argument('--gamma', type=float, default=1,
                        help='crf parameter gamma')
    parser.add_argument('--border_size', type=int, default=30,
                        help='the predictions are limited to the stains + a border of border_size pixels')
    parser.add_argument('--filter_size', type=int, default=51,
                        help='the size of the Gaussian kernel used to blur the borders controlled by border_size')
    args = parser.parse_args()
    print(args)

    # load the data
    images = load_images(os.path.join(args.data_dir, "images"))
    gts = load_images(os.path.join(args.data_dir, "annotations"), grayscale=True)
    predictions = load_predictions(os.path.join(args.data_dir, "prediction"))

    # CRF parameters
    w1    = args.w1     # weight of appearance kernel
    alpha = args.alpha  # spatial std (appearance)
    beta  = args.beta   # rgb std
    w2    = args.w2     # weight of the smoothness kernel
    gamma = args.gamma  # spatial std (smoothness)
    it    = 5.0         # iteration
    params = (w1, alpha, beta, w2, gamma, it)

    for i in range(gts.shape[0]):
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2 * args.border_size + 1, 2 * args.border_size + 1), (args.border_size, args.border_size))
        mask = cv.dilate(gts[i], kernel)
        mask = cv.GaussianBlur(mask, ksize=(args.filter_size, args.filter_size), sigmaX=0, sigmaY=0)
        prob_fgd = np.multiply(predictions[i], mask)
        prob = np.stack((np.ones(prob_fgd.shape) - prob_fgd, prob_fgd), axis=-1).astype(np.float32)

        # calculate the segmentation
        seg = denseCRF.densecrf(images[i], prob, params).astype(np.uint8)

        cv.imshow("segmentation", seg*255)
        cv.imshow("foreground probability", prob[:,:,1])

        images[i][seg < 0.5] = np.round(0.3 * images[i][seg < 0.5]).astype(np.uint8)
        cv.imshow("applied prediction", images[i])

        if cv.waitKey(0) == ord('q'):
            cv.destroyAllWindows()
            exit()


if __name__ == "__main__":
    main()
