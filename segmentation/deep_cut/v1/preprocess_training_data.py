import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, help='the path of the directory containing the dataset', default="./data/train")
    parser.add_argument('--halo', type=int, help='the size of the halo region in pixel', default=20)
    args = parser.parse_args()
    print(args)

    annotations = list(sorted(os.listdir(os.path.join(args.data, "annotations"))))

    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2 * args.halo + 1, 2 * args.halo + 1))

    background = []
    foreground = []
    stain_count = [2, 1, 3, 2, 3, 1]
    #stain_count = [2, 2, 2, 3]
    #header = ",".join(["image", "stain_count", "x", "y"])
    header = ",".join(["image", "x", "y"])

    for i, name in enumerate(tqdm.tqdm(annotations)):
        # load the annotation
        ann = cv.imread(os.path.join(args.data, "annotations", name), cv.IMREAD_GRAYSCALE)
        ann_halo = cv.dilate(ann, kernel, anchor=(args.halo, args.halo))

        # iterate over all pixels and add pixels within box or halo to list
        for r in range(ann.shape[0]):
            for c in range(ann.shape[1]):
                if ann_halo[r, c] > 0:
                    if ann[r, c] > 0:
                        #foreground.append([name, stain_count[i], c, r])
                        foreground.append([name, c, r])
                    else:
                        #background.append([name, stain_count[i], c, r])
                        background.append([name, c, r])

    # export files
    np.savetxt(os.path.join(args.data, "foreground.csv"), foreground, fmt='%s', delimiter=',', header=header, comments='')
    np.savetxt(os.path.join(args.data, "background.csv"), background, fmt='%s', delimiter=',', header=header, comments='')
    np.save(os.path.join(args.data, "foreground.npy"), np.array(foreground, dtype=np.str))
    np.save(os.path.join(args.data, "background.npy"), np.array(background, dtype=np.str))


if __name__ == "__main__":
    main()
