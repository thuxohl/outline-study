import torch

class Normalize(object):
    def __call__(self, image):
        return (image - torch.mean(image)) / torch.std(image)


class GaussianNoise():
    def __init__(self, mean=0, std=1):
        self.mean = mean
        self.std = std

    def __call__(self, image):
        return image + torch.randn(image.size()) * self.std + self.mean
