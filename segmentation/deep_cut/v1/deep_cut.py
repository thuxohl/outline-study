import argparse
import math
import sys
import os
import time
import copy

import numpy as np
import cv2 as cv
import tqdm
import denseCRF

import torch
import torch.nn as nn
import torchvision.transforms as T

import segmentation.deep_cut.v1.transforms as customT
from segmentation.deep_cut.v1.cnn import CNN
from evaluation.util import load_images, load_annotations, load_data


def normalize_images(images, device, pad_size=16):
    """
    Normalizes a dictionary of images to zero mean and unit variance.
    The images are also converted to a Torch Tensor and padded.
    Required for the images to be used with the CNN.

    Args:
        images: dictionary containing the images
        device: target device for the torch tensor
        pad_size: size of the padding

    Returns:
        new dictionary containing the normalized images
    """
    transforms = []
    transforms.append(T.ToTensor())
    transforms.append(customT.Normalize())
    transforms.append(T.Pad(pad_size, padding_mode='reflect'))
    transforms = T.Compose(transforms)

    normalized = {}

    for key in images.keys():
        # apply normalization
        normalized[key] = transforms(images[key]).to(device)

    return normalized


def extract_patches(img, batch):
    """
    Extracts a list of patches from an image.

    Args:
        img: image from which the patches should be extracted
        batch: list containing image coordinates ([x, y])
    """
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    patches = torch.empty((batch.shape[0], 3, 33, 33), device=device)
    for i, [x, y] in enumerate(batch):
        # extract a patch of size 33 x 33
        patches[i] = img[:, y : y + 33, x : x + 33]
    return patches


def predict_classes(model, mask, normalized_img, batch_size, dilate, kernel_size=5, interpolation_factor=2):
    """
    Predicts the segmentation for a normalized image.

    Args:
        model: CNN used for the prediction
        mask: mask containing the regions that are probably foreground.
            To improve the runtime only pixels within and close to foreground regions are evaluated.
        normalized_img: image for which the prediction should be computed
        batch_size: size of the mini batches
        dialte: whether the mask needs to be dilated (usually this is False for the first epoch of deepCut and True for the rest)
        kernel_size: size of the kernel that is used to dilate the mask
            (since the normalized image might already be scaled, the range in the unscaled image might be larger)
        interpolation_factor: interpolation factor of the predictions (integer).
            To improve the runtime not every pixel is evaluated. Instead an bilinear interpolation between known pixels is used.
            (e.g. only predict the class of every second pixel and interpolate the rest)
    """

    softmax = torch.nn.Softmax(dim=1)
    model.eval()

    # dilate the mask => predictions for pixels within stains and close to stains
    if dilate:
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (kernel_size, kernel_size))
        mask = cv.dilate(mask, kernel, anchor=(int((kernel_size - 1) / 2), int((kernel_size - 1) / 2)))

    # construct the coordinates array
    coordinates = np.argwhere(mask == cv.GC_PR_FGD)[:,::-1]     # [:,::-1] transforms the results from (row, column) pairs to (x, y) points
    coordinates = coordinates[(coordinates % interpolation_factor == 0).all(axis=1)]   # only use those points where row and column are a multiple of the interpolation factor

    # construct the prediction
    shape = np.ceil([mask.shape[0] / interpolation_factor, mask.shape[1] / interpolation_factor]).astype(np.int)
    prediction = np.stack((np.ones(shape), np.zeros(shape)), axis=-1).astype(np.float32)    # unless changed later the pixel is predicted to be a background pixel
    for i in range(0, coordinates.shape[0], batch_size):
        # extract the coordinates and the patches that belong to the current batch
        batch = coordinates[i : i + batch_size]
        patches = extract_patches(normalized_img, batch)

        # make a prediction
        with torch.no_grad():
            batch_prediction = softmax(model(patches)).cpu()

        # enter the predictions in the prediction array
        for j, p in enumerate(batch_prediction):
            # map the coordinates from the original size of the image to the downscaled size of the prediction array
            c = int(batch[j, 1] / interpolation_factor), int(batch[j, 0] / interpolation_factor)
            # enter the prediction
            prediction[c] = p

    # use OpenCVs resize function for the bilinear interpolation
    return cv.resize(prediction, dsize=mask.shape)


def retrain(model, normalized_img, data):
    """
    Retrains the CNN with new data.

    Args:
        model: the CNN that needs to be retrained
        normalized_img: the image from which the patches are extracted
        data: list containing [x, y, target_class] entries
            (the order should be randomized to decorrelate index and target_class)
    """
    global args
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    model.train()

    # create the loss function and the optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adagrad(model.parameters(), lr=args.learning_rate)

    # extract batches
    for i in range(0, data.shape[0], args.batch_size):
        optimizer.zero_grad()

        # extract the coordinates and the patches that belong to the current batch
        batch = torch.tensor(data[i : i + args.batch_size])
        patches = extract_patches(normalized_img, batch[:, :2])
        target = batch[:, 2].to(device).type(torch.long)

        # make a prediction and calculate the loss
        predictions = model(patches)
        loss = criterion(predictions, target)

        # backpropagation
        loss.backward()
        optimizer.step()


def deepCut(roi, img, normalized_img, mask, model, iterCount, downscale=2, early_stop_threshold=0.01, max_train_it=40):
    """
    Executes the deepCut algorithm.

    Args:
        roi: the region on which deepCut is executed. Using only parts of the image improves the runtime.
        img: the image on which deepCut is executed (used in the CRF)
        normalized_img: the image normalized to zero mean and unit variance (used for the CNN predictions)
        mask: mask marking the region that is possibly foreground (gets overwritten and contains the result)
        iterCount: maximum number of deepCut iterations
        downscale: the factor by which the image is downscaled to improve runtime
        early_stop_threshold: stop deepCut early if the number of pixels which changed classes between iterations
            is smaller then early_stop_threshold times the number of pixels within the original contour
        max_train_it: limits the number of samples used for training to max_train_it * args.batch_size

    Returns:
        the number of iterations needed
    """

    global args

    # extract the region of interest from the image
    p_1, p_2 = roi
    img = img[p_1[1] : p_2[1], p_1[0] : p_2[0]]
    normalized_img = normalized_img[:, p_1[1] : p_2[1] + downscale * 32, p_1[0] : p_2[0] + downscale * 32]
    target_size = list(np.round([normalized_img.shape[1] / downscale, normalized_img.shape[2] / downscale]).astype(np.int))
    resized_normalized_img = T.functional.resize(normalized_img, target_size)
    mask = mask[p_1[1] : p_2[1], p_1[0] : p_2[0]]

    # copy the mask to compare the changes between iterations
    mask_copy = np.copy(mask)
    contour_size = cv.countNonZero(mask)

    # DeepCut iterations: fixed number or until convergence
    for i in range(iterCount):
        # ============== #
        # CNN prediction #
        # ============== #

        # use the CNN to predict the classes (foreground and background)
        # scale the mask down to improve runtime
        resized_mask = cv.resize(mask, dsize=(0, 0), fx=1/downscale, fy=1/downscale)
        prediction = predict_classes(model, resized_mask, resized_normalized_img, args.batch_size, dilate=(i != iterCount), interpolation_factor=args.prediction_interpolation)
        # upscale the mask after the prediction (uses bilinear interpolation)
        prediction = cv.resize(prediction, dsize=mask_copy.shape[::-1])

        # ============== #
        # CRF refinement #
        # ============== #

        # use a dense CRF to refine the segmentation
        # since the densecrf function is fast compared to the CNN it is possible to take advantage of the full image resolution

        # CRF parameters (optimized using grid search on a training set)
        # appearance kernel
        w1    = 0.8  # weight of appearance kernel
        alpha = 70   # spatial std (appearance)
        beta  = 4.5  # rgb std
        # smoothness kernel
        w2    = 1    # weight of the smoothness kernel
        gamma = 1    # spatial std (smoothness)
        # number of iterations
        it    = 5.0
        params = (w1, alpha, beta, w2, gamma, it)

        # calculate the segmentation
        mask[:] = denseCRF.densecrf(img, prediction, params).astype(np.uint8) * cv.GC_PR_FGD

        # ============== #
        # CNN retraining #
        # ============== #

        # if the original model is retrained on each contour and deepCut terminated, no further retraining is required
        if args.model_mode == 0:
            # stop due to last iteration
            if i == iterCount:
                return i + 1

            # stop early if there is only little change since the last iteration
            if cv.countNonZero(mask_copy - mask) < early_stop_threshold * contour_size:
                return i + 1


        # use the updated segmentation to retrain the CNN
        fgd, bgd = [], []
        # since the CNN is retrained on the resized image, the training samples need to be extracted from the resized mask
        resized_mask = cv.resize(mask, dsize=(0, 0), fx=1/downscale, fy=1/downscale)

        # retraining mode 1: only use pixels close to a stain border
        if args.retraining_mode == 1:
            # find the stain borders by searching for contours
            contours, _ = cv.findContours(resized_mask, mode=cv.RETR_EXTERNAL, method=cv.CHAIN_APPROX_SIMPLE)
            train_mask = np.zeros(resized_mask.shape)
            # draw the contours with a high thickness. Pixels within this line will be used for retraining
            cv.drawContours(train_mask, contours, -1, 1, thickness=20)

        for r in range(resized_mask.shape[0]):
            for c in range(resized_mask.shape[1]):
                # retraining mode 1: check if pixel is close to a stain border
                if args.retraining_mode == 1 and train_mask[r, c] == 0:
                    continue

                # append the samples to the foreground or background list
                if resized_mask[r, c] == cv.GC_PR_FGD:
                    fgd.append([c, r])
                else:
                    bgd.append([c, r])
        fgd = np.array(fgd)
        bgd = np.array(bgd)
        # shuffle the lists to decorrelate the samples (decorrelates index and position)
        np.random.shuffle(fgd)
        np.random.shuffle(bgd)

        # limit the number of samples:
        # - use an equal amount of foreground and background samples
        # - training should take a maximum of max_train_it iterations
        max_samples = min(fgd.shape[0], bgd.shape[0], int(max_train_it * args.batch_size / 2))
        fgd = fgd[: max_samples].reshape((-1, 2))   # the reshape assures the correct dimensionality if the number of samples is 0
        bgd = bgd[: max_samples].reshape((-1, 2))

        # add the target class
        fgd = np.concatenate((fgd, np.ones([fgd.shape[0], 1])), axis=1)
        bgd = np.concatenate((bgd, np.zeros([bgd.shape[0], 1])), axis=1)

        # concatenate foreground and background data and shuffle the array to decorrelate index and target class
        data = np.concatenate((fgd, bgd), axis=0).astype(np.int)
        np.random.shuffle(data)

        # retraining model
        retrain(model, resized_normalized_img, data)

        # stop early if there is only little change since the last iteration
        # (since the retrained model is used again afterwards the check occurs after the retraining)
        if args.model_mode != 0 and cv.countNonZero(mask_copy - mask) < early_stop_threshold * contour_size:
            return i + 1

        # update the copy of the mask that is used for early stopping
        mask_copy = np.copy(mask)

    return iterCount


def main():
    global args
    parser = argparse.ArgumentParser('Uses the DeepCut algorithm to improve the annotations from the user study',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--model', type=str, help='the path of the cnn model', required=True)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('-o', '--output_dir', type=str, default='./data/segmentation/deepCut',
                        help='the directory to which the segmentation masks get exported')
    parser.add_argument('--batch_size', type=int, help='the number of samples used in a mini-batch', default=64)
    parser.add_argument('-v', '--visualize', action="store_true",
                        help='visualize the resulting segmentation masks')
    parser.add_argument('--deepcut_log', type=str, default=None,
                        help='where to store the log containing the iterations and execution time of the deepCut calls (needs to be a csv file)')
    parser.add_argument('--learning_rate', type=float, default=0.000001,
                        help='the learning rate during retraining')
    parser.add_argument('--retraining_mode', type=int, default=0,
                        help='0: select samples for retraining randomly\n'
                        + '1: only select samples close to a stain border')
    parser.add_argument('--model_mode', type=int, default=0,
                        help='0: retrain a copy of the default cnn for each contour\n'
                        + '1: retrain a copy of the default cnn for each combination of image and participant\n'
                        + '2: use the same cnn for all images')
    parser.add_argument('--downscale', type=int, default=2,
                        help='the factor by which deepCut scales the images down to improve the runtime')
    parser.add_argument('--prediction_interpolation', type=int, default=2,
                        help='the factor by which the predictions are interpolated to improve the runtime\n'
                        + '(e.g. only predict every second pixel and interpolate the rest)')
    args = parser.parse_args()
    print(args)


    # process the data on the GPU if available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "images"))
    gt = load_annotations(d=os.path.join(args.data_dir, "annotations")) if args.visualize else None
    normalized_images = normalize_images(images, device, pad_size = args.downscale * 16)    # the required padding depends on the scaling factor
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # create the output directory and open the output file
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)
    mapping_file = open(os.path.join(args.output_dir, "mapping.txt"), 'w')

    # initialize the CNN
    net = CNN()
    net = net.to(device)
    net.load_state_dict(torch.load(args.model))

    # create an empty list for the deepCut statistics
    statistics = []

    # iterate over all participants
    for participant in tqdm.tqdm(data):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():
            # create a new segmentation mask
            segmentation = np.zeros(images[image_name].shape[:2], np.uint8)

            # model mode 1: retrain the model for each participant-image combination
            if args.model_mode == 1:
                net_copy = copy.deepcopy(net)

            # iterate over all contours for the current participant and image
            for contour in data[participant][image_name].values():
                mask = np.zeros(segmentation.shape, np.uint8)
                # draw the contour onto the mask
                cv.drawContours(mask, [np.array(contour)], 0, cv.GC_PR_FGD, -1)

                # only work on the local area to improve runtime
                rect = cv.boundingRect(np.array(contour))
                shape = np.array([rect[2], rect[3]])
                s = 0.1     # size of the border to each side
                p_1 = np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int)
                p_1 = np.clip(p_1, 0, [images[image_name].shape[1], images[image_name].shape[0]])
                p_2 = np.round(p_1 + (1 + 2 * s) * shape).astype(np.int)
                p_2 = np.clip(p_2, 0, [images[image_name].shape[1], images[image_name].shape[0]])

                # execute deepCut
                start = time.time()     # measure the time for the statistics
                if args.model_mode == 0:
                    # model mode 0: retrain the model for each contour
                    net_copy = copy.deepcopy(net)
                    it = deepCut([p_1, p_2], images[image_name], normalized_images[image_name], mask, net_copy, iterCount=5, downscale=args.downscale, early_stop_threshold=0.05)
                elif args.model_mode == 1:
                    # model mode 1: retrain the model for each participant-image combination
                    it = deepCut([p_1, p_2], images[image_name], normalized_images[image_name], mask, net_copy, iterCount=5, downscale=args.downscale, early_stop_threshold=0.05)
                else:
                    # model mode 2: always use the same model
                    it = deepCut([p_1, p_2], images[image_name], normalized_images[image_name], mask, net, iterCount=5, downscale=args.downscale, early_stop_threshold=0.05)

                statistics.append([len(statistics), it, time.time() - start])   # add the id, the number of iterations and the time to the statistics
                mask = np.isin(mask, [cv.GC_FGD, cv.GC_PR_FGD]).astype(np.uint8) * 255

                # add the result to the segmentation
                segmentation = cv.bitwise_or(segmentation, mask)

            # export the segmentation mask and add it to the mapping file
            filename = f"{args.output_dir}/participant_{participant}_{image_name}.png"
            cv.imwrite(filename, segmentation)
            mapping_file.write(f"{args.data_dir}/annotations/{image_name}.png {filename}\n")

            # visualization
            if args.visualize:
                contour_img = np.copy(images[image_name])
                # draw the contours onto the image
                cv.drawContours(contour_img, [np.array(x) for x in data[participant][image_name].values()], -1, (255, 0, 0), 2)
                #cv.imshow(f"participant_{participant}_{image_name}", img)
                #cv.imshow("segmentation", segmentation)

                # compare the segmentation to the ground truth
                img = np.copy(contour_img)
                overlay = np.stack((np.zeros(img.shape[:2]), gt[image_name], segmentation), axis=-1)
                img[np.any(overlay != 0, axis=2)] = overlay[np.any(overlay != 0, axis=2)]
                cv.imshow(f"participant_{participant}_{image_name}_gt_comparison", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_gt_comparison", 100, 0)

                # visualize the segmentation by darkening unselected areas
                img = np.copy(contour_img)
                img[segmentation == 0] = np.round(0.3 * img[segmentation == 0]).astype(np.uint8)
                cv.imshow(f"participant_{participant}_{image_name}_segmentation", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_segmentation", img.shape[1] + 100, 0)

                # wait for the user to close the window
                # quit if the user pressed 'q'
                if cv.waitKey(0) == ord('q'):
                    cv.destroyAllWindows()

                    # close the mapping file
                    mapping_file.close()

                    # export the deepCut statistics
                    if args.deepcut_log:
                        np.savetxt(args.deepcut_log, statistics, fmt='%f', delimiter=',', header="call_id,iterations,time", comments='')
                    exit()
                cv.destroyAllWindows()

    # close the mapping file
    mapping_file.close()

    # export the deepCut statistics
    if args.deepcut_log:
        np.savetxt(args.deepcut_log, statistics, fmt='%f', delimiter=',', header="call_id,iterations,time", comments='')

    cv.destroyAllWindows()

if __name__ == "__main__":
    main()
