"""
It is assumed that the halo of contours encircling large stains contain no or only few stains.
This assumptions is based on two observations:
    - Large stains occur less often in groups then small stains.
    - While it is likely that a user only marks part of a group of small stains, large stains are unlikely to end up in multiple contours.

To test this hypothesis we look at the following measurements for contours containing small and large stains:
    - the number of contours containing stains in the halo region
    - the average number of stains in the halo region
    - the ratio of halo pixels that belong to stains


Testing this assumption is important for the DeepCut algorithm.
Since we can't assure that halo regions don't contain stains, we also execute DeepCut on the halo region.
This is helpful for two reasons:
    - the trained model is more exact, since stains in the halo region aren't trained to belong to the background
    - stains that are cut by the contour can be fully segmented
This approach is prone to the following problems:
    - if the halo contains mostly stains the model has only little background data
        - if the contour contains only small stains there are many background samples inbetween the stains,
          even if the halo contains little background data
        - if the contour contains a large stain there aren't many background samples within the contour
          => this is where the hypothesis is important
    - the training might be unstable, resulting in the whole region to be predicted as foreground
"""

import argparse
import math
import sys
import os
import time

import numpy as np
import cv2 as cv
import tqdm

from evaluation.util import load_images, load_annotations, load_data

def main():
    global args
    parser = argparse.ArgumentParser('Tests the assumption that there are only few stains in the halo region if the contour encircles a large stain.',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-d', '--data', type=str, help='the path of the directory containing the dataset', default="./data")
    parser.add_argument('--halo', type=int, default=20, help='the size of the halo')
    parser.add_argument('--size_threshold', type=int, default=500, help='the size required for a stain to be large (in pixels)')
    parser.add_argument('-v', '--visualize', action="store_true", help='visualize the contour and the halo')
    args = parser.parse_args()
    print(args)

    # load the images and the contours
    gt = load_annotations(d=os.path.join(args.data, "annotations"))
    data = load_data(filename=os.path.join(args.data, "data_processed.json"))

    # variables keeping track of the measurements
    cont_count_large = 0
    cont_count_small = 0
    halos_containing_stains_large = 0
    halos_containing_stains_small = 0
    halo_stain_count_large = 0
    halo_stain_count_small = 0
    halo_pixel_count_large = [0, 0]     # [background_pixels, foreground_pixels]
    halo_pixel_count_small = [0, 0]
    cont_pixel_count_large = [0, 0]
    cont_pixel_count_small = [0, 0]

    # iterate over all participants
    for participant in tqdm.tqdm(data):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():
            # iterate over all contours for the current participant and image
            for contour in data[participant][image_name].values():
                # create a mask for the contour
                contour_mask = np.zeros(gt[image_name].shape, dtype=np.uint8)
                cv.drawContours(contour_mask, [np.array(contour)], 0, 1, -1)

                # create a mask for the halo
                kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2 * args.halo + 1, 2 * args.halo + 1))
                halo_mask = cv.dilate(contour_mask, kernel, anchor=(args.halo, args.halo))
                halo_mask -= contour_mask

                # apply the masks
                contour_stains = cv.bitwise_and(gt[image_name], contour_mask)
                halo_stains = cv.bitwise_and(gt[image_name], halo_mask)

                # get the stain information
                cont_stain_count, _, cont_stain_stats, _ = cv.connectedComponentsWithStats(contour_stains, connectivity=4)
                halo_stain_count, _, halo_stain_stats, _ = cv.connectedComponentsWithStats(halo_stains, connectivity=4)

                # contour contains only background
                if cont_stain_count == 1:
                    continue

                # determine whether the contour contains a large stain
                if np.max(cont_stain_stats[1:, cv.CC_STAT_AREA]) < args.size_threshold:
                    # small stain
                    cont_count_small += 1
                    halos_containing_stains_small += 1 if cv.countNonZero(halo_stains) > 0 else 0
                    halo_stain_count_small += halo_stain_count - 1
                    halo_pixel_count_small[0] += cv.countNonZero(halo_mask) - cv.countNonZero(halo_stains)
                    halo_pixel_count_small[1] += cv.countNonZero(halo_stains)
                    cont_pixel_count_small[0] += cv.countNonZero(contour_mask) - cv.countNonZero(contour_stains)
                    cont_pixel_count_small[1] += cv.countNonZero(contour_stains)
                else:
                    # large stain
                    cont_count_large += 1
                    halos_containing_stains_large += 1 if cv.countNonZero(halo_stains) > 0 else 0
                    halo_stain_count_large += halo_stain_count - 1
                    halo_pixel_count_large[0] += cv.countNonZero(halo_mask) - cv.countNonZero(halo_stains)
                    halo_pixel_count_large[1] += cv.countNonZero(halo_stains)
                    cont_pixel_count_large[0] += cv.countNonZero(contour_mask) - cv.countNonZero(contour_stains)
                    cont_pixel_count_large[1] += cv.countNonZero(contour_stains)

                if args.visualize:
                    print("small stain" if np.max(cont_stain_stats[1:, cv.CC_STAT_AREA]) < args.size_threshold else "large stain")
                    cv.imshow("contour: blue, halo: green, gt: red", np.stack((contour_mask * 255, halo_mask * 255, gt[image_name]), axis=-1))
                    if cv.waitKey(0) == ord("q"):
                        exit()

    print(f"Contours encircling large stains: {cont_count_large}")
    print(f"percentage of halos containing stains: {halos_containing_stains_large / cont_count_large}")
    print(f"average number of stains in the halo: {halo_stain_count_large / cont_count_large}")
    print(f"ratio of stain pixels in the halo: {halo_pixel_count_large[1] / np.sum(halo_pixel_count_large)}")
    print(f"ratio of stain pixels in the contour: {cont_pixel_count_large[1] / np.sum(cont_pixel_count_large)}")

    print(f"\nContours encircling small stains: {cont_count_small}")
    print(f"percentage of halos containing stains: {halos_containing_stains_small / cont_count_small}")
    print(f"average number of stains in the halo: {halo_stain_count_small / cont_count_small}")
    print(f"ratio of stain pixels in the halo: {halo_pixel_count_small[1] / np.sum(halo_pixel_count_small)}")
    print(f"ratio of stain pixels in the contour: {cont_pixel_count_small[1] / np.sum(cont_pixel_count_small)}")


if __name__ == "__main__":
    main()
