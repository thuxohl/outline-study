import os
import itertools
import time
import traceback

import cv2 as cv
import numpy as np
import tqdm

from segmentation.util.argparse import create_parser
from segmentation.util.file import read_mapping_file
from segmentation.util.opencv import compute_connected_components, connect_regions, MATCH_75, CONNECT_BY_CONTOURS
import segmentation.util.opencv as region_utils

class PixelwiseQuantities:

    def __init__(self, tp: int = 0, tn: int = 0, fp: int = 0, fn: int = 0):
        self.tp = tp
        self.tn = tn
        self.fp = fp
        self.fn = fn

    def __add__(self, other):
        tp = self.tp + other.tp
        tn = self.tn + other.tn
        fp = self.fp + other.fp
        fn = self.fn + other.fn
        return PixelwiseQuantities(tp, tn, fp, fn)

    def __repr__(self):
        l = [self.tp, self.tn, self.fp, self.fn]
        max_len = len(str(max([self.tp, self.tn, self.fp, self.fn])))
        s = sum(l)
        ls = [x / s for x in l]
        return f'[TP, FP] = [{self.tp:{max_len}d}, {self.fp:{max_len}d}] = [{ls[0]:.3f}, {ls[2]:.3f}]\n' + \
               f'[TN, FN] = [{self.tn:{max_len}d}, {self.fn:{max_len}d}] = [{ls[1]:.3f}, {ls[3]:.3f}]'

    @classmethod
    def compute(cls, prediction: np.array, annotation: np.array):
        prediction = prediction > 0
        annotation = annotation > 0

        intersection = np.logical_and(prediction, annotation)

        tp = intersection.sum()

        fp = prediction.sum() - tp
        fn = annotation.sum() - tp

        pixel_count = prediction.shape[0] * prediction.shape[1]
        tn = pixel_count - tp - fp - fn

        return cls(tp, tn, fp, fn)


def precision(q: PixelwiseQuantities):
    if q.tp + q.fp == 0:
        return 1
    # if q.tp == 0:
        # raise ValueError(f'No true positives :\n{q}')
        # return 0
    denominator = q.tp + q.fp
    return q.tp / denominator

def recall(q: PixelwiseQuantities):
    if q.tp + q.fp == 0:
        return 0
    # if q.tp == 0:
        # raise ValueError(f'No true positives :\n{q}')
        # return 0
    denominator = q.tp + q.fn
    # if denominator == 0:
        # raise ValueError(f'No true positives or false negatives:\n{q}')
    return q.tp / denominator

def true_positive_rate(q: PixelwiseQuantities):
    return recall(q)

def false_positive_rate(q: PixelwiseQuantities):
    denominator = q.fp + q.tn
    if denominator == 0:
        # return 0
        raise ValueError(f'No false positives or true negatives:\n{q}')
    return q.fp / denominator

def f_beta(q: PixelwiseQuantities, beta_squared: float=0.3):
    _precision = precision(q)
    _recall = recall(q)
    if _precision == 0.0 and _recall == 0:
        raise ValueError(f'No precision and recall for FBeta')
        # return 0
    return ((1 + beta_squared) * _precision * _recall) / (beta_squared * _precision + _recall)

def intersection_over_union(q: PixelwiseQuantities):
    denominator = q.tp + q.fp + q.fn
    if denominator == 0:
        raise ValueError(f'No true positives, false positives or false negatives:\n{q}')
    return q.tp / denominator

def iou(q: PixelwiseQuantities):
    return intersection_over_union(q)


class RegionQuantities:

    def __init__(self, regions: int = 0, detected_regions: int = 0, false_predictions: int = 0):
        self.regions = regions
        self.detected_regions = detected_regions
        self.false_predictions = false_predictions

    def __repr__(self):
        return f'(Regions, Detections, False-Predictions) = ({self.regions}, {self.detected_regions}, {self.false_predictions})'

    def __add__(self, other):
        regions = self.regions + other.regions
        detected_regions = self.detected_regions + other.detected_regions
        false_predictions = self.false_predictions + other.false_predictions
        return RegionQuantities(regions, detected_regions, false_predictions)

    @classmethod
    def computeFromCcs(cls, ccs_pre, ccs_ann, region_threshold=0.5, prediction_threshold=0.5, deteced_regions=None, false_predictions=None):
        predictionCoverage = dict(map(lambda cc: (cc.label, 0), ccs_pre))
        for cc_pre in ccs_pre:
            for cc_ann in ccs_ann:
                predictionCoverage[cc_pre.label] += cc_pre.intersection_count(cc_ann) / cc_pre.stats[cv.CC_STAT_AREA]

        if false_predictions is None:
            false_predictions = []
        false_predictions.extend([label for label in predictionCoverage if predictionCoverage[label] < prediction_threshold])

        annotationCoverage = dict(map(lambda cc: (cc.label, 0), ccs_ann))
        for cc_pre in ccs_pre:
            if cc_pre.label in false_predictions:
                continue

            for cc_ann in ccs_ann:
                annotationCoverage[cc_ann.label] += cc_ann.intersection_count(cc_pre) / cc_ann.stats[cv.CC_STAT_AREA]

        if deteced_regions is None:
            deteced_regions = []
        deteced_regions.extend([label for label in annotationCoverage if annotationCoverage[label] >= region_threshold])

        return cls(len(ccs_ann), len(deteced_regions), len(false_predictions))

    @classmethod
    def computeFromImgs(cls, prediction, annotation, **kwargs):
        ccs_pre = compute_connected_components(prediction)
        ccs_ann = compute_connected_components(ann)
        return cls.computeFromCcs(ccs_pre, ccs_ann, **kwargs)


def region_detection_rate(q, false_prediction_weight=0.1):
    if q.regions == 0:
        raise ValueError(f'No regions in Quantities:\n{q}')
        return 0
    return q.detected_regions / (q.regions + false_prediction_weight * q.false_predictions)

def rdr(q, **kwargs):
    return region_detection_rate(q, **kwargs)


def area_under_curve(xs, ys):
    area = 0
    for x_0, x_1, y_0, y_1 in zip(xs[:-1], xs[1:], ys[:-1], ys[1:]):
        delta_x = x_1 - x_0
        delta_y = abs(y_1 - y_0)

        min_y = min(y_1, y_0)

        area += delta_x * min_y
        area += delta_x * delta_y / 2
    return area

def auc(xs, ys):
    return area_under_curve(xs, ys)


measures = (precision, recall, true_positive_rate, false_positive_rate, f_beta, iou, rdr)

if __name__ == '__main__':
    measures = [*measures]
    measures_names = tuple(map(lambda measure: measure.__name__, measures))

    parser = create_parser('Compute different measures for salient object detection.')
    parser.add_argument('--mapping_file', type=str, required=True,
                        help='a file mapping an annotation to a salience map in each line')
    parser.add_argument('-t', '--threshold', type=int, default=100,
                        help='the threshold applied to each salience map to create a binary prediction')
    parser.add_argument('-i', '--iterate', action='store_true',
                        help='this is a shortcut for setting threshold=-1 & threshold_end=256')
    parser.add_argument('--threshold_end', type=int, default=-1,
                        help='if this value is positive, the script will iterate over all \
                              thresholds from threshold to threshold_end to compute the measures')
    parser.add_argument('--threshold_step', type=int, default=8,
                        help='if iterating (threshold_end > 0) thresholds will be increased by this \
                              value after each iteration')
    parser.add_argument('--connect_regions', action='store_true',
                        help='connect all predicted regions mapping to the same annotated \
                              region through their convex hull')
    parser.add_argument('--matching_criterium', type=str, default=MATCH_75.__name__,
                        help='chose a criterium to select regions for mapping')
    parser.add_argument('--connection_method', type=str, default=CONNECT_BY_CONTOURS.__name__,
                        help='chose a method for connecting regions')
    parser.add_argument('--skip_measures', type=str, nargs='+',
                        help=f'names of measures{measures_names} which should not be computed')
    parser.add_argument('--output_file', type=str, default=None,
                        help='if provided the results will be written to this file in addition to printing on console')
    parser.add_argument('--beta_squared', type=float, default=0.3,
                        help='the beta squared value used to compute the FBeta measure')
    parser.add_argument('--region_threshold', type=float, default=0.5,
                        help='threshold how much of a regions has to be predicted correctly')
    parser.add_argument('--prediction_threshold', type=float, default=0.25,
                        help='threshold of how large the area of a prediction has to be to not count as a false positive')
    parser.add_argument('--false_prediction_weight', type=float, default=0.1,
                        help='how much false predictions are weighted in the region detection rate')
    parser.add_argument('--average_images', action='store_true',
                        help='compute measures per images and average them')
    args = parser.parse_args()

    if args.threshold_end < 0:
        args.threshold_end = args.threshold + 1

    if args.iterate:
        args.threshold = -1
        args.threshold_end = 256

    if args.skip_measures:
        for measure_name in args.skip_measures:
            measures.remove(next(filter(lambda measure: measure.__name__ == measure_name, measures)))

    mappings = read_mapping_file(args.mapping_file)

    min_column_size = 6
    headers = ('threshold', *map(lambda measure: measure.__name__, measures))
    headers_str = ', '.join(map(lambda header: f'{header:>{min_column_size}}', headers))

    print(headers_str)

    mappings = [(filename_ann, filename_sam,
                 cv.imread(filename_ann, cv.IMREAD_GRAYSCALE),
                 cv.imread(filename_sam, cv.IMREAD_GRAYSCALE))
                 for filename_ann, filename_sam in mappings]

    try:
        rows = []
        for threshold in range(args.threshold, args.threshold_end, args.threshold_step):
            quantites = {'pixelwise': [], 'region': []}

            for filename_ann, filename_sam, _ann, _sam in tqdm.tqdm(mappings):
                #print(f'T={threshold} - Process {os.path.basename(filename_ann)}', end='\r')
                ann = _ann.copy()
                sam = _sam.copy()

                _, pre = cv.threshold(sam, threshold, 255, cv.THRESH_BINARY)

                if rdr in measures or args.connect_regions:
                    ccs_pre = compute_connected_components(pre)
                    ccs_ann = compute_connected_components(ann)

                if args.connect_regions:
                    matching_criterium = getattr(region_utils, args.matching_criterium)
                    connection_method = getattr(region_utils, args.connection_method)
                    connect_regions(pre, ann, ccs_pre=ccs_pre, ccs_ann=ccs_ann, matching_criterium=matching_criterium, connection_method=connection_method)
                    # connect_regions(ann, pre, ccs_pre=ccs_ann, ccs_ann=ccs_pre, matching_criterium=matching_criterium, connection_method=connection_method)

                quantites['pixelwise'].append(PixelwiseQuantities.compute(pre, ann))
                if rdr in measures:
                    quantites['region'].append(RegionQuantities.computeFromCcs(ccs_pre, ccs_ann, region_threshold=args.region_threshold, prediction_threshold=args.prediction_threshold))
                    # print(quantites['region'][-1])
                # combined = np.zeros((*ann.shape, 3), np.uint8)
                # combined[:, :, 1] = ann
                # combined[:, :, 2] = pre
                # cv.namedWindow('Test', cv.WINDOW_NORMAL)
                # cv.resizeWindow('Test', 1920, 1080)
                # cv.imshow('Test', combined)
                # cv.waitKey(0)


            measure_values = {}
            for measure in measures:
                if args.average_images:
                    if measure == rdr:
                        values = list(map(lambda q: measure(q), quantites['region']))
                    elif measure == f_beta:
                        values = list(map(lambda q: measure(q, args.beta_squared), quantites['pixelwise']))
                    else:
                        values = list(map(lambda q: measure(q), quantites['pixelwise']))
                    measure_values[measure] = sum(values) / len(values)
                else:
                    pq = sum(quantites['pixelwise'], PixelwiseQuantities())
                    pr = sum(quantites['region'], RegionQuantities())
                    if measure == f_beta:
                        measure_values[measure] = measure(pq, args.beta_squared)
                    else:
                        measure_values[measure] = measure(pr) if measure == rdr else measure(pq)

            row = [f'{threshold:>{len(headers[0])}}']
            for measure, value in measure_values.items():
                value_formatted = f'{value:.4f}'
                value_formatted = f'{value_formatted:>{len(measure.__name__)}}'
                row.append(value_formatted)

            rows.append(', '.join(row))
            print(rows[-1])

        if args.output_file:
            with open(args.output_file, mode='w') as f:
                f.write('\n'.join([headers_str, *rows]))

    except Exception as e:
        print(f'Error {e} at threshold {threshold} for mappings file {args.mapping_file} for mapping {os.path.basename(filename_ann)}')
        traceback.print_exc()
        exit(1)
