import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm

from evaluation.util import load_images, load_annotations, load_data


def main(args):
    parser = argparse.ArgumentParser('Uses GrabCut for interactive segmentation of stains',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('-o', '--output_dir', type=str, default='./data/segmentation/grabCut',
                        help='the directory to which the segmentation masks get exported')
    parser.add_argument('-m', '--mapping_file', type=str, default='./data/segmentation/grabCut/mapping.txt',
                        help='where to generate the file which contains the mapping from segmentation mask to annotation')
    parser.add_argument('-v', '--visualize', action="store_true",
                        help='visualize the resulting segmentation masks')
    args = parser.parse_args(args[1:])
    print(args)

    # load the images and the contours
    images = load_images(d=os.path.join(args.data_dir, "images"))
    gt = load_annotations(d=os.path.join(args.data_dir, "annotations")) if args.visualize else None
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # create the output directory and open the output file
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)
    mapping_file = open(args.mapping_file, 'w')

    # iterate over all participants
    for participant in tqdm.tqdm(data):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():
            # create a new segmentation mask
            segmentation = np.zeros(images[image_name].shape[:2], np.uint8)

            # smooth the image
            image = cv.GaussianBlur(images[image_name], (5, 5), 0)

            # increase the contrast by equalizing the histogramms of each channel followed by a gamma correction
            image = np.stack((cv.equalizeHist(image[:, :, 0]), cv.equalizeHist(image[:, :, 1]), cv.equalizeHist(image[:, :, 2])), axis=2)
            lookUpTable = np.empty((1,256), np.uint8)
            gamma = 0.4
            for i in range(256):
                lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
            image = cv.LUT(image, lookUpTable)

            # iterate over all contours for the current participant and image
            for contour in data[participant][image_name].values():
                mask = np.zeros(segmentation.shape, np.uint8)
                # draw the contour
                cv.drawContours(mask, [np.array(contour)], 0, cv.GC_PR_FGD, -1)

                # only work on the local area to improve runtime
                rect = cv.boundingRect(np.array(contour))
                shape = np.array([rect[2], rect[3]])
                s = 0
                p_1 = np.clip(np.round(np.array([rect[0], rect[1]]) - s * shape).astype(np.int), 0, [images[image_name].shape[1], images[image_name].shape[0]])
                p_2 = np.clip(np.round(p_1 + (1 + 2 * s) * shape).astype(np.int), 0, [images[image_name].shape[1], images[image_name].shape[0]])

                # execute grabCut
                bgdModel = np.zeros((1, 65), np.float64)
                fgdModel = np.zeros((1 ,65), np.float64)
                cv.grabCut(image[p_1[1] : p_2[1], p_1[0] : p_2[0]], mask[p_1[1] : p_2[1], p_1[0] : p_2[0]], None, bgdModel, fgdModel, 5, cv.GC_INIT_WITH_MASK)
                mask = np.isin(mask, [cv.GC_FGD, cv.GC_PR_FGD]).astype(np.uint8) * 255

                # add the result to the segmentation
                segmentation = cv.bitwise_or(segmentation, mask)

            # export the segmentation mask and add it to the mapping file
            filename = f"{args.output_dir}/participant_{participant}_{image_name}.png"
            cv.imwrite(filename, segmentation)
            mapping_file.write(f"{args.data_dir}/annotations/{image_name}.png {filename}\n")

            # visualization
            if args.visualize:
                contour_img = np.copy(images[image_name])
                # draw the contours onto the image
                cv.drawContours(contour_img, [np.array(x) for x in data[participant][image_name].values()], -1, (255, 0, 0), 2)
                #cv.imshow(f"participant_{participant}_{image_name}", img)
                #cv.imshow("segmentation", segmentation)

                # compare the segmentation to the ground truth
                img = np.copy(contour_img)
                overlay = np.stack((np.zeros(img.shape[:2]), gt[image_name], segmentation), axis=-1)
                img[np.any(overlay != 0, axis=2)] = overlay[np.any(overlay != 0, axis=2)]
                cv.imshow(f"participant_{participant}_{image_name}_gt_comparison", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_gt_comparison", 100, 0)

                # visualize the segmentation by darkening unselected areas
                img = np.copy(contour_img)
                img[segmentation == 0] = np.round(0.3 * img[segmentation == 0]).astype(np.uint8)
                cv.imshow(f"participant_{participant}_{image_name}_segmentation", img)
                cv.moveWindow(f"participant_{participant}_{image_name}_segmentation", img.shape[1] + 100, 0)

                # wait for the user to close the window
                # quit if the user pressed 'q'
                if cv.waitKey(0) == ord('q'):
                    cv.destroyAllWindows()

                    # close the mapping file
                    mapping_file.close()
                    exit()
                cv.destroyAllWindows()

    # close the mapping file
    mapping_file.close()

    cv.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
