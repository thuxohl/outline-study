import time

import torch
import tqdm

from segmentation.deep_cut.cnn import CNN

batch_size = 512
img_shape = (3, 33, 33)
iterations = 1000

def measure_time(device, iterations):
    cnn = CNN()
    cnn = cnn.to(device)
    since = time.time()
    for i in tqdm.trange(iterations):
        inputs = torch.rand((batch_size, *img_shape)).to(device)
        outputs = cnn(inputs)
    total_time = time.time() - since
    return total_time

print('Measure speed GPU:')
total_time = measure_time(torch.device('cuda'), iterations)
print(f'Took {total_time:.3f}s which is {1000 * total_time / iterations:.3f}ms per prediction.')

print('Measure speed CPU:')
total_time = measure_time(torch.device('cpu'), iterations)
print(f'Took {total_time:.3f}s which is {1000 * total_time / iterations:.3f}ms per prediction.')
