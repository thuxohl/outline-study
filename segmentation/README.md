# Interactive Stain Segmentation using DeepCut

## <u>The Task</u>
We face the problem that the annotations, made by users, often include multiple stains and parts of the background. This means that the labels for the neural network include a huge level of noise. To improve the training data for the neural network we tried to use approaches for interactive segmentation. The following characteristics are important for the task:
- The encircled region can contain a huge number of objects
- The encircled region can contain a large percentage of background [I guess that regions with small stains are more than 75% background]
- The background also contains foreground objects, since a single annotation doesn't contain all stains
- The laundry pieces contain patterns (mostly stripes) and folds that stand out but don't belong to the foreground class
- The foreground objects have a huge variation in size (from only a few pixels wide to over 100 pixels wide)
- The algorithm needs to be fast since it is used in real time (our goal is to finish the segmentation in less than a second for moderate sized annotations)



## <u>General</u>

### Requirements
We used several python libraries for this project. All approaches require OpenCV and the DeepCut implementations use Torchvision for the neural network and SimpleCRF for the conditional random fields. The third version of DeepCut uses Scikit Learn for the foreground models. A complete list of requirements can be found in the file `requirements.txt`.

### Setting up the training and the test sets
We split the available data into a training and a test set. This can be done by executing `source segmentation/deep_cut/setup.sh` from within the top level directory of the repository.

### Visualization of network results
We created a script that visualizes the exported segmentation masks. It can be executed by running `python3 -m segmentation.visualize_segmentation -d [data directory] -s [directory containing the segmentation masks]`

### Results
The following table contains the F_beta scores for the different approaches. Since, for us, it is more important to catch all stains than having a low amount of false positives we chose beta as 2.

Algorithm                                | F_beta Score (beta=2)
---------------------------------------- | ---------------------
UpperBound                               | 0.9026
GrabCut                                  | 0.0736
DeepCut v1                               | 0.5387¹
DeepCut v2                               | 0.2022
DeepCut v3 + dilation 5                  | 0.3490²
DeepCut v3 + line penalties + dilation 7 | 0.3792
DeepCut v4                               | 0.2192

¹ [Ich habe Ergebnisse von mehreren Variationen von DeepCut v1. Dieses Ergebniss gehört zur Standardkonfiguration.]\
² [Die CRF Parameter wurden für die Version mit Streifenerkennung optimiert. Ich denke aber nicht, dass der Unterschied relevant ist.]



## <u>Approach 0: Upper Bound</u>
To get a better understanding of the quality of the algorithms we started by computing an upper bound by creating segmentation masks that contain the union of the contours and the ground truth masks. This simulates an algorithm that perfectly segments all stains that lie within the user-annotated contour.

### Usage
To create the segmentation mask execute `python3 -m segmentation.upperBound`. Use the argument `-h` to learn more about the possible arguments.



## <u>Approach 1: GrabCut</u>
As a first approach we tried using the well known GrabCut algorithm for interactive foreground segmentation. Unfortunately the resulting segmentation masks are quite bad. Almost all of the stains are missed. Instead the algorithm selects nothing, the stripes of the piece of laundry or the area between the stripes. We suppose that this has several reasons:
- Since the unmarked areas also contain stains it isn't clear what is fore- and background
- The color of the stripes is more similar to the color of the stains than to the color of the remaining background
- The stripes are larger and thus stand out more than the stains
- The stripes have a hard border incentivising GraphCut to make a cut between a stripe and the remaining background

We tested different preprocessing approaches to improve the performance of GrabCut but those attempts didn't yield any improvements worth mentioning.

### Usage
There are two different implementations for the GrabCut algorithm:
- classical grabCut: `python3 -m segmentation.grabCut`
- grabCut with preprocessing: `python3 -m segmentation.grabCut_high_contrast`

Use the argument `-h` to learn more about the possible arguments.



## <u>Approach 2: DeepCut</u>
After GrabCut yielded no usable results we took a look at DeepCut that uses a principle similar to GrabCut but replaces its components to achieve better results. The Gaussian Mixture Models are replaced by a shallow convolutional neural network that predicts the foreground probability for the middle pixel of a 33x33 patch. Instead of learning the model from scratch it is retrained from the previous iteration. GraphCut is replaced by a [fully connected conditional random field](https://arxiv.org/abs/1210.5644) that considers all pairs of pixels. Unfortunately the publication [DeepCut: Object Segmentation from Bounding Box Annotations using Convolutional Neural Networks](https://arxiv.org/abs/1605.07866) by Rajchl et al. doesn't contain the source code. Therefore we wrote our own implementation and tested different variations.


### Version 1
[Ich habe mir nicht nochmal den Code angeschaut sondern schreibe das meiste aus dem Gedächtnis. Da das schon recht lang her ist, bin ich mir bei einigen Dingen nicht mehr wirklich sicher (vor allem, was am Ende der genaue Grund war weswegen v1 nicht nutzbar war). Wenn Du in einem Paper auf diese Version eingehen möchtest oder Dich die Details interessieren schaue ich mir den Code nochmal an, um sicher zu gehen, dass das richtig ist.]

Our first version of DeepCut stayed quite close to the original approach of Rajchl et al. It uses the same CNN architecture combined with conditional random fields. Unfortunately we made a mistake in the implementation: instead of training a new CNN for every contour we retrained a pretrained CNN that was trained on the training set. While this led to acceptable results, we observed that our results got better the lower we chose the learning rate of the CNN. The low learning rate indicates that the good results are only due to the pretrained network and that the retraining actually decreases performance. We suspect that we aren't able to use this architecture for unseen images since it only utilizes the existing network without adapting to new images. Since we realized some other problems as well, we decided to create a new DeepCut version with some changes to the original paper, instead of fixing the problems with this version.

#### Usage
###### Setting up the training and the test sets
The python script `preprocess_training_data.py` is used to extract possible foreground and background examples from the datasets. Like Rajchl et al. we use a halo of 20 pixels. To finish the setup of the dataset execute \
`python3 -m segmentation.deep_cut.v1.preprocess_training_data --data ./data/train` \
`python3 -m segmentation.deep_cut.v1.preprocess_training_data --data ./data/test`

###### Training the CNN
DeepCut requires a CNN that is pretrained on the dataset. This can be done using the file `cnn.py`. It is possible to train a new model or to continue the training of an existing model. For further information execute `python3 -m segmentation.v1.deep_cut.cnn -h`. \
The training loss can be visualized using `plot_training.py` (this requires the log file that is generated when executing `cnn.py` with the argument `--log_csv [path]`).

###### Visualizing the predictions of the CNN
The script `visualize_cnn.py` can be used to compare the CNN predictions to the ground truth annotations for the pictures in the dataset. The predictions are exported to save on computation time the next time the script is executed. If a different CNN is used the folder `prediction` within the data folder needs to be deleted to recompute the predictions. For further information use `python3 -m segmentation.v1.deep_cut.visualize_cnn -h`.

###### DeepCut
The script `deep_cut.py` executes the DeepCut algorithm for all contours of all participants and exports the resulting segmentations to the folder `data/segmentation/deepCut`. The DeepCut algorithm requires a pretrained CNN, which can be trained using `cnn.py`. It is possible to show the segmentations during execution using the argument `-v`. For further information use `python3 -m segmentation.v1.deep_cut.deep_cut -h`.


### Version 2
In the second version of DeepCut we changed the convolutional neural network that is used to represent the probability that a pixel belongs to the foreground. Rajchl et al. use a CNN that works on 33x33 image patches and only predicts the class of the middle pixel. This approach is quite ineffective, since many of the convolutions are calculated multiple times for different image pixels.\
We decided to replace this CNN by a more conventional U-Net for image segmentation. Our solution is oriented at the [paper](https://arxiv.org/abs/1505.04597) by Ronneberger et al. [Das Paper von Ronneberger et al. habe ich nicht gelesen, sondern ein anderes Paper zu U-Nets. Die [Python Implementierung](https://github.com/milesial/Pytorch-UNet), an der ich mich orientiert habe, basiert aber auf dem Paper von Ronneberger et al.]. The U-Net serves the same functionality as the network by Rajchl el al.: it represents the probability of a pixel belonging to the foreground. The U-Net has two main advantages over the network by Rajchl et al. Since it predicts a segmentation for a complete image it is able to reuse many of the convolutions that need to be recalculated for every patch in the original CNN. Using a single pass through the network, might also enable further optimization by Pytorch. Furthermore U-Nets are widely used for image segmentation and thus known by a wide range of people.\
We used two separate U-Nets: one pretrained, complex U-Net was used to compute an initial segmentation as mentioned by Rajchl et al. Later on we switched the initialization and used the user annotated contour followed by the CRF. A second, more simple U-Net was used as a foreground model within DeepCut.

Unfortunately our approaches with the U-Net didn't work as expected. The main problem was that the segmentation network required too much training to be usable for our task. We tried multiple different approaches like e.g. retraining the last layers of a pretrained network but weren't able to create a solution that converged. We assume that even the simplified version of the U-Net was to complex for the task and thus not flexible enough.

#### Usage
[Da die Version nicht erfolgreich war, habe ich den Code nie so weit aufgeräumt, dass ich eine fertiges, vorzeigbares Programm habe. Momentan fehlen noch Argumente, um die verschiedenen Optionen auswählen zu können und einige Sachen sind hardgecoded]


### Version 3
After discovering that the main problem of version 2 was the model complexity, we decided to use a simpler model to represent the fore- and background. We tested a color based approach like in the original GrabCut paper, but kept the conditional random fields for creating the binary segmentation. In our previous tests, we discovered that CRFs are capable of producing good segmentations from rough foreground estimates. We are even able to use them to create a rough initial segmentation.\
We tested a logistic regression and decision trees that make a foreground prediction based on the RGB values of a pixel. The GMMs used in GrabCut might also be applicable but weren't used since they would have been more complicated to implement. The decision trees produced the best results and were thus used from here on.

We noticed that the resulting segmentation contained most of the stains but many of the small stains collapsed to a small inner region of only a few pixels. We included a dilation with kernel size 3 directly after the conditional random field to prevent this sort of shrinking. Furthermore we applied an additional dilation before returning the final result.\
The main problems of this version are the stripes that are similar in color to the stains and thus are often classified as foreground. To counteract this, we searched for the stripes with a Hough Lines Transform in the RGB image. We took the orientation of lines that occurred most often and only kept those lines. This approach was quite effective at filtering out all lines that didn't belong to stripes. We created a mask where lines were marked as 0.5 and all other areas were marked as 1. If two lines were close to each other (10 pixels) they are likely to belong to the two edges of a single stripe and thus the area between them is also marked. Furthermore we executed a dilation on the stripes to include the border regions that were often marked by the CNN and smoothed the transition between stripe and non-stripe. The resulting mask was multiplied with the prediction to decrease the influence of stripes. The areas that don't belong to a stripe are multiplied with 1 and thus remain unchanged. The areas that contain stripes are multiplied by 0.5 and thus their influence is decreased. We don't set the predictions for stripes to 0, since there are many cases where stains and stripes intersect. With our approach the CRF is discouraged from marking stripes as foreground but it is possible if there are stains in those regions.\
We also noticed that faint stains are similar in color to the background and hard to detect, but weren't able to find a solution for this problem.

#### Usage
###### Testing the classifiers
We started by testing the capabilities of the different classifiers.
- To plot the distributions of the color spaces, run: `python3 -m segmentation.deep_cut.v3.color_space_test`
- To train a classifier on a single image and show its prediction, run: `python3 -m segmentation.deep_cut.v3.color_classification_test`

Use the argument `-h` to learn more about the possible arguments.

###### Running DeepCut
To execute our third version of DeepCut, run: `python3 -m segmentation.deep_cut.v3.deep_cut`\
Use the argument `--penalize_lines` to reduce the foreground probability of stripes. Use `-h` to learn more about other possible arguments.

###### Searching good CRF parameters
We used a grid search to find good hyperparameters for the conditional random fields. To run the script yourself execute `python3 -m segmentation.deep_cut.v3.crf_parameters_deep_cut`. Use the argument `-h` to learn more about the possible arguments.


### Version 4
After the success of the simple model used in version 3, we tried using a CNN yet again. This time we kept the CNN even simpler to counteract the convergence issues of version 2. The CNN consists of an initial 3x3 convolution that takes the image and produces 8 output channels. Those 8 channels are used by a final 1x1 convolution that produces a single probability for every pixel.

Unfortunately, the results aren't as good as in version 3. While the CNN is better at finding faint stains and stains with blurred borders it focuses even more on stripes. If a prominent stripe is found, the network focuses only on that stripe and the actual stains aren't segmented as foreground. It also seems that the CNN version has problems if the selected area is large.

#### Usage
###### Running DeepCut
To execute our fourth version of DeepCut, run: `python3 -m segmentation.deep_cut.v4.deep_cut`. Use the argument `-h` to learn more about the possible arguments.

###### Searching good CRF parameters
We used a grid search to find good hyperparameters for the conditional random fields. To run the script yourself execute `python3 -m segmentation.deep_cut.v4.crf_parameters_deep_cut`. Use the argument `-h` to learn more about the possible arguments.
