import argparse
import math
import sys
import os

import numpy as np
import cv2 as cv
import tqdm

from evaluation.util import load_annotations, load_data


def main(args):
    parser = argparse.ArgumentParser('Calculates the intersection of contour and annotation to get an upper bound on the segmentation quality',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--data_dir', type=str, default='./data',
                        help='the directory containing the image data, the annotations and the contours')
    parser.add_argument('-o', '--output_dir', type=str, default='./data/segmentation/upperBound',
                        help='the directory to which the segmentation masks get exported')
    parser.add_argument('-m', '--mapping_file', type=str, default='./data/segmentation/upperBound/mapping.txt',
                        help='where to generate the file which contains the mapping from segmentation mask to annotation')
    args = parser.parse_args(args[1:])
    print(args)

    # load the images and the contours
    annotations = load_annotations(d=os.path.join(args.data_dir, "annotations"))
    data = load_data(filename=os.path.join(args.data_dir, "data_processed.json"))

    # create the output directory and open the output file
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)
    mapping_file = open(args.mapping_file, 'w')

    # iterate over all participants
    for participant in tqdm.tqdm(data):
        # iterate over all images the current participant annotated
        for image_name in data[participant].keys():
            # create a new segmentation mask
            segmentation = np.zeros(annotations[image_name].shape[:2], np.uint8)

            # iterate over all contours for the current participant and image
            for contour in data[participant][image_name].values():
                mask = np.zeros(segmentation.shape, np.uint8)
                # draw the contour
                cv.drawContours(mask, [np.array(contour)], 0, 255, -1)

                # add the intersection of annotation and contour to the segmentation mask
                mask = cv.bitwise_and(annotations[image_name], mask)
                segmentation = cv.bitwise_or(segmentation, mask)

            # export the segmentation mask and add it to the mapping file
            filename = f"{args.output_dir}/participant_{participant}_{image_name}.png"
            cv.imwrite(filename, segmentation)
            mapping_file.write(f"{args.data_dir}/annotations/{image_name}.png {filename}\n")

    # close the mapping file
    mapping_file.close()


if __name__ == '__main__':
    main(sys.argv)
